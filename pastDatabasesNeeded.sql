-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 09, 2018 at 04:22 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `isupport`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE IF NOT EXISTS `banner_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(85) COLLATE utf8_unicode_ci NOT NULL,
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `upload_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `title`, `banner_caption`, `image_url`, `user_id`, `upload_id`, `created_at`, `updated_at`) VALUES
(1, 'Default Banner', '', 'uploads/2015-03-16/755b662e71cb719c0d2afda72b7df4c7.png', 0, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Default Banner2', 'Welcome to our Home!', 'resources/images/background-image.png', 0, 80, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `amount` decimal(5,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cart_user_id_foreign` (`user_id`),
  KEY `cart_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `project_id`, `amount`, `created_at`, `updated_at`) VALUES
(11, 30, 1, 1.00, '2016-02-18 05:11:35', '2016-02-18 05:11:35'),
(12, 30, 11, 50.00, '2016-02-18 05:11:58', '2016-02-18 05:11:58');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `homepage` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`, `homepage`) VALUES
(1, 'Technology', '0000-00-00 00:00:00', '2015-08-05 01:31:03', 1),
(2, 'Health', '0000-00-00 00:00:00', '2016-03-12 00:53:36', 12),
(3, 'Music Fest', '0000-00-00 00:00:00', '2016-03-12 00:07:03', 0),
(4, 'Art', '0000-00-00 00:00:00', '2016-03-12 00:07:34', 0),
(5, 'Science', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'Poetry', '0000-00-00 00:00:00', '2015-08-05 01:31:25', 8),
(7, 'Theatre', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 'Fashion', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(9, 'Games', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, 'Comics', '0000-00-00 00:00:00', '2015-07-23 19:05:56', 0),
(11, 'Crafts', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, 'Dance', '0000-00-00 00:00:00', '2015-08-05 01:31:19', 4),
(14, 'Testing', '2015-09-11 21:37:51', '2015-09-11 21:37:51', 0),
(16, 'Art', '2016-03-12 00:10:47', '2016-03-12 00:10:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_follow`
--

CREATE TABLE IF NOT EXISTS `category_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_follow_user_id_foreign` (`user_id`),
  KEY `category_follow_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=116 ;

--
-- Dumping data for table `category_follow`
--

INSERT INTO `category_follow` (`id`, `user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(4, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 10, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 10, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 10, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 11, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 11, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 11, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 11, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 11, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 11, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 11, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 11, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 11, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 11, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 12, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 12, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 13, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 13, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 14, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 14, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 14, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 14, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 14, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 14, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 15, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 17, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 18, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 18, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 18, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 18, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 18, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 18, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 23, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 31, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 31, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 31, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 30, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 30, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 30, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 30, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 30, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 30, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 30, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 30, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 30, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 30, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 30, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 30, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 30, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 32, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 32, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 32, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 32, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 32, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 32, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 32, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 32, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 32, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 32, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 32, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 32, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 32, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `comments_user_id_foreign` (`user_id`),
  KEY `comments_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `project_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 30, 1, 'testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 30, 1, 'test again', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 32, 1, 'hfxhfhxchfe', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `content_path` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `galleries_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `project_id`, `type`, `content_path`, `created_at`, `updated_at`) VALUES
(1, 15, 'image/png', '10.png', '2016-02-03 05:39:44', '2016-02-03 05:39:44'),
(2, 18, 'image/png', 'paint-jamaica-2.png', '2016-04-13 20:50:18', '2016-04-13 20:50:18');

-- --------------------------------------------------------

--
-- Table structure for table `google_maps_info`
--

CREATE TABLE IF NOT EXISTS `google_maps_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) NOT NULL,
  `place_id` varchar(255) NOT NULL,
  `log` decimal(10,6) NOT NULL,
  `lat` decimal(10,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `place_id` (`place_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `google_maps_info`
--

INSERT INTO `google_maps_info` (`id`, `location`, `place_id`, `log`, `lat`, `created_at`, `updated_at`) VALUES
(1, 'Manchester Parish, Jamaica', 'ChIJ2WhaWlm4244RRc0ndo73kdY', -77.516079, 18.066965, '2015-04-19 22:35:00', '2015-04-19 18:35:00'),
(2, 'Mandeville, Jamaica', 'ChIJt_eI1DC-244R7dSl7GBNWPo', -77.500000, 18.033333, '2015-04-29 23:18:53', '2015-04-29 19:18:53'),
(3, 'St Ann Parish, Jamaica', 'ChIJe9l9kzVS2o4RllOWGihszuE', -77.240515, 18.328143, '2015-08-25 16:28:10', '2015-08-25 12:28:10'),
(4, 'Kingston Parish, Jamaica', 'ChIJPToenYJA244RNbz9cV3Tl78', -76.782702, 17.968327, '2015-12-03 08:47:22', '2015-12-03 00:47:22'),
(5, 'Falmouth, Jamaica', 'ChIJa6vPw_gz2o4RNiV9z_h1cZQ', -77.656476, 18.492846, '2015-12-03 20:43:32', '2015-12-03 12:43:32'),
(6, 'Port Antonio, Jamaica', 'ChIJvyh0wjvSxI4RR08sJxJWxTU', -76.447632, 18.171276, '2015-12-03 23:32:44', '2015-12-03 15:32:44'),
(7, 'Las Vegas, NV, USA', 'ChIJ0X31pIK3voARo3mz1ebVzDo', -115.139830, 36.169941, '2015-12-09 01:35:30', '2015-12-08 17:35:30'),
(8, 'Fremont St, Las Vegas, NV, USA', 'ChIJlZRFKKTEyIARu6KuqW6fRIw', -115.121903, 36.161175, '2016-02-03 03:53:44', '2016-02-02 19:53:44'),
(9, 'Kingston, ON, Canada', 'ChIJqY5AdAar0kwR6tnWFXdJpXY', -76.485954, 44.231172, '2016-03-22 22:36:02', '2016-03-22 15:36:02'),
(10, 'Kinley Dr, Nellis AFB, NV 89191, USA', 'ChIJNdxFVgHdyIARnHYf_H19wGI', -115.055965, 36.219891, '2016-03-24 22:51:39', '2016-03-24 15:51:39'),
(11, 'Los Angeles, CA, USA', 'ChIJE9on3F3HwoAR9AhGJW_fL-I', -118.243685, 34.052234, '2016-03-24 23:55:52', '2016-03-24 16:55:52'),
(12, 'Kingston, Jamaica', 'ChIJodLehJc_244RQK-3q78h8yQ', -76.809904, 18.017874, '2016-04-16 21:44:03', '2016-04-16 14:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `heading`
--

CREATE TABLE IF NOT EXISTS `heading` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `heading`
--

INSERT INTO `heading` (`id`, `name`, `section`, `created_at`, `updated_at`) VALUES
(1, 'Staff Picks', 'first_head', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Trending now', 'second_head', '0000-00-00 00:00:00', '2015-12-14 05:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=327 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `action`, `created_at`, `updated_at`) VALUES
(6, 2, 'generate report for registered users', '2015-04-02 18:45:32', '0000-00-00 00:00:00'),
(7, 2, 'download csv report for registered users', '2015-04-02 18:45:47', '0000-00-00 00:00:00'),
(8, 2, 'generate report for donor', '2015-04-02 18:46:28', '0000-00-00 00:00:00'),
(9, 2, 'download csv report for donor', '2015-04-02 18:46:38', '0000-00-00 00:00:00'),
(10, 2, 'generate report for project views', '2015-04-02 18:50:46', '0000-00-00 00:00:00'),
(11, 2, 'generate report for project views', '2015-04-11 18:57:43', '0000-00-00 00:00:00'),
(12, 1, 'generate report for registered users', '2015-04-19 18:07:06', '0000-00-00 00:00:00'),
(13, 1, 'add another one hit the dust to homepage', '2015-04-19 18:30:09', '0000-00-00 00:00:00'),
(14, 1, 'generate report for registered users', '2015-04-19 18:30:46', '0000-00-00 00:00:00'),
(15, 1, 'modified project kool no', '2015-04-19 18:37:13', '0000-00-00 00:00:00'),
(16, 1, 'generate report for fully funded', '2015-04-19 18:37:45', '0000-00-00 00:00:00'),
(17, 1, 'generate report for registered users', '2015-04-19 18:39:06', '0000-00-00 00:00:00'),
(18, 1, 'download pdf all logs', '2015-05-05 16:11:53', '0000-00-00 00:00:00'),
(19, 1, 'download pdf all logs', '2015-05-05 16:13:08', '0000-00-00 00:00:00'),
(20, 1, 'download csv all logs', '2015-05-05 19:39:50', '0000-00-00 00:00:00'),
(21, 20, 'generate report for registered users', '2015-05-07 17:54:10', '0000-00-00 00:00:00'),
(22, 20, 'generate report for registered users', '2015-05-13 16:25:25', '0000-00-00 00:00:00'),
(23, 2, 'generate report for registered users', '2015-05-28 20:01:57', '0000-00-00 00:00:00'),
(24, 17, 'Login into system', '2015-06-29 22:17:55', '0000-00-00 00:00:00'),
(25, 2, 'generate report for registered users', '2015-07-08 21:40:39', '0000-00-00 00:00:00'),
(26, 2, 'edited page home', '2015-07-13 20:51:21', '0000-00-00 00:00:00'),
(27, 2, 'edited page home', '2015-07-13 20:53:21', '0000-00-00 00:00:00'),
(28, 2, 'edited page home', '2015-07-13 20:54:03', '0000-00-00 00:00:00'),
(29, 2, 'edited page home', '2015-07-13 20:54:40', '0000-00-00 00:00:00'),
(30, 2, 'reset page home', '2015-07-13 20:55:18', '0000-00-00 00:00:00'),
(31, 2, 'edited page home', '2015-07-13 20:57:41', '0000-00-00 00:00:00'),
(32, 2, 'reset page home', '2015-07-13 20:57:59', '0000-00-00 00:00:00'),
(33, 2, 'Login into system', '2015-07-15 13:59:00', '0000-00-00 00:00:00'),
(34, 2, 'add Build the HIVE to homepage', '2015-07-15 14:38:58', '0000-00-00 00:00:00'),
(35, 2, 'add Anothr one hit the dust Tv shows2 to homepage', '2015-07-15 14:39:07', '0000-00-00 00:00:00'),
(36, 2, 'add Helpus to homepage', '2015-07-15 14:45:17', '0000-00-00 00:00:00'),
(37, 2, 'add another one hit the dust to homepage', '2015-07-15 14:45:27', '0000-00-00 00:00:00'),
(38, 2, 'add Build the HIVE to homepage', '2015-07-15 14:45:33', '0000-00-00 00:00:00'),
(39, 2, 'add another one hit the dust to homepage', '2015-07-15 14:45:39', '0000-00-00 00:00:00'),
(40, 2, 'create new user account for Kimberly Manradge', '2015-07-23 01:16:36', '0000-00-00 00:00:00'),
(41, 2, 'delete user account for Kimberly Manradge', '2015-07-23 01:21:01', '0000-00-00 00:00:00'),
(42, 2, 'create new user account for Kimberly Manradge', '2015-07-23 01:21:52', '0000-00-00 00:00:00'),
(43, 2, 'delete user account for Kimberly Manradge', '2015-07-23 01:56:33', '0000-00-00 00:00:00'),
(44, 2, 'create new user account for Kimberly Manradge', '2015-07-23 02:00:07', '0000-00-00 00:00:00'),
(45, 2, 'created page Test', '2015-07-23 04:11:30', '0000-00-00 00:00:00'),
(46, 2, 'created page Fast 7 Soundtrack', '2015-07-23 04:24:36', '0000-00-00 00:00:00'),
(47, 2, 'edited page Edited Test', '2015-07-23 12:06:50', '0000-00-00 00:00:00'),
(48, 2, 'delete page Edited Test', '2015-07-23 12:19:02', '0000-00-00 00:00:00'),
(49, 2, 'generate report for registered users', '2015-07-23 14:16:17', '0000-00-00 00:00:00'),
(50, 2, 'generate report for donor', '2015-07-23 14:16:23', '0000-00-00 00:00:00'),
(51, 2, 'generate report for project views', '2015-07-23 14:16:59', '0000-00-00 00:00:00'),
(52, 2, 'generate report for project donation', '2015-07-23 14:17:14', '0000-00-00 00:00:00'),
(53, 2, 'generate report for partial funded', '2015-07-23 14:17:26', '0000-00-00 00:00:00'),
(54, 2, 'generate report for fully funded', '2015-07-23 14:17:32', '0000-00-00 00:00:00'),
(55, 2, 'generate report for donor', '2015-07-23 14:17:37', '0000-00-00 00:00:00'),
(56, 2, 'generate report for registered users', '2015-07-23 14:17:53', '0000-00-00 00:00:00'),
(57, 2, 'add Helpus to homepage', '2015-07-23 15:00:25', '0000-00-00 00:00:00'),
(58, 2, 'add Build the HIVE to homepage', '2015-07-23 15:02:17', '0000-00-00 00:00:00'),
(59, 2, 'generate report for registered users', '2015-07-23 15:29:37', '0000-00-00 00:00:00'),
(60, 2, 'generate report for donor', '2015-07-23 15:29:52', '0000-00-00 00:00:00'),
(61, 2, 'generate report for registered users', '2015-07-23 15:30:06', '0000-00-00 00:00:00'),
(62, 2, 'generate report for donor', '2015-07-23 15:33:32', '0000-00-00 00:00:00'),
(63, 2, 'generate report for fully funded', '2015-07-23 15:34:24', '0000-00-00 00:00:00'),
(64, 2, 'generate report for partial funded', '2015-07-23 15:34:33', '0000-00-00 00:00:00'),
(65, 2, 'generate report for project donation', '2015-07-23 15:34:40', '0000-00-00 00:00:00'),
(66, 2, 'generate report for project views', '2015-07-23 15:35:43', '0000-00-00 00:00:00'),
(67, 2, 'download csv report for project views', '2015-07-23 15:39:16', '0000-00-00 00:00:00'),
(68, 2, 'download pdf report for project views', '2015-07-23 15:40:00', '0000-00-00 00:00:00'),
(69, 2, 'add Helpus to homepage', '2015-07-23 17:10:06', '0000-00-00 00:00:00'),
(70, 2, 'delete user account for anthony lindsay', '2015-08-03 20:42:59', '0000-00-00 00:00:00'),
(71, 2, 'created page Netballers', '2015-08-03 21:28:17', '0000-00-00 00:00:00'),
(72, 2, 'created page Facebook', '2015-08-04 20:11:45', '0000-00-00 00:00:00'),
(73, 2, 'created page Instagram', '2015-08-04 20:12:40', '0000-00-00 00:00:00'),
(74, 2, 'created page YouTube', '2015-08-04 20:15:48', '0000-00-00 00:00:00'),
(75, 2, 'created page Twitter', '2015-08-04 20:16:50', '0000-00-00 00:00:00'),
(76, 2, 'delete page facebook', '2015-08-04 20:17:02', '0000-00-00 00:00:00'),
(77, 2, 'created page Google+ ', '2015-08-04 20:18:16', '0000-00-00 00:00:00'),
(78, 2, 'created page Contact Us', '2015-08-04 20:30:31', '0000-00-00 00:00:00'),
(79, 2, 'delete page Contact Us', '2015-08-04 20:32:59', '0000-00-00 00:00:00'),
(80, 2, 'add Helpus to homepage', '2015-08-04 21:31:03', '0000-00-00 00:00:00'),
(81, 2, 'add another one hit the dust to homepage', '2015-08-04 21:31:13', '0000-00-00 00:00:00'),
(82, 2, 'add Another one hit the dust Tv shows2 to homepage', '2015-08-04 21:31:19', '0000-00-00 00:00:00'),
(83, 2, 'add kool no to homepage', '2015-08-04 21:31:25', '0000-00-00 00:00:00'),
(84, 2, 'generate report for registered users', '2015-08-04 21:32:11', '0000-00-00 00:00:00'),
(85, 2, 'generate report for registered users', '2015-08-04 21:32:11', '0000-00-00 00:00:00'),
(86, 2, 'generate report for registered users', '2015-08-04 21:32:17', '0000-00-00 00:00:00'),
(87, 2, 'generate report for project views', '2015-08-04 21:32:27', '0000-00-00 00:00:00'),
(88, 2, 'generate report for registered users', '2015-08-04 21:56:05', '0000-00-00 00:00:00'),
(89, 2, 'generate report for donor', '2015-08-04 21:56:08', '0000-00-00 00:00:00'),
(90, 2, 'generate report for fully funded', '2015-08-04 21:56:14', '0000-00-00 00:00:00'),
(91, 2, 'generate report for project donation', '2015-08-04 21:56:18', '0000-00-00 00:00:00'),
(92, 2, 'generate report for project views', '2015-08-04 21:56:21', '0000-00-00 00:00:00'),
(93, 2, 'generate report for registered users', '2015-08-04 21:56:59', '0000-00-00 00:00:00'),
(94, 2, 'download pdf report for registered users', '2015-08-04 21:58:20', '0000-00-00 00:00:00'),
(95, 2, 'generate report for registered users', '2015-08-04 21:59:38', '0000-00-00 00:00:00'),
(96, 2, 'download csv report for registered users', '2015-08-04 21:59:57', '0000-00-00 00:00:00'),
(97, 2, 'created page Testing 1 3', '2015-08-17 14:15:42', '0000-00-00 00:00:00'),
(98, 2, 'created page Testing 1 3', '2015-08-17 14:15:49', '0000-00-00 00:00:00'),
(99, 2, 'delete page Testing 1 3', '2015-08-17 14:17:46', '0000-00-00 00:00:00'),
(100, 2, 'delete page Testing 1 3', '2015-08-17 14:17:56', '0000-00-00 00:00:00'),
(101, 2, 'edit user account for Rushio Billings', '2015-08-17 14:19:09', '0000-00-00 00:00:00'),
(102, 2, 'edit user account for Anquani Campbell', '2015-08-17 16:14:58', '0000-00-00 00:00:00'),
(103, 2, 'edit user account for Anquane Campbell', '2015-08-17 16:15:28', '0000-00-00 00:00:00'),
(104, 2, 'delete user account for Kimberly Manradge', '2015-08-17 16:36:11', '0000-00-00 00:00:00'),
(105, 2, 'create new user account for Kimberly Manradge', '2015-08-17 16:36:50', '0000-00-00 00:00:00'),
(106, 2, 'created page Test Page', '2015-08-17 16:44:58', '0000-00-00 00:00:00'),
(107, 2, 'edit user account for Anquane Campbell', '2015-08-19 00:35:49', '0000-00-00 00:00:00'),
(108, 2, 'edit user account for Anquani Campbell', '2015-08-19 00:36:02', '0000-00-00 00:00:00'),
(109, 2, 'edit user account for Anquane Campbell', '2015-08-19 00:36:41', '0000-00-00 00:00:00'),
(110, 2, 'edit user account for Kornel Lewis', '2015-08-19 00:36:56', '0000-00-00 00:00:00'),
(111, 2, 'delete user account for Kimberly Manradge', '2015-08-19 00:37:54', '0000-00-00 00:00:00'),
(112, 2, 'create new user account for Kimberly Manradge', '2015-08-19 00:39:51', '0000-00-00 00:00:00'),
(113, 2, 'generate report for fully funded', '2015-08-21 14:06:42', '0000-00-00 00:00:00'),
(114, 2, 'generate report for project views', '2015-08-21 14:06:48', '0000-00-00 00:00:00'),
(115, 2, 'generate report for donor', '2015-08-21 14:06:54', '0000-00-00 00:00:00'),
(116, 2, 'generate report for registered users', '2015-08-21 14:07:00', '0000-00-00 00:00:00'),
(117, 2, 'generate report for partial funded', '2015-08-21 14:07:07', '0000-00-00 00:00:00'),
(118, 2, 'generate report for project donation', '2015-08-21 14:07:13', '0000-00-00 00:00:00'),
(119, 2, 'delete page Fast 7 Soundtrack', '2015-08-25 14:56:02', '0000-00-00 00:00:00'),
(120, 2, 'created page What is ISupportJamaica?', '2015-08-25 15:04:03', '0000-00-00 00:00:00'),
(121, 2, 'delete page Test Page', '2015-08-25 15:04:39', '0000-00-00 00:00:00'),
(122, 2, 'edited page Faq', '2015-08-25 15:06:27', '0000-00-00 00:00:00'),
(123, 2, 'created page Contact Us', '2015-08-25 15:09:28', '0000-00-00 00:00:00'),
(124, 2, 'edited page Contact Us', '2015-08-25 15:10:49', '0000-00-00 00:00:00'),
(125, 2, 'edited page Contact Us', '2015-08-25 15:12:30', '0000-00-00 00:00:00'),
(126, 2, 'edited page Contact Us', '2015-08-25 15:13:13', '0000-00-00 00:00:00'),
(127, 2, 'created page How It Works', '2015-08-25 15:17:46', '0000-00-00 00:00:00'),
(128, 2, 'create new user account for Phillip Lindsay', '2015-08-25 15:18:17', '0000-00-00 00:00:00'),
(129, 2, 'created page Start A Campaign', '2015-08-25 15:23:27', '0000-00-00 00:00:00'),
(130, 2, 'delete page Earned it', '2015-08-25 15:25:32', '0000-00-00 00:00:00'),
(131, 2, 'edited page Faq', '2015-08-25 15:34:34', '0000-00-00 00:00:00'),
(132, 2, 'created page Background', '2015-08-25 15:44:18', '0000-00-00 00:00:00'),
(133, 2, 'created page Terms and Conditions', '2015-08-25 15:53:30', '0000-00-00 00:00:00'),
(134, 2, 'edited page FAQ', '2015-08-25 16:09:09', '0000-00-00 00:00:00'),
(135, 2, 'edited page FAQ', '2015-08-25 16:21:47', '0000-00-00 00:00:00'),
(136, 2, 'generate report for registered users', '2015-08-27 04:43:06', '0000-00-00 00:00:00'),
(137, 2, 'download pdf report for registered users', '2015-08-27 04:43:19', '0000-00-00 00:00:00'),
(138, 2, 'generate report for registered users', '2015-08-27 04:43:58', '0000-00-00 00:00:00'),
(139, 2, 'generate report for registered users', '2015-08-27 04:44:08', '0000-00-00 00:00:00'),
(140, 2, 'generate report for registered users', '2015-08-27 04:44:10', '0000-00-00 00:00:00'),
(141, 2, 'generate report for registered users', '2015-08-27 04:44:12', '0000-00-00 00:00:00'),
(142, 2, 'edited page Start A Campaign', '2015-08-28 03:05:09', '0000-00-00 00:00:00'),
(143, 2, 'edited page FAQ', '2015-08-28 14:39:53', '0000-00-00 00:00:00'),
(144, 2, 'edited page FAQ', '2015-08-28 14:42:32', '0000-00-00 00:00:00'),
(145, 2, 'Login into system', '2015-08-28 14:43:26', '0000-00-00 00:00:00'),
(146, 2, 'edited page FAQ', '2015-08-28 14:48:40', '0000-00-00 00:00:00'),
(147, 2, 'edited page FAQ', '2015-08-28 15:23:44', '0000-00-00 00:00:00'),
(148, 2, 'edited page home', '2015-08-28 15:26:31', '0000-00-00 00:00:00'),
(149, 2, 'edited page home', '2015-08-28 15:37:02', '0000-00-00 00:00:00'),
(150, 2, 'reset page home', '2015-08-28 15:37:39', '0000-00-00 00:00:00'),
(151, 2, 'edited page home', '2015-08-28 15:38:11', '0000-00-00 00:00:00'),
(152, 2, 'edited page home', '2015-08-28 15:43:24', '0000-00-00 00:00:00'),
(153, 2, 'edited page FAQ', '2015-08-28 15:46:25', '0000-00-00 00:00:00'),
(154, 2, 'edited page FAQ', '2015-08-28 18:36:33', '0000-00-00 00:00:00'),
(155, 2, 'edited page home', '2015-08-28 18:38:49', '0000-00-00 00:00:00'),
(156, 2, 'reset page home', '2015-08-28 18:39:13', '0000-00-00 00:00:00'),
(157, 2, 'edited page Contact Us', '2015-08-28 18:41:39', '0000-00-00 00:00:00'),
(158, 2, 'edited page Contact Us', '2015-08-28 18:52:19', '0000-00-00 00:00:00'),
(159, 2, 'edited page Contact Us', '2015-08-28 18:53:05', '0000-00-00 00:00:00'),
(160, 2, 'edited page Contact Us', '2015-08-28 18:54:04', '0000-00-00 00:00:00'),
(161, 2, 'edited page Contact Us', '2015-08-28 18:54:42', '0000-00-00 00:00:00'),
(162, 2, 'edited page Contact Us', '2015-08-28 18:56:24', '0000-00-00 00:00:00'),
(163, 2, 'edited page home', '2015-08-28 19:00:26', '0000-00-00 00:00:00'),
(164, 2, 'edited page home', '2015-08-28 19:03:28', '0000-00-00 00:00:00'),
(165, 2, 'edited page Contact Us', '2015-08-28 19:08:07', '0000-00-00 00:00:00'),
(166, 2, 'edited page Contact Us', '2015-08-28 19:08:46', '0000-00-00 00:00:00'),
(167, 2, 'edited page Terms and Conditions', '2015-08-28 19:11:58', '0000-00-00 00:00:00'),
(168, 2, 'edited page Contact Us', '2015-08-28 20:01:50', '0000-00-00 00:00:00'),
(169, 2, 'edited page Contact Us', '2015-08-28 20:05:39', '0000-00-00 00:00:00'),
(170, 2, 'edited page Contact Us', '2015-08-28 20:08:14', '0000-00-00 00:00:00'),
(171, 2, 'edited page Contact Us', '2015-08-28 20:09:31', '0000-00-00 00:00:00'),
(172, 2, 'edited page Contact Us', '2015-08-28 20:10:09', '0000-00-00 00:00:00'),
(173, 2, 'edited page Terms and Conditions', '2015-08-28 20:12:16', '0000-00-00 00:00:00'),
(174, 2, 'edited page Terms and Conditions', '2015-08-28 20:16:43', '0000-00-00 00:00:00'),
(175, 2, 'edited page Terms and Conditions', '2015-08-28 20:17:30', '0000-00-00 00:00:00'),
(176, 2, 'edited page Background', '2015-08-28 20:20:00', '0000-00-00 00:00:00'),
(177, 2, 'edited page Background', '2015-08-28 20:20:37', '0000-00-00 00:00:00'),
(178, 2, 'edited page Background', '2015-08-28 20:28:07', '0000-00-00 00:00:00'),
(179, 2, 'edited page Background', '2015-08-28 20:51:51', '0000-00-00 00:00:00'),
(180, 2, 'edited page Background', '2015-08-28 20:52:53', '0000-00-00 00:00:00'),
(181, 2, 'edited page FAQ', '2015-08-28 21:10:46', '0000-00-00 00:00:00'),
(182, 2, 'edited page Background', '2015-08-28 21:15:03', '0000-00-00 00:00:00'),
(183, 2, 'edited page What is ISupportJamaica?', '2015-08-28 21:38:52', '0000-00-00 00:00:00'),
(184, 2, 'edited page What is ISupportJamaica?', '2015-08-28 21:45:04', '0000-00-00 00:00:00'),
(185, 2, 'edited page What is ISupportJamaica?', '2015-08-28 21:49:56', '0000-00-00 00:00:00'),
(186, 2, 'edited page What is ISupportJamaica?', '2015-08-28 21:50:43', '0000-00-00 00:00:00'),
(187, 2, 'edited page What is ISupportJamaica?', '2015-08-28 21:51:50', '0000-00-00 00:00:00'),
(188, 2, 'edited page FAQ', '2015-08-28 21:54:45', '0000-00-00 00:00:00'),
(189, 2, 'edited page Contact Us', '2015-08-28 21:56:04', '0000-00-00 00:00:00'),
(190, 2, 'edited page Terms and Conditions', '2015-08-28 22:01:38', '0000-00-00 00:00:00'),
(191, 2, 'created page Partners / Angel Investors', '2015-08-28 22:20:12', '0000-00-00 00:00:00'),
(192, 2, 'edited page Partners / Angel Investors', '2015-08-28 22:20:35', '0000-00-00 00:00:00'),
(193, 2, 'edited page Partners / Angel Investors', '2015-08-28 22:21:13', '0000-00-00 00:00:00'),
(194, 2, 'edited page Partners / Angel Investors', '2015-08-28 22:23:41', '0000-00-00 00:00:00'),
(195, 2, 'edited page Partners / Angel Investors', '2015-08-28 22:27:09', '0000-00-00 00:00:00'),
(196, 2, 'edited page Partners / Angel Investors', '2015-08-29 04:53:11', '0000-00-00 00:00:00'),
(197, 2, 'edited page Partners / Angel Investors', '2015-08-29 04:55:32', '0000-00-00 00:00:00'),
(198, 2, 'edited page Contact Us', '2015-08-29 04:56:53', '0000-00-00 00:00:00'),
(199, 2, 'edited page Background', '2015-08-29 04:57:40', '0000-00-00 00:00:00'),
(200, 2, 'edited page home', '2015-08-29 05:00:50', '0000-00-00 00:00:00'),
(201, 2, 'edited page home', '2015-08-29 05:01:52', '0000-00-00 00:00:00'),
(202, 2, 'edited page Contact Us', '2015-08-29 09:55:00', '0000-00-00 00:00:00'),
(203, 2, 'edited page home', '2015-08-29 09:56:53', '0000-00-00 00:00:00'),
(204, 2, 'edited page home', '2015-08-29 09:57:21', '0000-00-00 00:00:00'),
(205, 2, 'edited page FAQ', '2015-08-29 12:14:37', '0000-00-00 00:00:00'),
(206, 2, 'edited page Partners / Angel Investors', '2015-08-30 17:53:53', '0000-00-00 00:00:00'),
(207, 2, 'edited page Partners / Angel Investors', '2015-08-30 17:56:30', '0000-00-00 00:00:00'),
(208, 2, 'edited page Partners / Angel Investors', '2015-08-30 17:59:00', '0000-00-00 00:00:00'),
(209, 2, 'edited page Partners / Angel Investors', '2015-08-30 18:00:01', '0000-00-00 00:00:00'),
(210, 2, 'edited page FAQ', '2015-08-30 18:20:25', '0000-00-00 00:00:00'),
(211, 2, 'edited page FAQ', '2015-08-30 18:25:25', '0000-00-00 00:00:00'),
(212, 2, 'edited page FAQ', '2015-08-30 18:28:51', '0000-00-00 00:00:00'),
(213, 2, 'edited page FAQ', '2015-08-30 18:29:27', '0000-00-00 00:00:00'),
(214, 2, 'edited page FAQ', '2015-08-30 18:30:34', '0000-00-00 00:00:00'),
(215, 2, 'edited page FAQ', '2015-08-30 18:32:35', '0000-00-00 00:00:00'),
(216, 2, 'edited page FAQ', '2015-08-30 18:34:28', '0000-00-00 00:00:00'),
(217, 2, 'edited page FAQ', '2015-08-30 18:40:14', '0000-00-00 00:00:00'),
(218, 2, 'edited page Terms and Conditions', '2015-08-30 18:43:15', '0000-00-00 00:00:00'),
(219, 2, 'edited page Terms and Conditions', '2015-08-30 18:48:08', '0000-00-00 00:00:00'),
(220, 2, 'edited page Terms and Conditions', '2015-08-30 18:49:34', '0000-00-00 00:00:00'),
(221, 2, 'edited page Terms and Conditions', '2015-08-30 18:57:41', '0000-00-00 00:00:00'),
(222, 2, 'edited page Partners / Angel Investors', '2015-08-30 18:59:28', '0000-00-00 00:00:00'),
(223, 2, 'edited page Partners / Angel Investors', '2015-08-30 19:00:16', '0000-00-00 00:00:00'),
(224, 2, 'edited page Terms and Conditions', '2015-08-30 19:00:55', '0000-00-00 00:00:00'),
(225, 2, 'edited page FAQ', '2015-08-30 19:23:51', '0000-00-00 00:00:00'),
(226, 2, 'edited page Contact Us', '2015-08-31 01:43:48', '0000-00-00 00:00:00'),
(227, 2, 'edited page Contact Us', '2015-08-31 01:44:20', '0000-00-00 00:00:00'),
(228, 2, 'edited page Partners / Angel Investors', '2015-08-31 01:45:15', '0000-00-00 00:00:00'),
(229, 2, 'edited page What is ISupportJamaica?', '2015-08-31 01:50:17', '0000-00-00 00:00:00'),
(230, 2, 'edited page What is ISupportJamaica?', '2015-08-31 01:51:12', '0000-00-00 00:00:00'),
(231, 2, 'edited page What is ISupportJamaica?', '2015-08-31 01:55:06', '0000-00-00 00:00:00'),
(232, 2, 'edited page What is ISupportJamaica?', '2015-08-31 01:56:05', '0000-00-00 00:00:00'),
(233, 2, 'edited page What is ISupportJamaica?', '2015-08-31 01:57:02', '0000-00-00 00:00:00'),
(234, 2, 'delete user account for Phillip Lindsay', '2015-08-31 01:59:19', '0000-00-00 00:00:00'),
(235, 2, 'delete user account for Lisandra Rickards', '2015-08-31 01:59:36', '0000-00-00 00:00:00'),
(236, 2, 'delete user account for yuyuyuy jkjkj', '2015-08-31 01:59:46', '0000-00-00 00:00:00'),
(237, 2, 'edited page Terms and Conditions', '2015-08-31 02:11:28', '0000-00-00 00:00:00'),
(238, 2, 'edited page Terms and Conditions', '2015-08-31 02:12:45', '0000-00-00 00:00:00'),
(239, 2, 'edited page Terms and Conditions', '2015-08-31 02:13:06', '0000-00-00 00:00:00'),
(240, 2, 'edited page Background', '2015-08-31 02:33:19', '0000-00-00 00:00:00'),
(241, 2, 'edited page Background', '2015-08-31 02:33:59', '0000-00-00 00:00:00'),
(242, 2, 'edited page Our Partners', '2015-08-31 02:52:49', '0000-00-00 00:00:00'),
(243, 2, 'created page Successful Campaigns', '2015-08-31 03:01:07', '0000-00-00 00:00:00'),
(244, 2, 'edited page Our Partners', '2015-08-31 14:55:07', '0000-00-00 00:00:00'),
(245, 2, 'delete page Our Partners', '2015-08-31 14:55:58', '0000-00-00 00:00:00'),
(246, 2, 'created page Our Partners', '2015-08-31 14:59:28', '0000-00-00 00:00:00'),
(247, 2, 'edited page Our Partners', '2015-08-31 15:00:01', '0000-00-00 00:00:00'),
(248, 2, 'edited page Our Partners', '2015-08-31 15:00:54', '0000-00-00 00:00:00'),
(249, 2, 'edited page Our Partners', '2015-08-31 15:01:15', '0000-00-00 00:00:00'),
(250, 2, 'edited page Our Partners', '2015-08-31 15:03:05', '0000-00-00 00:00:00'),
(251, 2, 'edited page Our Partners', '2015-08-31 15:07:05', '0000-00-00 00:00:00'),
(252, 2, 'edited page Our Partners', '2015-08-31 15:07:56', '0000-00-00 00:00:00'),
(253, 2, 'edited page Our Partners', '2015-09-01 20:46:17', '0000-00-00 00:00:00'),
(254, 2, 'edited page Contact Us', '2015-09-01 20:48:59', '0000-00-00 00:00:00'),
(255, 2, 'edited page Contact Us', '2015-09-01 20:49:34', '0000-00-00 00:00:00'),
(256, 2, 'edited page FAQ', '2015-09-01 20:50:12', '0000-00-00 00:00:00'),
(257, 2, 'edited page Our Partners', '2015-09-01 20:51:16', '0000-00-00 00:00:00'),
(258, 2, 'edited page Background', '2015-09-01 20:51:43', '0000-00-00 00:00:00'),
(259, 2, 'edited page Contact Us', '2015-09-02 15:44:09', '0000-00-00 00:00:00'),
(260, 2, 'edited page Contact Us', '2015-09-02 15:47:19', '0000-00-00 00:00:00'),
(261, 2, 'edited page Contact Us', '2015-09-02 15:54:44', '0000-00-00 00:00:00'),
(262, 2, 'edited page Contact Us', '2015-09-02 15:57:42', '0000-00-00 00:00:00'),
(263, 2, 'edited page Contact Us', '2015-09-02 15:59:05', '0000-00-00 00:00:00'),
(264, 2, 'edited page Contact Us', '2015-09-02 16:01:32', '0000-00-00 00:00:00'),
(265, 2, 'edited page Contact Us', '2015-09-02 16:02:00', '0000-00-00 00:00:00'),
(266, 2, 'edited page Contact Us', '2015-09-02 16:18:07', '0000-00-00 00:00:00'),
(267, 2, 'edited page Terms and Conditions', '2015-09-03 01:24:19', '0000-00-00 00:00:00'),
(268, 2, 'edit user account for Kimberly Manradge', '2015-09-03 03:50:37', '0000-00-00 00:00:00'),
(269, 2, 'edit user account for dwayne brown', '2015-09-03 03:51:00', '0000-00-00 00:00:00'),
(270, 2, 'create new user account for Phillip Lindsay', '2015-09-03 03:52:14', '0000-00-00 00:00:00'),
(271, 31, 'Login into system', '2015-12-02 22:53:51', '0000-00-00 00:00:00'),
(272, 31, 'Login into system', '2015-12-03 07:29:42', '0000-00-00 00:00:00'),
(273, 30, 'Login into system', '2015-12-04 20:41:56', '0000-00-00 00:00:00'),
(274, 30, 'delete project: testing', '2015-12-07 22:28:59', '0000-00-00 00:00:00'),
(275, 30, 'modified project Blown up', '2015-12-08 22:29:50', '0000-00-00 00:00:00'),
(276, 30, 'modified project X-RAY MACHINE ', '2015-12-30 01:52:56', '0000-00-00 00:00:00'),
(277, 30, 'generate report for registered users', '2016-02-03 02:56:08', '0000-00-00 00:00:00'),
(278, 30, 'generate report for project views', '2016-02-03 02:56:19', '0000-00-00 00:00:00'),
(279, 30, 'generate report for project views', '2016-02-03 02:57:09', '0000-00-00 00:00:00'),
(280, 30, 'download pdf report for project views', '2016-02-03 02:57:20', '0000-00-00 00:00:00'),
(281, 30, 'download pdf all logs', '2016-02-03 03:00:24', '0000-00-00 00:00:00'),
(282, 30, 'Login into system', '2016-02-17 21:06:18', '0000-00-00 00:00:00'),
(283, 30, 'Login into system', '2016-02-18 01:42:12', '0000-00-00 00:00:00'),
(284, 30, 'modified project Test ISJ', '2016-03-11 01:03:02', '0000-00-00 00:00:00'),
(285, 30, 'edit user account for Kornel-Sims Lewis', '2016-03-11 20:36:16', '0000-00-00 00:00:00'),
(286, 30, 'edit user account for Shanice Clarke', '2016-03-11 20:47:12', '0000-00-00 00:00:00'),
(287, 30, 'edit user account for dwayne brown', '2016-03-11 20:51:53', '0000-00-00 00:00:00'),
(288, 30, 'Login into system', '2016-03-22 20:09:29', '0000-00-00 00:00:00'),
(289, 30, 'edit user account for Anquane Ann Campbell', '2016-03-22 21:17:00', '0000-00-00 00:00:00'),
(290, 30, 'download csv logs between 2015-01-22 and 2015-01-31', '2016-03-25 00:11:39', '0000-00-00 00:00:00'),
(291, 30, 'download csv logs between 2015-01-1 and 2015-08-1', '2016-03-25 00:12:36', '0000-00-00 00:00:00'),
(292, 30, 'edited page Background', '2016-04-13 22:07:56', '0000-00-00 00:00:00'),
(293, 30, 'edited page Background', '2016-04-13 22:11:18', '0000-00-00 00:00:00'),
(294, 30, 'edited page FAQ', '2016-04-19 20:16:52', '0000-00-00 00:00:00'),
(295, 30, 'edited page Frequent asked questions', '2016-04-19 20:18:10', '0000-00-00 00:00:00'),
(296, 30, 'edited page FAQ', '2016-04-19 20:19:23', '0000-00-00 00:00:00'),
(297, 30, 'edited page Questions', '2016-04-19 20:48:26', '0000-00-00 00:00:00'),
(298, 30, 'edited page FAQ', '2016-04-19 21:26:16', '0000-00-00 00:00:00'),
(299, 30, 'edited page FAQ', '2016-04-19 21:27:43', '0000-00-00 00:00:00'),
(300, 30, 'edited page Contact ISJ', '2016-04-19 21:32:36', '0000-00-00 00:00:00'),
(301, 30, 'edited page Contact Us', '2016-04-19 21:33:39', '0000-00-00 00:00:00'),
(302, 30, 'edited page Contact', '2016-04-19 21:35:02', '0000-00-00 00:00:00'),
(303, 30, 'edited page Contact Us', '2016-04-19 21:38:16', '0000-00-00 00:00:00'),
(304, 30, 'edited page FAQ', '2016-04-19 21:39:20', '0000-00-00 00:00:00'),
(305, 30, 'edited page FQA', '2016-04-19 21:45:53', '0000-00-00 00:00:00'),
(306, 30, 'edited page FAQ', '2016-04-19 21:49:04', '0000-00-00 00:00:00'),
(307, 30, 'edited page FAQ', '2016-04-19 21:50:10', '0000-00-00 00:00:00'),
(308, 30, 'edited page FAQ', '2016-04-19 21:51:51', '0000-00-00 00:00:00'),
(309, 30, 'edited page FAQ', '2016-04-19 22:12:54', '0000-00-00 00:00:00'),
(310, 30, 'edited page FAQ', '2016-04-19 22:13:18', '0000-00-00 00:00:00'),
(311, 30, 'edited page Contact ISJ', '2016-04-19 22:13:40', '0000-00-00 00:00:00'),
(312, 30, 'edited page Contact Us', '2016-04-19 22:16:32', '0000-00-00 00:00:00'),
(313, 30, 'edited page Terms', '2016-04-19 22:19:54', '0000-00-00 00:00:00'),
(314, 30, 'edited page Terms  and Conditions', '2016-04-19 22:20:50', '0000-00-00 00:00:00'),
(315, 30, 'edited page What is ISJ?', '2016-04-19 22:21:22', '0000-00-00 00:00:00'),
(316, 30, 'edited page What is ISupportJamaica?', '2016-04-19 22:22:06', '0000-00-00 00:00:00'),
(317, 30, 'edited page FAQ', '2016-04-20 20:17:04', '0000-00-00 00:00:00'),
(318, 30, 'edited page FAQ', '2016-04-20 20:17:06', '0000-00-00 00:00:00'),
(319, 30, 'edited page Contact Us', '2016-04-20 20:33:23', '0000-00-00 00:00:00'),
(320, 30, 'edited page Contact Us', '2016-04-22 20:10:54', '0000-00-00 00:00:00'),
(321, 30, 'edited page Contact Us', '2016-04-22 20:12:04', '0000-00-00 00:00:00'),
(322, 30, 'edited page Contact Us', '2016-04-22 20:12:14', '0000-00-00 00:00:00'),
(323, 30, 'edited page Contact Us', '2016-04-22 20:12:27', '0000-00-00 00:00:00'),
(324, 30, 'edited page Contact Us', '2016-04-22 20:13:39', '0000-00-00 00:00:00'),
(325, 30, 'edited page Terms  and Conditions', '2016-04-22 20:15:45', '0000-00-00 00:00:00'),
(326, 30, 'edited page FAQ', '2016-04-26 21:59:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2015_02_17_162954_create_users_table', 1),
('2015_02_19_201818_create_projects_table', 1),
('2015_02_19_211215_create_pledges_table', 1),
('2015_02_19_211410_create_rewards_table', 1),
('2015_02_19_211421_create_cart_table', 1),
('2015_02_24_134534_add_user_initial', 1),
('2015_02_24_140912_add_email_validate', 1),
('2015_03_03_033505_create_category_table', 2),
('2015_03_03_193109_add_security_questions', 3),
('2015_03_03_195549_add_security_question', 4),
('2015_03_03_201913_dump_security_questions', 4),
('2015_03_03_225631_category_follow', 5),
('2015_03_03_232437_add_few_categories', 5),
('2015_03_04_163454_create_uploads_table', 5),
('2015_03_04_164438_create_uploads_modify_table', 6),
('2015_03_04_165849_create_uploads_projectid_table', 7),
('2015_03_04_175023_create_project_startdate_table', 8),
('2015_03_04_192619_add_reward_dates', 9),
('2015_03_04_184557_update_pledges_table', 10),
('2015_03_04_212443_create_order_table', 11),
('2015_03_04_212637_create_order_items_table', 11),
('2015_03_04_213844_add_status_field_to_order_table', 11),
('2015_03_04_214041_add_anonymous_field_to_order_table', 11),
('2015_03_04_221755_add_project_id_order_item_table', 11),
('2015_03_06_195908_create_project_view_column', 11),
('2015_03_10_220147_add_user_accee_level', 12),
('2015_03_14_195827_create_page_section_table', 13),
('2015_03_14_200247_add_page_section_table', 13),
('2015_03_14_200603_create_pages_table', 13),
('2015_03_14_211127_add_discover_pages_table', 14),
('2015_03_15_002716_add_pages_columns', 15),
('2015_03_16_015824_add_page_section_other', 16),
('2015_03_16_020053_add_page_home', 17),
('2015_03_16_132402_create_projects_pick', 18),
('2015_03_16_142145_add_category_homepage_column', 19),
('2015_09_08_132809_create_banner_images_table', 20),
('2015_09_08_140936_create_banner_images_table', 21),
('2015_09_09_151617_add_about_project_column', 22),
('2015_09_10_095148_create_slides_table', 23),
('2015_09_12_091122_create_heading_table', 24),
('2015_09_12_091334_add_headings_table', 24),
('2015_09_23_115123_add_slider_url_to_slides_table', 25),
('2015_11_19_220445_add_use_of_funds_field_to_projects_table', 25),
('2015_12_03_011833_add_state_to_projects_table', 26),
('2015_12_03_012001_add_social_media_to_user_table', 26),
('2015_12_03_014148_alter_user_table', 27),
('2015_12_03_150044_add_project_column_is_project_to_watch', 28),
('2015_12_11_170752_create_comments_table', 29),
('2016_01_04_101941_create_galleries_table', 29),
('2016_01_10_151029_create_updates_table', 30),
('2016_02_03_144401_add_previous_state_to_projects_table', 30);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `created_at`, `updated_at`, `order_id`, `amount`, `project_id`) VALUES
(1, '2015-03-25 02:24:28', '2015-03-25 02:24:28', 1, 500.00, 1),
(2, '2015-04-02 22:54:57', '2015-04-02 22:54:57', 2, 50.00, 1),
(3, '2015-04-17 17:19:34', '2015-04-17 17:19:34', 3, 900.00, 2),
(4, '2015-05-08 21:11:21', '2015-05-08 21:11:21', 4, 50.00, 4),
(5, '2015-07-01 21:32:52', '2015-07-01 21:32:52', 5, 100.00, 9),
(6, '2015-07-01 21:35:10', '2015-07-01 21:35:10', 7, 1.00, 7),
(7, '2015-07-01 21:35:10', '2015-07-01 21:35:10', 7, 50.00, 2),
(8, '2015-08-05 01:24:28', '2015-08-05 01:24:28', 9, 10.00, 9),
(9, '2015-12-08 04:30:10', '2015-12-08 04:30:10', 10, 80.00, 10),
(10, '2015-12-08 10:58:27', '2015-12-08 10:58:27', 13, 70.00, 1),
(11, '2015-12-08 10:58:27', '2015-12-08 10:58:27', 13, 1.00, 8),
(12, '2015-12-08 10:58:27', '2015-12-08 10:58:27', 13, 40.00, 1),
(13, '2015-12-08 10:58:27', '2015-12-08 10:58:27', 13, 5.00, 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `paypal_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `anonymous` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `user_id`, `paypal_token`, `paypal_user_id`, `status`, `anonymous`) VALUES
(1, '2015-03-25 02:24:28', '2015-03-25 02:24:28', 12, '', '', 0, 0),
(2, '2015-04-02 22:54:57', '2015-04-02 22:54:57', 10, '', '', 0, 1),
(3, '2015-04-17 17:19:34', '2015-04-17 17:19:34', 15, '', '', 0, 0),
(4, '2015-05-08 21:11:21', '2015-05-08 21:11:21', 15, '', '', 0, 1),
(5, '2015-07-01 21:32:52', '2015-07-01 21:32:52', 17, '', '', 0, 1),
(6, '2015-07-01 21:32:57', '2015-07-01 21:32:57', 17, '', '', 0, 1),
(7, '2015-07-01 21:35:10', '2015-07-01 21:35:10', 31, '', '', 0, 0),
(8, '2015-07-01 21:35:13', '2015-07-01 21:35:13', 17, '', '', 0, 0),
(9, '2015-08-05 01:24:28', '2015-08-05 01:24:28', 2, '', '', 0, 0),
(10, '2015-12-08 04:30:10', '2015-12-08 04:30:10', 30, '', '', 0, 0),
(11, '2015-12-08 04:31:41', '2015-12-08 04:31:41', 30, '', '', 0, 1),
(12, '2015-12-08 04:31:58', '2015-12-08 04:31:58', 30, '', '', 0, 0),
(13, '2015-12-08 10:58:27', '2015-12-08 10:58:27', 31, '', '', 0, 0),
(14, '2015-12-08 11:00:43', '2015-12-08 11:00:43', 31, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_sections`
--

CREATE TABLE IF NOT EXISTS `page_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `page_sections`
--

INSERT INTO `page_sections` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'About us'),
(2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Help'),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Discover'),
(4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Hello'),
(5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_section_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_page_section_id_foreign` (`page_section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Technology', '', '', '', '', 3, 'static', '', '', 0),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Health', '', '', '', '', 3, 'static', '', '', 0),
(4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Music', '', '', '', '', 3, 'static', '', '', 0),
(5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Art', '', '', '', '', 3, 'static', '', '', 0),
(6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Science', '', '', '', '', 3, 'static', '', '', 0),
(7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Poetry', '', '', '', '', 3, 'static', '', '', 0),
(8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Theatre', '', '', '', '', 3, 'static', '', '', 0),
(9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Fashion', '', '', '', '', 3, 'static', '', '', 0),
(10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Games', '', '', '', '', 3, 'static', '', '', 0),
(11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Comics', '', '', '', '', 3, 'static', '', '', 0),
(12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Crafts', '', '', '', '', 3, 'static', '', '', 0),
(13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Dance', '', '', '', '', 3, 'static', '', '', 0),
(14, '2015-03-15 04:29:05', '2016-04-26 23:59:43', 'FAQ', 'Frequently asked questions', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">What is Crowdfunding<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">It''s\r\nthe practice of funding a project or venture by raising small\r\namounts of money from a large group of people, typically via the internet.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><br></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">What type of crowdfunding do you offer<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<ul style="margin-top:0in" type="disc">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Donations<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Partnership (Coming\r\n     Soon)<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lending (Coming\r\n     Soon)</span></li>\r\n</ul><div><font face="Roboto, serif"><span style="line-height: normal;"><br></span></font></div>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Can overseas project seek funding via ISupport<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Only\r\npermits locally based projects and projects that are clearly faced with a goal\r\nand an actual end point; we are open to all project creators globally that\r\nsupports the growth of our country. <o:p></o:p></span></p><div><span style="line-height: 1.42857143;"><br></span></div>', 'uploads/2016-04-26/62875f43d652c309e0ec2342ec9cadee.jpg', '', 2, 'page', 'questions', '', 0),
(16, '0000-00-00 00:00:00', '2015-08-29 08:57:21', 'home', '', '', 'uploads/2015-08-29/23930903398a520da680e762d4eca8b8.png', 'Flag', 5, 'static', 'home', '', 0),
(20, '2015-08-04 01:28:17', '2015-08-04 01:28:17', 'Netballers', '', '<h1 class="yt watch-title-container" style="text-align: left; margin-top: 0px; margin-bottom: 13px; padding: 0px; border: 0px; font-size: 24px; display: table-cell; vertical-align: top; width: 610px; color: rgb(34, 34, 34); line-height: normal; word-wrap: break-word; font-family: Roboto, arial, sans-serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="background-color: transparent;">Goal Rush Down Under - One Goal Campaign</span><br></h1>', '', '', 4, 'blog', 'netballers', 'https://www.youtube.com/watch?v=O3FsKjtvLUU', 2),
(21, '2015-08-05 00:11:45', '2015-08-05 00:11:45', 'Facebook', '', '', '', '', 4, 'link', 'https://www.facebook.com/ISupportJamaica', '', 0),
(22, '2015-08-05 00:12:40', '2015-08-05 00:12:40', 'Instagram', '', '', '', '', 4, 'link', 'https://instagram.com/isupportjamaica/', '', 0),
(23, '2015-08-05 00:15:48', '2015-08-05 00:15:48', 'YouTube', '', '', '', '', 4, 'link', 'https://www.youtube.com/user/isupportjamaica', '', 0),
(24, '2015-08-05 00:16:50', '2015-08-05 00:16:50', 'Twitter', '', '', '', '', 4, 'link', 'https://twitter.com/ISupportJamaica', '', 0),
(25, '2015-08-05 00:18:17', '2015-08-05 00:18:17', 'Google+ ', '', '', '', '', 4, 'link', 'https://plus.google.com/+iSupportJamaica/posts', '', 0),
(30, '2015-08-25 14:04:03', '2016-04-20 00:22:06', 'What is ISupportJamaica?', 'About Us', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ISUPPORT\r\nJAMAICA is a project which seeks to provide an easy and creative way for\r\nJamaicans in the Diaspora and elsewhere and friends of Jamaica to provide\r\nfinancial support to projects, both microenterprise and nonprofit, that the JN\r\nSmall Business Loans Ltd. consider worthy and reliable. Jamaicans are known for\r\ntheir indomitable spirit in business, sports, music and any other area they\r\nchoose to become involved in. As Benjamin Disraeli said, "The secret of\r\nsuccess in life is for a man to be ready for his opportunity when it\r\ncomes." Unfortunately, this opportunity tends to hinge on the catalyst of\r\nmoney, and thousands of small entrepreneurs are unable to realize their true\r\nbusiness potential because of their inability to access financing from traditional\r\nsources. The inherent perceived risk and the lack of adequate collateral are\r\ncited as the primary causes for this problem.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">In\r\nan attempt to provide a tourniquet for this inherent business practice problem,\r\nI SUPPORT''s mission is to deliver innovative and accessible credit in a timely\r\nand profitable manner to micro and small entrepreneurs in Jamaica who have\r\nlimited access to loans from traditional banking sources. The objectives of\r\nthis project are to assist micro entrepreneurs to increase their income and\r\nimprove the quality of their lives, create new jobs and to support the\r\ndevelopment of the Jamaican micro enterprise sector.<br>\r\n<br>\r\nI SUPPORT also provides a platform for anyone who wishes to donate money to\r\nnon-profits in Jamaica.</span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 24pt; font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lending to an entrepreneur<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nloan process will be simple and rewarding. Persons who give and those who\r\nreceive will be able to express their appreciation by posting testimonials on\r\nthe website. This essentially will reflect a good rapport between the demand\r\nand supply connection and consequently can lead to additional interest from\r\nother persons. Additionally, the entrepreneur will receive a rebate on the last\r\nloan payment as an incentive to list on the site.</span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; line-height: normal; font-stretch: normal;">\r\n\r\n<p class="MsoNormal" style="color: rgb(62, 77, 92); font-family: ''Open Sans'', sans-serif; margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Giving to local charities and community groups</span></p></h1><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Social\r\nentrepreneurship facilitates the framework for effecting social change by using\r\ntraditional business practices to gain something more than profit: namely, a\r\nbetter world. I SUPPORT makes it easy for individual philanthropists who want\r\nto contribute to worthwhile causes in communities across Jamaica. People around\r\nthe world will be able to donate to worthy and responsible initiatives located\r\nin what could be their home parish, or home town, and will feel a sense of pride\r\nin knowing that they have helped Jamaica take a step in the right direction. In\r\ncreating this causal link between idea and execution and by extension dreams\r\nand funding, I SUPPORT JAMAICA fosters a viable and thriving entrepreneurial\r\nspirit that is at the core of the Jamaican social landscape.<br>\r\n<br>\r\nI SUPPORT JAMAICA is an initiative of JN Small Business Loans Ltd.<o:p></o:p></span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; font-family: ''Open Sans'', sans-serif; line-height: normal; color: rgb(62, 77, 92); font-stretch: normal;"><p class="MsoNormal" style="margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Jamaica National Small Business Loans (JNSBL)<o:p></o:p></span></p></h1><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;"><img src="https://www.isupportjamaica.com/images/jnsbl_logo.png" alt="JN Small Business Loans" style="border-radius: 0px !important;" align="left" width="100"></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">JNSBL provides innovative and accessible credit to small business people. Through\r\nour involvement with them we have increased their productivity, improved their\r\neconomic and social status and contributed to their sustained growth and\r\ndevelopment. We are the micro finance institution of choice in Jamaica. Our products\r\nand services are delivered island wide and are accessible in most remote areas.\r\nWe use research to assist in determining the changing needs of our clients and\r\nto inform us in the development and launching of new products. We are\r\nrecognized as the organization that provides excellent business management\r\nsupport in the communities in which we operate. We are well positioned to\r\napproach the future with confidence and expect to be vigorous participants in\r\nthe development of Jamaica.<o:p></o:p></span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; font-family: ''Open Sans'', sans-serif; line-height: normal; color: rgb(62, 77, 92); font-stretch: normal;"><p class="MsoNormal" style="margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">JN FOUNDATION<o:p></o:p></span></p></h1><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;"><img src="https://www.isupportjamaica.com/images/jnf.png" alt="JN Foundation" style="border-radius: 0px !important;" align="left" width="100"></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nJamaica National Building Society (JN) Foundation works with internal and\r\nexternal partners to identify develop and provide technical and financial\r\nsupport to projects and programmes that focus on issues relating to rural development,\r\nhealth, housing, education, youth, community, crime and safety. Established in\r\n1990 as the charitable arm of the Jamaica National Building Society (JNBS), JN\r\nFoundation''s mandate is to manage and execute the philanthropic efforts of the\r\nSociety, and contribute to the developmental needs of Jamaica. Built on the\r\nconcept of mutuality, JNBS gives back to the Jamaican people and communities\r\nacross the island, by providing financial and technical support to projects and\r\nprogrammes both at the community and at the national level.<o:p></o:p></span></p>', 'uploads/2015-08-30/89fc0dca27527e75b532348553b186d1.png', '', 1, 'page', 'what-is-isupportjamaica', '', 0),
(31, '2015-08-25 14:09:28', '2016-04-22 22:13:40', 'Contact Us', 'Contact Us', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nget in touch with the <b>ISUPPORTJAMAICA</b> team, give us a call,\r\ndrop us a line via email or send us a letter through the regular mail.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Tel:</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> 1(876) 926-1344 or 733-7100 <b>Ext:</b> 2737\r\n& 2703 - 4<br>\r\n<b>Customer Service Agents: </b>Jamaica 1-888-991- 4065 , USA & Canada\r\n1-800-462-9003 & UK 0-800-328-0387    <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b>Email: <a href="https://www.isupportjamaica.com/ISupportJamaica@jnbs.com">ISupportJamaica@jnbs.com</a></b><br>\r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n            <b>   </b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Mailing Address</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ISUPPORTJAMAICA</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><br>\r\n2 - 4 Constant Spring Road<br>\r\nKingston 10<br>\r\nJamaica<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">If\r\nyou have any questions about using PayPal and the payment process, you can call\r\n888-445-5032 (Toll Free US) or 402-952-8811 (International), Monday through\r\nFriday, 8 a.m. to 5 p.m. CST for help in completing your transaction.<o:p></o:p></span></p><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Don''t\r\nforget, you can keep up-to-date with what we''re up to, by following us on\r\nFacebook and Twitter.<o:p></o:p></span></p>', 'uploads/2016-04-20/2c7fda42bf46a2a896521cfdcc181310.png', '', 2, 'page', 'contact-us', '', 0),
(32, '2015-08-25 14:17:46', '2015-08-25 14:17:46', 'How It Works', '', '', '', '', 1, 'link', 'http://isupport.spidercentro.com/learn', '', 0),
(33, '2015-08-25 14:23:27', '2015-08-25 14:23:27', 'Start A Campaign', '', '', '', '', 1, 'link', 'http://isupport.spidercentro.com/create/general', '', 0),
(34, '2015-08-25 14:44:18', '2016-04-14 00:11:18', 'Background', 'Background', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif;">The website, <a href="http://isupport.spidercentro.com/page/edit/www.isupportjamaica.com"><b>www.isupportjamaica.com</b></a>,\r\nwas introduced in June 2013 at the Diaspora Conference held in St. James.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif;">ISupportJamaica.com</span></b><span style="font-family: Roboto, serif;"> is the Caribbean’s\r\nfirst English Speaking crowdfunding website and it''s the leading crowdfunding platform\r\nin Jamaica.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif;">Social\r\nMission</span></b><span style="font-family: Roboto, serif;"> Community\r\nEmpowerment<o:p></o:p></span></p><p class="MsoNormal">\r\n\r\n\r\n\r\n\r\n\r\n</p><ul type="disc">\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Embrace\r\n     Technology & Innovation<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Encourage Entrepreneurial Habits<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Build Patriotism<o:p></o:p></span></li>\r\n</ul><p></p>', 'uploads/2015-08-30/d8c17f80d49f1ed5e90d581fb5b967fb.jpg', '', 1, 'page', 'background', '', 0);
INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(35, '2015-08-25 14:53:30', '2016-04-22 22:15:45', 'Terms  and Conditions', 'Terms and Conditions', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Grant Programme</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nIsupportjamaica.com website also promotes community-based projects and seeks\r\ndonations from the Jamaican Diaspora and globally to support and develop such\r\nphilanthropic projects. If you continue to browse and/or use this Website you\r\nare agreeing to comply with and be bound by the these terms and conditions and\r\nany other terms and conditions linked to this Agreement (including the Privacy\r\nPolicy), which govern isupportjamaica.com''s relationship with you.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Isupportjamaica.com\r\nis a registered Website (“the Website) owned and operated by JN Small Business\r\nLoans Limited (“JNSBL”) a micro finance Company registered and operating in\r\nJamaica with registered office located 32 ½ Duke Street, Kingston. This link\r\n(“the Grant Link”) which deals with grant funding is administered by JNBS\r\nFoundation. JNBS Foundation is a charitable organisation established in 1990 to\r\nmanage and execute the philanthropic programmes of the Jamaica National\r\nBuilding Society (“JNBS”) and its group of companies. Since its establishment\r\nthe Foundation has managed in excess of J$350,000,000.00 of grant funds\r\nprovided by the JNBS Group, international aid agencies, NGOs, local private\r\nsector organizations and other grantors.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nterm "Foundation” or "us" or "we" or “our” refers to\r\nJNBS Foundation. The term "you” or “your” refers to the user or viewer of\r\nthe Website. Words denoting one gender include all other genders.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">These\r\nterms and conditions constitute an Agreement between you and the Foundation\r\nrelating to, and govern, all businesses transacted by you using the Grant Link\r\nof this Website. By using the Grant Link of this Website, you agree to be bound\r\nby this Agreement, whether or not you participate in isupprtjamaica.com\r\nprogramme (“the Programme") by providing a grant or otherwise use the\r\nWebsite (in each such capacity, a "User"). If you wish to become a\r\nGrantor or otherwise become a User of the Website, you must comply with all the\r\nterms and conditions herein and all terms and conditions linked to this\r\nAgreement.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">This\r\nAgreement may be modified by the Foundation in its sole discretion from time to\r\ntime and such modifications will be effective once posted on isupportjamaica.com\r\nWebsite. You should review the Website and this Agreement regularly and your\r\ncontinued participation in the Grant Programme and use of the Website after any\r\nsuch modifications become effective will indicate your acceptance of, agreement\r\nwith and willingness to be bound by such modifications.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">How the Grant Programme Works</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will identify community based projects deserving of funding. The\r\nFoundation will assist the Project Sponsors to post a full description and\r\nother pertinent details of the Project on the Isupportjamaica.com Web Site.\r\nThrough the Web site Project Sponsors will solicit grants to fund their\r\nProjects. The Foundation will manage the Web Site Grant Link which matches\r\ndonors with Project Sponsors.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">A\r\npotential donor will have the opportunity to review the various community\r\nprojects posted on the Web Site. For each project the minimum target funding\r\n(“Target Amount”) will be specified. The Target Amount is the minimum amount\r\nsought to be raised to successfully undertake the particular Project.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">A\r\ndonor who identifies a Project to support will make a donation to the\r\nFoundation. The Foundation will hold the funds donated and if the Target Amount\r\nis achieved within the time stated the Foundation will then disburse the funds\r\nto the Project Sponsors. If the Target Amount is not reached within the\r\ndonation period then donors to that Project will be informed and will have the\r\noption to assign their donation to another Project.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Where\r\nthe Target Amount is not reached within the donation period and the donors have\r\nbeen notified and have not taken the option to assign their donation to another\r\nProject within six months of being notified the Foundation reserves the right\r\nto assign the donation to another Project or to allocate the donation to assist\r\nin offsetting the administrative costs associated with the Isupportjamaica.com\r\ninitiative.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Project Updates</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Donors\r\nwill have the opportunity to track the progress of their sponsored project via\r\nregular e-dates. Subject to the Privacy Policy donors shall be at liberty to\r\ngrant a testimonial which may be posted on the Web Site or in the Foundation\r\nFacebook page or other e-media outlet.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acknowledgement and Agreement of Project Sponsors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">By\r\nsubmitting a Project for sponsorship on the Isupportjamaica.com Web site each\r\nProject Sponsor will be deemed to undertake to the Foundation and to any Donor\r\nthat:<o:p></o:p></span></p><ol style="margin-top:0in" start="1" type="a">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">(a) the information\r\n     provided to the Foundation about the Project is accurate and complete in\r\n     all material respects;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">donated funds\r\n     disbursed to the Project Sponsor will be applied solely towards the\r\n     Project and for no other purpose whatsoever;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will keep and\r\n     maintain accurate and complete accounting records of all project\r\n     expenditures;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will submit such\r\n     accounting and other records to spot audits, without prior notice, by the\r\n     Foundation or by any accountant or other agent appointed by the\r\n     Foundation;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will provide\r\n     regular update of the progress of the Project and after completion regular\r\n     reports of the utilization and contribution being made by the Project in\r\n     the community;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will provide\r\n     prompt notice to the Foundation of the occurrence of any event which is\r\n     likely to adversely affect the smooth implementation and execution of the\r\n     Project or utilization in the community;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">disbursed funds will\r\n     not be handled in any corrupt manner and that where third party goods or\r\n     services have to be acquired such acquisition will be made at the best\r\n     price reasonably obtainable without favour and free of partisan influences;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">any breach of these\r\n     terms may result in the Project Sponsor being black-listed and each\r\n     Project Sponsor waives any claim against the Foundation for defamation or\r\n     otherwise arising form being black-listed and barred from promoting any\r\n     further Project on the web site unless the Project Sponsor can positively\r\n     prove that the statement made by the Foundation was known to be untrue but\r\n     was nevertheless willfully made to deliberately harm the Project Sponsor.<o:p></o:p></span></li>\r\n</ol><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nfact that a Project Sponsor has been black-listed does not necessarily mean\r\nthat he is guilty of some act of dishonesty. It only signifies that the Project\r\nSponsor has failed to comply with the terms of the sponsorship on the\r\nIsupportjamaica.com Web Site. The Foundation reserves the right to continue the\r\npromotion of a worthwhile Project under different leadership even though the\r\noriginal Project Sponsor has been black-listed for breach of the terms.\r\n      <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acknowledgement and Agreement of Donors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will endeavour to vet Projects and use reasonable efforts, within\r\nthe limits of its resources, to post Projects for grant funds which have the\r\nrequisite management and institutional support to ensure that the grant funds\r\nare used as intended and that the Project will ensure for the benefit of the\r\nintended beneficiaries. Notwithstanding the Foundation best efforts it must be recognized\r\nthat not all projects will succeed. Each Donor acknowledges and agrees as\r\nfollows:<o:p></o:p></span></p><ol style="margin-top:0in" start="1" type="a">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that the funds\r\n     donated by him is a grant to the Foundation and not a loan will not earn\r\n     interest nor will it be repaid;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that the Foundation\r\n     gives no guarantee and makes no representation or warranty as to the\r\n     implementation or execution of any Project;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that whilst the\r\n     Foundation will endeavour to guard against misuse of donated funds it\r\n     shall not be liable to any donor if a Project Sponsor actually\r\n     mis-allocates, misuses or appropriates donated funds to non-approved\r\n     purposes; and<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that whilst the\r\n     Foundation will undertake spot checks on a Project it does not have the\r\n     resources to undertake detailed Project audits.<o:p></o:p></span></li>\r\n</ol><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Communication</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Subject\r\nto applicable laws, communications may be posted on this Website, or another\r\nwebsite disclosed to donors, or sent to the e-mail address provided by them.\r\nAll communications will be deemed to have been received by a donor immediately\r\nafter the Foundation sends it to the donor by e-mail or posts the communication\r\non the Website, whether or not the donor has received the e-mail or retrieved\r\nor read the communication from the Website. A communication by e-mail is\r\nconsidered to be sent at the time that it is directed by the sender''s e-mail\r\nserver to the e-mail address provided by the intended recipient. A\r\ncommunication posted to a website is considered to be sent at the time it is\r\npublicly available. Each Donor hereby agrees that these are reasonable\r\nprocedures for sending and receiving communications and accepts such procedures\r\nof communication.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Eligible Donors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nbe a donor you must be either an individual at least 18 years of age or a\r\nlegally constituted body such as a corporation and be able to form legally\r\nbinding contracts under applicable law. You must have a valid postal /mailing\r\naddress and an active e-mail address. You must have either (i) a valid credit\r\ncard or debit card or (ii) a deposit account with a reputable financial\r\ninstitution. Your mailing address must match the billing address for your\r\ncredit card, debit card and/or deposit account (as applicable). Other\r\nrestrictions may apply.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lenders Commitment</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nparticipate in the granting of funds to the Foundation on the\r\nisupportjamaica.com Web Site you must provide complete and correct information\r\nabout yourself sufficient to verify your identity. Such information must\r\ninclude, at a minimum, your name, address, date of birth, and social security\r\nnumber or taxpayer identification number. The Foundation reserves the right to\r\naccept or reject your request to participate in the Grant Programme. You\r\nrepresent and warrant that all information you provide to isupportjamaica.com\r\nor the Foundation including but not limited to information about yourself, your\r\nsource of funds, will be complete and accurate in all respects. The Foundation\r\nwill be entitled to rely on any such information, and you agree to update your\r\npersonal information if it changes. The Foundation also reserves the right to\r\ntake steps to verify the information you provide. Your ability to use or\r\notherwise participate in the Grant Programme may be terminated and any pending\r\ntransactions may be cancelled if you provide false information. In addition,\r\nyou may be subject to civil and criminal penalties.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">In\r\nthe event that you provide incorrect banking information you may be required to\r\npay a fee to cover bank charges incurred as a result of the error and you may\r\nalso be required to submit to the Foundation, documentation to verify your bank\r\naccount number.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">If\r\nany false, inaccurate or incomplete information is provided by you to the\r\nFoundation or if the Foundation has reasonable grounds to suspect that the\r\ninformation is untrue or incomplete the Foundation has the right to suspend or\r\ncancel your access to the Programme.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Neither\r\nthe Foundation nor the owners or operators of the Isupportjamaica.com website\r\nwill be liable for any loss, damage or injury arising from your failure to\r\nprovide accurate information.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Accessing Your Dashboard</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nwill create a unique user ID and password to access your Dashboard. You should\r\ncontact ISupportJamaica immediately if you suspect your password or user ID has\r\nbeen compromised.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Security Procedures</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Access\r\nto your Dashboard on isupportjamica.com and your ability to initiate grants\r\nwill be verified based on security procedures that are offered as part of the\r\nservice. Each time you attempt to access your Dashboard, you will be asked to\r\nprovide your unique user ID and password. Providing this information will give\r\nyou full Dashboard access with the ability to initiate a grant and review the\r\nProjects you have supported. You agree to follow any additional on-screen\r\ninstructions with regard to Dashboard security. You also agree to update and\r\nupgrade your Internet browser software on a regular basis as such updates or\r\nupgrades may become available from time to time. You acknowledge and agree that\r\nthis security procedure is commercially reasonable and that in selecting a user\r\nID and password as your security procedure you have elected not to use any\r\nother commercially reasonable procedures that may be available.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Confidentiality of Security Procedures</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree not to divulge your password or user ID to any other person, and you\r\nagree not to use another person''s password or user ID. If, notwithstanding the\r\nforegoing prohibition, you give your password to another person or persons, you\r\nwill be deemed to have authorized them to use your password and Account for any\r\nand all purposes, without limitation. You agree that the Foundation will not\r\nhave any responsibility or liability to you or any other person for any loss or\r\ndamage which you or anyone else may suffer if you disclose your password to any\r\nother person, including any loss or damage arising out of the disclosure of\r\nthis information by the recipient to another person.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Forbidden Transactions</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree that you will not use isupportjamaica.com for the improper, unlawful, or\r\nfraudulent transfers of funds ("Forbidden Transactions"), and any\r\nfunds donated by you to the Foundation will not be from any Forbidden\r\nTransactions. Forbidden Transactions include but are not limited to attempts to\r\n(a) to use  funds gained from using stolen, unauthorized, or otherwise\r\ncompromised credit card, check card, debit card, bank account, or Account\r\ninformation, (b) using funds from an account with insufficient funds (c) to\r\nseek reimbursement of funds that have already been disbursed, (d) to launder\r\nfunds, (e) to use the service in connection with gambling or pornography, (f)\r\nto fund terrorist organizations or individuals identified from time to time by\r\nappropriate governing authority, (g) to make corrupt payments or gifts or (g)\r\nto violate applicable local laws, laws of Jamaica, credit card association\r\nrules and regulations, or the laws of any other relevant jurisdiction.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Ownership and Use of Data</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree that the Foundation and the owners of the Isupportjamaica.com website\r\nwill own and possess the data that you provide in connection with your grant.\r\nThe Foundation and the owner of the Web site agree that they will not disclose\r\nor sell your personal information to third parties.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nacknowledge, consent and agree that the Foundation and the Owner of the Web\r\nSite may, at its sole discretion and to the extent permitted by law, access,\r\nread, preserve and disclose your account information, usage history and\r\nsubmitted content in order to: (a) comply with any applicable law, regulation,\r\nlegal process, or governmental request; (b) respond to claims that any content\r\nviolates the rights of third parties, including intellectual property rights;\r\n(c) enforce this Agreement and investigate potential violations thereof; (d)\r\ndetect, prevent, or otherwise counter fraud, security, or technical issues; (e)\r\nrespond to your requests for customer service; or (f) protect the rights,\r\nproperty or safety of the Web Site, the Foundation, its users and the general\r\npublic.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">By\r\ndefault, you may receive periodic newsletter emails from the Foundation or the\r\nowners of the Web site. The frequency of these newsletters may vary. You can\r\nchoose not to receive these newsletters through a preference on the Website.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will not disclose your personally identifiable activity to any third\r\nparty without your consent. The Foundation reserves the right to record and\r\ndisplay projects details, testimonials and the like on the Website and display\r\nthe general regions where project beneficiaries are located.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Indemnity</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree to indemnify and hold the Foundation and its parent company, Jamaica\r\nNational Building Society, respective affiliates, subsidiaries, officers,\r\nagents, co-branders and other partners, directors, and employees, harmless from\r\nany claim or demand, including reasonable attorneys'' fees, made by any third\r\nparty due to or arising out of your use of isupportjamaica.com Website, your\r\nviolation of this Agreement, or your violation of any rights of another.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Resale of service</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree not to reproduce, duplicate, copy, sell, resell or exploit for any\r\ncommercial purposes, any portion of the Website, use of the Website, or access\r\nto the Website provided hereunder.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The Foundations’ intellectual property</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nacknowledge that the isupportjamica.com service provided hereunder, including\r\nbut not limited to the content of this Website, text, graphics, links, buttons,\r\nlogos, and images, as well as all other copyrights, trademarks, service marks,\r\nlogos, and product and service names are owned exclusively by JN Small Business\r\nLoans Limited. ("Isupportjamaica.com Intellectual Property"). You\r\nagree not to display, use, copy, or modify Isupportjamaica.com Intellectual\r\nProperty in any manner. You are authorized solely to view and retain a copy of\r\nthe pages of this Website for your own personal, non-commercial use. You\r\nfurther agree not to: (a) use any robot, spider, scraper or other automated\r\ndevice to access the Website or (b) remove or alter any author, trademark or\r\nother proprietary notice or legend displayed on this Website (or printed pages\r\nthereof).<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Any use, copying or reproduction of the Foundation’s trademarks or logos\r\ncontained in this site, without prior written permission of the Foundation is\r\nstrictly prohibited.</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation is not responsible, and shall have no liability, for any incorrect\r\nor inaccurate Content posted on the Website or any liability, cost or expense\r\nyou may incur in connection with the Programme, whether caused by any User, the\r\nFoundation’s Officers, Project Sponsors or other person or by any of the\r\nequipment or programming associated with or utilized in the Programme. The\r\nFoundation is not responsible for the conduct, whether online or offline, of\r\nany User of the Website or any other person.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation cannot always foresee or anticipate technical or other difficulties.\r\nThese difficulties may result as a loss of data or personalization settings, or\r\nother service interruptions. For this reason, you agree that access to the\r\nWebsite is provided as is without any warranties of any kind. The Foundation\r\ndoes not assume responsibility for timeliness, errors or inaccuracies, deletion\r\nor failure to store any User data, communications or personalized settings. You\r\nagree that you assume the risk for use of the on-line application service on\r\nthis Website. The Foundation shall not be liable for any special, direct,\r\nincidental or consequential damages.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The Website, including any Content or information contained within it or\r\nany service or advice provided in connection with the Programme, is provided\r\n"as is" with no representations or warranties of any kind, express or\r\nimplied, including, but not limited to, implied warranties of merchantability,\r\nfitness for a particular purpose and non-infringement. You assume total\r\nresponsibility and risk for your use of the Website and related services.</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Business Days</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nphrase "business days" as used herein means Monday through Friday,\r\nexcluding public holidays. However, access to the website will generally still\r\nbe available online on non-business days.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Assignments and Transfers</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nmay not assign or otherwise transfer your rights under your Account or the\r\nAgreement to any other person or entity.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Tax Deductibility</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nunderstand that you are solely responsible for determining the proper tax\r\ntreatment for any grant you make through the Website and the Programme. The\r\nFoundation will, on request, provide receipt of other proof of grant to a\r\nDonor. The Foundation has not and will not provide any tax or legal advice to\r\nyou in connection with any Grant or Loan you might make<b>.</b> This\r\nAgreement does not attempt to define the tax implications of participants in\r\nthe Programme. If you participate in the Programme, you should consult with\r\nyour own accountant, tax or legal advisor.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><a name="0.1_01000003"></a><a name="0.1_01000004"></a><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acceptable\r\nUse</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nWebsite is for use in connection with the isupportjamaica.com Grant Programme\r\nand may not be used by you in connection with any other commercial endeavors\r\nexcept as previously approved in writing by the Foundation. Use of the Website\r\nand participation in the Programme shall be in strict compliance with this\r\nAgreement, all other procedures and guidelines set forth on the Website and\r\napplicable law. You may not engage in advertising to, or solicitation of, any\r\nUser, donor or any other Person to buy or sell any products or services through\r\nthe Website. You may not transmit any chain letters or junk email to any User,\r\ndonor or any other Person. Illegal and/or unauthorized uses of the Website,\r\nincluding collecting the name, email address or any other personal or\r\nconfidential information of any User, donor or any other Person by electronic\r\nor other means for any reason, including, without limitation, the purpose of\r\nsending unsolicited email and unauthorized framing of or linking to the\r\nWebsite, will be investigated and appropriate legal action will be taken,\r\nincluding, without limitation, civil, criminal and injunctive redress.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Although\r\nthe Foundation assumes no obligation to monitor the conduct of any User off the\r\nWebsite, it is a violation of this Agreement to use any information obtained\r\nfrom the Website in order to harass, abuse, or harm another person, or in order\r\nto contact, advertise to, solicit, or sell to any User, donor or other person\r\nwithout their prior explicit consent. In order to protect such persons from\r\nsuch advertising or solicitation, the Foundation reserves the right to restrict\r\nthe number of emails that a User may send to others through the Website in any\r\n24-hour or other period to a number that', 'uploads/2016-04-20/a8fff358ef1109fba51a0cfbab28e9fe.png', '', 2, 'page', 'terms-and-conditions', '', 0);
INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(37, '2015-08-31 02:01:07', '2015-08-31 02:01:07', 'Successful Campaigns', 'Successful Campaigns', 'To be completed<br>', '', 'Success Stories', 1, 'page', 'successful-campaigns', '', 0),
(38, '2015-08-31 13:59:28', '2015-09-01 19:51:16', 'Our Partners', 'Partners / Angel Investors', '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYwAAABXCAYAAAAXt3ISAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAKGFJREFUeF7tXQl4VNXZDggCsopWRBQErWDRglRlERUEi4gKFkUUKU9RUPsjUKwVEUVxrSsPVbFuaCsVSEJYshEg7BB2AmELEMhCICQhIWSDEL7/e8/MhZvJzD3nzkwmEziH5z4hmbuc8947573nW94vhHTTCGgENAIaAY2AAgIhCvvoXTQCGgGNgEZAI0CaMPRDoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAKaMPQzoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAKaMPQzoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAJBTxinT5+mkpISKisr03dLI6AR0Ai4ReBM+Rk6x/90q1oEgpIwzp07R4sWLaLhw4dThw4dqHXr1tStWzd66623KDk5uWoR0WfXCGgEghIBEEJWaRYl5SfRiqwV9POhn+mNxDdo8NrBdGfcnZRenB6U/b6YOhV0hFFUVERjxoyhkJAQt1ubNm1owYIFF9M90GPRCGgELBBYemwpvbTtJRq0ZhB1WdyFfhPxGwoJ4/lhPm8RvC3kLTSEDhUd0jhWMQJBRRjl5eU0efJkj2RhkEiTJk1o48aNVQyNPr1GQCMQDAhM2D6BQlYzKSxyEgTIYg5vs50bk0XduXXpcNHhYOjuRd2HoCIMmJuaNm0qJQwQx7Bhwy7qG6MHpxHQCDgQmJg4kUKiTQRhEIUmjIA/IiFwKO/duzfgW0FBQaXBzp07V4ksQBjt27enkydPBhwwfUH7CJw4fYL2FvAzZrWd2ktHio/YP7k+4qJH4PUdr2vCCJK7HALTjid/QVX+ffbs2ZUg+PLLL5X7Al9GWlpakMCou2GFwNPrnnbYml3fDM2/h4dQu0XtKK1I31P9NFVEQBNG8DwRIRs2bFCepP1JIL/++mslFGbNmqXcl5tvvplOnDgRPEgGQU9KS0spJyeHDh48SHFxcQQCHjlyJH388cfV1rtNuZuoaQSbGUEYcyXbghCatm9atfVVXzg4EdCEETz3JahWGDt37qQGDRookcagQYOCB8Vq6gmCBCIjI2natGkisqx///4EIgWGtWvXPo/jI488Uk09JBq3dZzDWWm1ujA+mxdCt0ffTqfKTlVbf/WFgw8BTRjBc0+CijDOnDlDL774opQw6tatS0uXLg0eFKupJ1hRXHXVVVK8nnzyyWrp4aHCQxdCIFUIg1cgtcJq0X8O/ada+qsvGpwIaMIInvsSVIQBWI4fP05/+tOfPE6CiKKaMWNG8CBYjT1BwELLli2DljCmJk11mKLMIZAy4uCY+r7L+1JpeWk1IqsvHUwIaMIInrsRdIQBaAoLC+mbb76hvn37ijfoK664gtq2bUvPPfccrVmzJnjQq+aegDBatWoVlISRU5pDt0bdKnd2uwmRrDevHiFZSzeNABDQhBE8z0G1EYY7p7crLMXFxZSZmUnp6emUnZ0dPKgFSU+CmTB+OvSTIxsXjm7ZqsL1c3Z+j0gYESQo625UNwKaMKr7Dly4frURhruw2uCBpWb0JFgJo6S8hPot76fu7HYlDCaaZvOaibwN3TQCmjCC5xlQJow333yT9uzZ47dNJ935/hAEK2GsylpFl829zHp1YeXXwGfs+5i4faLvIOkz1HgENGEEzy1UJoxvv/02eHqteyIQUCWMp59+OqCIjUwY6RCGszBFXTaHCcWKNPj49lHthTqpbpc2Apowguf+KxMGksAC0ZB4piJVgjoZdhuUcA8fPkyrVq0STvWJEyfSn//8Z0Kewv333y8k1Lt27Uq9e/emIUOG0IQJE2j69OkihPfAgQN06pR6fgAk2lNSUigiIoKwOnvmmWeEE7979+7iOn369BHXxfXx+cyZM2nt2rUiex1EoNJUCeOhhx5SwhS4HznimzwHQmmvjbiWQjinwi1hsE+jaXhTemnTSw7C8EQa/Pda82vRzJSZKlDY2gd5HpDC3pq7leZnzKcvk78UekUjNoygx1c/Tv1X9Kdey3pR1yVdqceSHtRneR8asnYIvbLtFdGf7XnbA5Ircrb8LKUWpVLM0Rh6f9f79NzG5+jhFQ/TvUvvpa5xXen+ZfeL359c+ySN2jiK3kl6h35I+YHijsbR7vzdlFmcSSVn1Z4lOwAWlhXS9hPb6dsD34r7+PDKh6nH0h7UbUk36h3fW/Tn1W2v0v9S/yfMiqVnfYt4CxRhZJZk0pKjS+jD3R/SXzb8hR5a8ZAYF56DXvG9xDiHrh1Kr25/lWbsnyECM1JOpVDBmcoyR3bwrEn7Bh1hvPfee9KoH2Scwzym0g4dOkRz5syhSZMm0YMPPkhXX3210vlds9obNWpE9913HyUmJkovi2S6Z599lpo3b277Wtdeey0NGDCA3nnnHVq/fr3ltVQJw06G/lNPPSUdn9UOn+759EJWt7sVBpuaMBnvKdhDt0Td4plYcCw7v/vG96Wz58761Cd8oVccW0Ff7/+axmwdIya16xZc5yA0VjoVznmWJhEhwNiwOsLG1xebIaONfXhfKKP2WtGLpidPp6wS/6+A8k7niVyUoQlDHXksRh9Bwua+Gf9Hn/EZ+od9mZTRx5sib6IBKwfQxB0TRe2IxPxEn7A8WnxUkOsDKx6gunPqOnAz+uQBq4ZhDemxNY+J8YBovGlVTRjrc9bT2K1jqW1kWwd+wNHduMzPgRNnvPzct+w++sf2f1DkkUif8PUGm0AfE3SEMXUqx+57qIVh/vvu3bstscKkjTf4m266Sel8KtesU6eOWJ14aliFwPyDxEKV88n2eeGFFwJOGL4k+RWUFYg3clGfwJM5irO+3016V4zr+Q3PWzvGnbLVa7PX+vS9wJtw4+jGjsnfLJFtSJUYKx1ZvoixH45zTpC3x9xOc1Pn+tQ/4+Cy8jKxQkDNBzFpoa9GpJnVagxYm8eA/hkTHya5SAcRtlzYkg4UHrDd1/Jz5aJfHWM6XiBSXEOyQhSfox94Hng8eDZiM2NtX7+qCOPUmVOCTJsv5Bc7YATMZeMyY23gDMJeHEJ3x91NxWeLbY+vJh0QdIThrxUG3vBlE7Ldz++44w6h1eSuJSQk0K23ct6BAtmp7gOzmVULthUGlvO1w1iSxFMoLU8eDcIaiGppaGFpYVQntI5jUnFHMJhweMIbtWGUT98pEFTIEucEZzfE12p/9I+Jo25YXfrn7n/61MdDpw7Rk+uedGABMvImHNmqr86CQzvyd9jqJ1ZQwxOGXyhYJCNVT31wkuwV4VfQR7s+slVOtSoIA+a6P674owNrT+ZTO88Kn+eFjS8QTNEXc7toCQO+AdWJWXW/J554wu2zkJSURFDPVT2Pyn7Qgtq2bVuNIgzYfS2d3fylgp27qKxIjAtveHBsWyrZ8ttb64Wt6XCh98VxPtr9kbU8tp2Jwd2+/GZae15tmpHsnQLBltwthJWKeMv1N1EY/XWuppJOJinPZ8D8/vj7z1e0s51P4wErTNBv7XhLuR/+Joz80/n04PIHHXh7S4DmseEcvIr6/uD3ymOqqTtetIQxYsQIv07gmOThV3Bt0HOC6J8KCdjZB/U+IJNSU1YYaYVp1CKihWNZ72m1wJP/a9tfqzAkOBAt5UOcqwzYzr1tVU4YGC+P7eoFVxMmfzst+WQy3RZ3m/c5K6pkZ5MwUMPkgfgHHP3yx6Rq7idWO7ypEqy/CWNyIlf1hJnMX+TMY2kY3pBWHfdsrrbzTATzvsqE8cMPPwRkHP4ySfmbMOC/WLRoUSUM4FA3K8PaIQWrfYcOHUpnz1o7e4PJJPX5ns8v2IDdTWLwR7CjdF32ugoYrs9eT3VD2edj9eVlsxQc1UgI9KYpEwbIznB2Y0LBBpOFJxJ0HSfvj9ofsPmrNERr9Ynvo04WwAj2cvTJ3D/0GaasXz2QNfppgzDOscFo9MbRapMqrovrG/0x+iIjMh7HVRFXiYgzWfMnYSB67OoIDnyRmaFAkrjvrlgbJixzmVge820xtxGCAi72pkwYY8eOpfj4eJ+33NxcS0wDTRjNmjUT1fvuvvtuEVqLDaGvt912WwVhv/r16wuJEnPDhI6JXZUkbrzxRhFS26tXLxFxhet06tSJbrjhhkqO8g8++ED67FUFYWC1ZLfB0YfIJ0tnN39B71x8ZyVRQUTOSI/lyQ6JgGuOe6cjpkoYrRa0os6xnYXZDJEvCFv9w+I/OCYYTISyN1KeRBqENhAhpypN9EvlvE5fCVZuv438regfwjzRP/y/U2wnunLeldb9s0EY89LmOQjIk2/JcPzy5Flvbj1hTgNe9y67lzpGd6Tac9iPhXHJViZsEhq2bhiVnSuzhMufhPHZ3s+sn1ODXJnQWsxvIZ5Z4Ay84bTvEtuF2i5qSw1DG14IoogNocdWPaZyy2v8PsqEoTopyvZD9JJVCyRhIFR28eLFIvcA0urmBmJDCG14eDghcuuNN94g19wPEMgtt3BoqMTRDQHFzz//nPbv30+oYWE0OMiQ8Y6IL+CCQkePP/64EBRE/oasqRJG586dxfVVtqioKNllK30OJ3b9efWtJxg2bUzdOdXtuT/e87F8guEJ6MVNL9ruGw6QEoZzMo06EiXyK8wrBOQxbMjdIByatebUkmev80QzJWmKtJ/IV7lhwQ1ycUZnSO2jqx+lsPQwUZHQ3D/xDJ05KXIDhE3eyunM45T5MDD+O2LvsPZFAS9++x64ZiBFH4kmmK+Mll2aTRHpEWKClSoVw5QzryFtytkUEMJAePbA1QOtMeex1Zlbh8ZuGUtbTmyplEMC/9vBUwdp2bFl9NW+r2jkppHUJqoNvbVT3ScjfTiCeIeAE0ZMTEzQEAZyMmRmH3NnXSMgtmzZQvXq1ZMSBnJAVBtIKTU1VSj2ypoqYSBirCrbuC1cJMkqlBbRUeENyFN47I68HdQ8nEMbrUw/PBHfuPBGSi+quMpTGZcqYewr2Gd5ur9u+qvDlGH15szE2G9FP6lZSpCkFWbON93Lwi8TYciy5Dfx5hztO2HAcWspGomxMwZjN4+l0+Wek2eRS/L4mselGf8w+UzeMTkghIHVbIeoDtbPGd8/RE/ZaRmFGRVI086xNW3fS5ow8Oafn5/v9T1DZrZsdYHPv/76a6+vYXWgKmH4klsh6zjeKBHFZDnZ88TYc2lPy8StB+M5asVKTsRpU/52v32JGlXCkIWcphamCjOFjNgwKVkRW9HZImFKktY5ZzxQsVClqY7RaoVRXFYsEiUtKyQyccNEg31lLaMog9otbGc9Tqep0io5018mqfwz+XRNxDXW9y8qhJ5Z+4xsaJfs55owfCAM1OZQIQz4KpKTk/3+kAUDYcw8OPN8drFVdJTsLfKb/d84vshWb+9MPMhclr1tuwKtOpnKCAPnFSYNK4cpm1ngT0D2sKeWmJfoyJS28hHwxNwhugMdKVaTalEdoxVhoP5643mc4GiRF1MrvBaFpoYqP8u475arMr7nzSOa075Tnld3ASUM7s91C6+jpUcDWI/lzEGiMlZmPsOb+Mm/q7ayTKJy57E4Hht+J9/UETxdXhOGD4SxefNmQvSUCmkg4xx+kLCwMELeBsJxfW3VTRh4K4Szz9K0Akfw3AbCD2DVYBe+Mpwdt1aTKH+GxK+tJ7bagk51MlUhjHd2vmP9xsx9hNMXMhGemjD7YJwWOlow1UxKVDdlqo7RijCQES9bXfwu+neE4liqbe3xtdbkyDjUmVOHwtPDPZ7SX4SBvJ+bI2+WR71BXj+8Gf1181/pv4f+SyBS5G5UTeOJPa0d0dEQohTe8BO/q0z4pfw9yG5LlOE8Fsdjy8I2jInD/xpXmjB8IIyDBw9S69ZsjrGZ3X399dfTXXfdRUgEhJPfcLzbFVSsbsJASCRCIy1NNPw2ftfiu+hMecWgAtcvH8gHAn+WE5YzJ+PNnW/a+u6qTqYqhPFTyk/yvBFeHcxOne2xj69sf8X6rZsnrCvCrqDFRxcrj1N1jFaE8cKmF6zNgrzCg38G5raM4gylbUPOBkdEkacXATjQebyf7/28ygkDz+CDK9j0idBkWdgv+uuUZoGeV+fFnemRVY8IyX0EH+AFx1ttrIoDZcJIbcO1qXmST+VN/OTfZYRxjr9POWOIinn/w85jcTw2EEgab2f9X3Qu4IQhi8IJZJSUrz4MVAR8+OGHbROGK8EgjwN9gfbVv//9b+XqgtVNGJ/s+USeQ8B2eExmKg0CdbUjOCTTyizFX/aOUR1tfVlVJ1MVwliYsVBOGDzR/Jzys8chP7XuKeuoMCbZGxbeYMvBrzpGT4SByRS11C0nU74vWA0gdNjOVms2R5d5mqCdLwGvJ75e5YSBC4ikPRCBjDDMGfLm/Byn2bRRWCPquaynUBGWBUtYP/terjDO7GJSqOMgCGNlYfwEgWTwZ6d+Vfna2dpHmTBQV/vKK6/0eYNUuFWrSYSBccChbXeFIdsfOSEypVpcuzoJA+GmXRd3tbbn89tjvfB6FJMZI6JIZNvG3I10XQSryFpFS8GEwWGPkCVXbaqTqQphLDm2RB4uyo7TH1N+9Ng9yGZbmvGYFCGZYsdXozpGT4SBt2VRg12WpGgIChp5Gio/ZZMzT+Avb3k5IISBRNFGEY0ckWCyfnlSLDCLDnIo841RN9L/Dv9P9XF02c9LwihZTXTEubpwJQz8nsvbkTuIzvlu+jZ3WJkwkCOA3ARfN9d8B1eUaxphIMoKooQyErD7ebt27WjHDmuhuOokjDXZa0gUQbLyOfAXrvbs2sIJ3Hxec+kG8xZkuVUSvp7f+Lyy0JvqZKpCGMuzlvtMGKivYUkYvCqDr8BOUx2jJ8KA0vD1C66XE4Y3k6zsGJ50R28YHRDCwEX+ksCaZ/6UPGF/U/3w+l6qFntJGBk9iI6ZVheHXMgDv+djleF5pWvn+TL2VSYMmXKqNxd3d0xNIwyMIS4ujpo0aeJ30kDBJZi9PLXqJIyXN78snziNicKpHSTe6mSbLDsY52STTetF6oKEqpNpoAgDfgAhOeFpInWuMErK1KVQVMfoiTAQcipqb8hWGLLJ35vPefKGg9lT85fT2zg/kiZByNI8GDtjYZJHIiYKKtlrXhBG4Rz2dTRy+CnOry5q8f+ZIMyrDTi/M/7AvgyOpPJTUyaMQFXcq4mEgXvx888/U+PGHJJo0wEu23/2bM/O0+oiDETJwI8gK8Pq1ZJf5UvqtHv/J+U/Sl8D1ck0UIQxZB07960IAz4MnnzsJCmqjtETYSBbXFRKlBEGsDfqiPjjJ1aovMJANcNAEQauA9NUu0jOEUF2vFXEmsrzaJJKsRqH+/F5QRi5f3c4uw86CQNO7vRbiPJYTghOc7MvA1FXZalK3xOVnTRh+BAl5Qrw8uXLRVU/GQnY+Rwy7Z409quLMOalz6O6EZI8AtUvmrf78Vv4o6seVXnGlaVBAkUYSMazzE1wRkmhvohq85UwIAkCnSoZYVg6sL25lyAgvpfv7XovoISBi+0+uZuGrR9G9UNZ1ga5NUa1RaOQkt3x8EvA7dG3C6kW9WaTMM5yXs7xOy+E4YIc4MvI+D1RAQvEZruYqfDZiSnq3ZHsqQnDj4QBrCHpsXr1anr33XepX79+QmvKF3NVly5dPEZNgTAgXCgjIH9mep9XMrUTaWL3i6eyP3+prwy7UilCRXUyDRRhoLSrUUrVY7KjgmSG+butOkZPKwzIfNwXf591EAOH1aKOxJ6Te0SJXX9uuac9i5L62yRlxg0vY0ik/Nf+f9HgtYNFVUEhNulc+QgSkYlOGs8rnknOJUrISbAxQdshDC7OVPAd+yZMqwv4KmB6KvyF8y7y+P8sa5JjIg18duR6NkupJYDKOq4Jw8+E4Qo4KvStXLmSYNIbPXo0/fa3/BZnw2yFuuB79yJzs3JD8h+UdmXnGzhwoOw5UP78WMkxajW/lfRNtMrMUeZwR34rhCaTrKlOpoEiDKjuCnysJiIeG1RRVQtHfbqXa6n7oCWFiVNaAIvt9FDv9VZmXnafPH2uShjwTfjaIC6IRD1EuY3fMp7uirvLEYShQhzO6KnQdPVMeJFvYSdxL5Un/0yXVQQIomS5Y+jHBjsIxezLKODf8yrX8vEGK00YVUwYrjcFCrfwd3TsyD4ABeJAJvn27dvd3ltEnKlEaEFKHasRf7RfD/9q/XZsTOhGvWdffspWGrzKgfYRQnytWrARRm5prlrGMU/Qg9YMopwSeWb1lJ1TKITDeX1Rq522b5q1IxhKrhH2Qpr98cxNTJxoTYbOUOukfPVqgqr9glYa6pD3W8mBCjLpGqcMvVVIdeXr2iCMsxlsemrpMEEZhIDw2ZwhHD7rNIOVbmMCasI+DdM+J2CW+j/ex/c5QBNGgAnDeGDS0tKoa1fOY1AgjU2b3Ms/Q2n3gQe4KprkHDBb+UvL6om1T8ilyI03MogJ+rJJQnZhNmgc0Zg25mysUYSBzkLt1VKO3HCk8kqjZ3xPkTkO8UNzw2oP2eCQEGkdyYoDVngp1MNAxTjkzViufPhNu1tcN5t2etXp2f1+yOyXSbdDen5RRuUCZ75d+cLREFscsZ6reMoKL7ET/etkO2KjNggjmyPJXFcPp0AYnPFtNORdpHAElVkuRCTy8Xba/YunHYw0YfhIGAh7LSuzLgDj6YZMm8ZvdJLJHlngW7d61k4aNWqU9By1atWiH3/0nEim+sAgZFCYo6ykFdhhiyzYr/d/TUuPLfV6Q7KfqHMti9phuzqybWvSCgN9hVmqYQRLZqgkkDmLEcEpfc/Se0TpVKj//i7mdyLj2pCw8LXiHiTJu8bxS4xVBBeIjPvzbMKzVaivVPFufrj7Q0efrLS3eCIftXGU9FFGRruK0q67EyVkJ1ivMpwrDHu1vRUJo4TFLLN5dWEmAqw0jrauLAFy8iuH8xv+DaxEQBgwYxXHSvGR7aAJw0fCeP3112nw4ME0a9Ys2rWL0/VttJdeekk62V9zzTWi8JKn9sknLM+hsEr5/e9/T4cPH5b2Li8vz+M+QpxOVg+CyeTOOI7i8EN7L+k9eaw89wd2daus6GAzSRnQDF8/XD2BzCjPapRCxU8Qt2wVZvb5KBRQQrSStFoe+sKk0XdFX0LxLLttb8Fe+u7Ad7TtxDalQ2ccmGFdowNjZByazG9imWGPi/1y+Bfqs7qPWAVAWcAOefxw8Afr5x/Ck3NrE6IIbbW0m+Tig8WsKwZfhNk3AfJIvY6T8zj0/tRPzo0T9U6wzArCaQ3CMEQN0zvZ6pa7nTVh+EAYqJbXo0eP8xM2RAX79OlDL7/8spAMQUIfSAQFkeD8zs7OppSUFFq2bBmh5C0q/skm+549e1rW7IiOjpaew7gGysF+9dVXosIf+pKVlSUc6rGxsaLaH6K6nnvuObdFpfBm9sQaNkdZRUc5i+u8ucOeOKCnpxjRJjA5yRRsL597OcGc4qkFK2HsP7VfaEYFJJ9FwSQF/FDDos3CNnKBPmcuDEQSH1n5CH2y+xNBHhDlyyrJouMlx4XDfmf+TiENM2P/DEKhLSQttonk8/Nz9PbOt5UmsJijMVR/Loe+ylZjvBqtF1aPhq4bSrMO8QvcyV2VTGeo2hgSywTDK5ar5l1F9yy7h6Aa8Nmezwg6YSivi5U0+o/IrbTCNBH19OGuD6nlgpbWzyK/vKBeCsas3KAom37jBUe2O7Xac5y8e7RfRWc3SAB5GFg9CB+FaUNklJGjYRAMNKeyGzCpcOitD00Thg+EgckWVfs8TfqXX365IIWmTZue1+BCiK1KlT7jnK+++qrl7c3IyCBIp8uIx/w5+mTogiHZEP00Pm/btm2l2uXoQHJBspD2kNnJ64TWISiU+qPh7a9PvERGwzlx/W3r32ocYaDDkRmR1Hg+k6KKgqosCMDqc0XCQJ9gThQrSZXVCyZxmIt4X9T3bhLeREjBYGsa3lSYJ0XtD8OvZayMeLzPrFMrVISytNctYI0xmf/gV2efefUDn0ajyEb0wW5OZnM2hA53X9r9wioB5k703dl/CCs2DGtITcIcY8DzjjEIsx/GqWAe7R3fm6yKQVV6SHM5YTGv9oXVgFt5czZbHbyisoy5Ow0pT38DgRTB3+FZs0vlO6sJwwfCgCy5nYna7r4gGtTckLXx48f7rR8NGzYkFIZybaLAkYI5qvuS7qIutr/aR7s+kl+Xv/CoQ513Js/tZYN1hWF0NjQtlFos4kp+mExVY/7tkocNwigtL6Wha4c6HM0qUi1GiLCz/ngl+Rf8Hdc3n4sJo8viLsp6YA8tZ8FGq4qMrnhgcudJ/seDF3x3R4qOOOT4XVcqRva6rP8yQuZrQv7eVoPD2mxqckcYJ6dzfkV9x2rCWFlgxYBwWqsNWlPmlQZ8HsdZo+y0PdO5eTyaMHwgDJhx7JKAnf1fe+01pWcPK52WLXm5rODLUNkH/hjXJvSPZG/BbGaYsmOKUp9Vd0KSWJNQ1umyMkfwZFQ3vC7FHnXv1At2wgAWkKros4JXUyBlEIchV2Hl6DUmOkykuDd4w/Y0qdkgDPTneOlxR3/8KdJn7huP8/qF1yvLnyzIWEC1QlkmXWXVg+vw84LVgTmJbnHmYnmlQ7tEbOzPOKHMcPFZN9pvp+OJSt0l8/HKIeexiol2gjDYRGWuh3H0TxVJBWRxiCOhMntbb1CrdZU/BzmVVH4hVP0+asLwgTCeeorrGvhpknY9D5LtCgrUK2Z9//33hGgof/Rn8uTJFZ6fXfm7hL3XcknOX9CG4Q29coLKHtb+K/orkZUnHZ+aQBjAACY41AQZsHrABZu9WbICphMjTBl/5zdaVPcbsGoACQVcK5MNEwb2hRyGasssznRUVMQ1ZeYguxMtnhcurLTs2DKl7pSdK6PRG0c7CEyFNLi/0ONCzovRxHOAFw/VVZPKmEDE3KeOsR1pf4Gb4JRcdkAXYmXAZqecsSw7zqZTYzvG0jZYBZjNSK4rjNNb2PfAOlHm3AvoRaUrqBmXMjHAt+F6/uMjGRLvIjs1YXhJGEVFRQQntz8maPM5MOmPHDmS4FC327DiMfsjvO0bikKZZejhEBRvsFZfNJ7M7lx8J9lRWFUdn4hOcYaWenyD5skTNSSguuraagphGP1GIiKS0GamzKTxW8fToNWD6N5l99LdS+6m7nHdqd/yfiKEdPq+6bQ5d7NIXPxi3xfS5DbY5+0W+8G53056m65awC8Mzgp0PmfxGzU1eCVlVWnP9T6ixOqz6591PIuy55E/77GEJcBNTZBfjJ8Iw/DJ8HPfe3lv2pXnxsxTxol2RzgUFqSAN33kUJg3hL66OqfxeaGptkZRWEWhQSPi6djj8q/PaTZnZ7WoqGoriKeZ/FgPe4QkJHBsscJb8hdffOH1RewcOGUKZ6wq9EcWwjp0KNtgJedp0KABWYWRWvUbdTCGDBlCKCwlu47q50jk++UX1oTxoc2fP18p+9uqT926dSMQIhoK6/SK70Uhcc4vqfFlNf/Emy/bu1/brmZCszs8RBOJFQ4ymd1dH3+DGYdJxV1I47tJ71JIvKT/PAaUnJU15JYICQ7DDOSuP8tC6NuD38pO5dfPEZlmmenNb9Z444YD2ZuGQIYRCSOEU/v82LHqwBu7q3/CSDo0+wbgUzBjxauEzvGdbSfbIVoPRNk+liVxjHtg9MNsxuO/Pb/h+fNDhQba5B2T6ZrIaxyrJWPl5vR1nB+D60uRoVTgLCVrFilsF9WOPtj1ged8lNwJjtWFKyl4ckzn8b5Z3VgTKsvR73OFbBvkwABEPRnHIFQWk/4ZxZViLq9qkNxnHA9J9GOcCV7kuYa61fMRgiI90COSbb5OZKoP6YwZM6R9QV9RT9uqwf4vGxMmaDtmH9frIWlv586dBHMQCApCg4iaUnnLx0qiWbNmhEJJTz/9NIWFhYniVP5oCJdF/RJIgrRo0YIgL2JFECA91CZ/9NFH6bvvvhN5H4ZCLpbZqMmNLyje4N1uMe3p1uhbxdtuVbTyc+U0bvM4ar/cog/R7emWJbfQO7sqa+Ygf6T9Suv+t+cxqLx9b8jdQO2X8Ln4eh7xWNGewtO9+0J6gx/weWotm0clNTaQlIeqh760xBOJ9NGej+ieJfdQq0WtHKYzZ16GWIGA1LHh/079JajBQtCv3aJ29McVf6QpSVOEKQrht9629MJ0+j7le+q/sr+ojdI4vDEJJV0QAK69hHXGdlfUGSsrL6Pkk8mEIAPUL+8U04muXXCtIwrKnOcCZ7/rOJg4EPGFjHpUTPwu5btKmfeVxpLPdcohQ26sJDDZu26YyCHjUQqHNK+IzprmAKxQsOJAdJOxMsH5Utl/UXZADbp8Lo9cwsfAd4FzgJQIpPGQ2vEue+Fo3fyEAPSaEhMTCW/5IL7333+fkNj3yiuv0Lhx42jChAk0adIkQrIdCBhlWH0hLJVu79u3j0JDQ+mzzz4T10YfEFWFcN2pU6cKYkG+yPHjx92eDpORrTBBlU5V4T41qa/+ggGkfs18fnO2CgzgSXR4wnDC/fRXg1RJ1JEoQgTd1KSpYoU5ftt4QogzanTj7RtkDckOKMIWnFH3ydnpI3ImVmatFP6fT/d8SpN3TiZIyMvEJIEFCCQ6M1qQzz/3/JPe2PEG/X3734UpEOOYuH0iIdMcZlGvSC6fSSv7bseEjZWC64YJPJVNRHnjmASOVRw2cjRyOaw+nz/LdW4l/LNYze9z/mR5rIRQYDpHMf/fbPayAbYmDBtg6V01Av5GwB8ENzJhpHXIKcwqTBiYTHWrBgTKstnMtJQnejdbGf/NDxpPgRqVJoxAIa2voxFwg8C/kv9F43eMF9nQR4uP2sIImciIDDtfAc9TVA+baRqHNab1OaxHpJtGwAcENGH4AJ4+VCPgCwJw4IqiRRxQUHt2bbo99nYavHqwkMz4b8p/aU32Gtp7cq+wlUNmGzZ/mJ/ijsYR5MyR+CacsLJkP/Zt9F3el3A93TQCviCgCcMX9PSxGgEfEMgqzaLfRPzG4ahFhA+ifhDtxb/DgQtnMVYGSEAz5DYQpQTtLHGMLNTYKcoHQgmkE94HSPShQY6AJowgv0G6excvAnCiQr+oUiKailSFavIZ+y4gGonEN900Ar4ioAnDVwT18RoBLxFAednzeQwqWcV29+HVCmpmII9FN42APxDQhOEPFPU5NAJeIAC1VmGCsksEsv1h3uKVxa0xnBuTUzW5MV4MVx9yESCgCeMiuIl6CDUPAUQ4dYjq4EgQM7KJZUQg+9xZ8Q3+jYFrBtK+k/tqHjC6x0GNgCaMoL49unMXKwKoxz1s3TBqGtG0olSFWebCkNxwJ1eBz7CSMEtu8O89l/cUGlTIatZNI+BvBDRh+BtRfT6NgCICRWVFtDt/N806PIvGbB4jxPJQga9ZeDOqO5eLDiFz24icMktV4G9MGChYhAzvTrGdaMzWMRSbGUuoy62bRqCqENCEUVXI6vNqBLxAAMl7qI0RlhZG0/dPF5IbkxInCZkKbJDfQK1zlDxFSVHUC/FHtrgXXdWHXIIIaMK4BG+6HrJGQCOgEfAGAU0Y3qCmj9EIaAQ0ApcgApowLsGbroesEdAIaAS8QUAThjeo6WM0AhoBjcAliIAmjEvwpushawQ0AhoBbxD4fyWKHJTWr78fAAAAAElFTkSuQmCC" alt="" title="First Angel.png"><div><a href="http://firstangelsja.com/">http://firstangelsja.com/</a><br></div><div><br></div><div><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAADZCAMAAAAdUYxCAAAAz1BMVEX///9gXl+KiYnk5ORYVlYAAADsACHsAB3sACDsACrsACXsACzsACjn5+fsABjsABzsABXtGjf5+fnw8PD0j5h4dnftEDJycXH+8vOMi4v2pKvxY3G2tbVLSEnyb3v96Ora2tr4vMGioaH5yMz719r1maHzf4m6ubk4NjaAf3/uKkLxYW+rqqr3sLbycn7uNEr0iZKdnJzwVGT83uD3rbPNzc1DQEHvTF3yeoXwUGDGxsbrAADuL0YfGhv60NTvQFMzMDFoZ2cYExQ0MTIoJSVwd6MOAAASRElEQVR4nO1dCXuiPBfFqmEJhkVNXVqR4lK1at2t0+q07/z/3/QlYREUW/QbxqHDeZ5WhQTuSW5yb25C4LgUKVKkSJEiRazYzp/ru76m9VeN0rB1bWliQqX2ogoydCFKAprWjGtL9dthTVVCUuLV/mpQrw9e+kgBIpTVlXltyX4rrDEPRdR/Nn01aFjtFZIhwMPryfWbURkgLMmlWciZOSkB0N/+eZnigCVAGc5Pnd2ueKx2/qQ8caGkYrX0WQJLE+Vx5U+JExsaAGohShvAGmGY9P63LknTr1MNVSwnm+laAqMo6bYIYz1uYWJEjZci8SRMVfgSryxxoqWKg6hphwqoxSlLrMD4NXriBkRJ7XrXonJGD6MDMaHmtKUKZ3l3NVlNZpXuzu1eBDmRrdTk1TNNY0fexSNKvBjL5zY5CyRRd7fK+Z0oAlYcosSLgXR+H7qT2jFIEi8q6rktlGAN1jGIEi9qIIIvf4gSiOxI/TUYCxcEgy4qnevCQOiCXAkkOhcuUcJ28lR3wF8S23tOXmfEo0sCBgPwaXDpL8RM0S7JpoGkhbOHSsTAQgAVxH8VRvvb0FFOxnE/gSkJv12SmLFDl0yVraXEdbqCeklIT5KSNg1joP4FuRI4SrNQ/YJcUzF5fhG6wB4aauKMC/eMLmhsI/Ei23tV1NH5kQJDlS4xSdfF7gIHsA5hDJLEDAmdbV1aKkhehXKIPzvLDl5ika4MA43PzTIHKIELGVroXINY4WEjFlHihYXOHbtMoZTEaeAhej4zg4AS5ytQ1NB5cWgDiUlUXI4rofMsxRgmzydiWJ+niCNZTVpgwUHjLA9wrvCJnBYlmJ5jE7eqnMwGylFXN7oqGgAmcvaXoR/dp9c1iJMWVtjjjHU0Y8gneGlc9LnuFVQT6OK60CPPpE1FNZEekYMKihiHrstq0uKbARiKGCkd4ZnAsbYPMwVHSUZ4JtVRcNBSogQLBonnyW35CAGGl+Tz5Cx+9WWanZjsfojB/JKoPoaJtisOTP6LkFFFg2oC18IdYch/Ps1pQJjEkN8x5vyn466WgKWEDrQPUPuUqKVCnGA/3o+a8Em0c6jCfnLHZUG0hdPLomqqmNxx9iHawsmVuiUkJ/gxnkOUThJdC9Ilc/5/K56FE4H6BkjeWr/PcIroQOLPnKr4y9EJJzqVL1nC8TejI4QRIjyT92TA5wglOhWTPyw7RBjRgYy+HU9iRI6I1r8jT250tI56LSnfrX1SHBEtCacsa7JxSHSIoj7onTAcEN2qyVu1GQ2NAFGDh2evOkoIgkQ1DL/L+PMQAaIvMKkLFL6Gn2gJXLJ0NyHwdUYtVfyeHS6Dj2gfJ3QFUSTsiSZzzWZk7IniRK7ZjIy1S9QEFzznnSB4RBvydwqFHcMjKibvQZaz4MaMKkj5rj6RDZdoS7jkgfYE4Z8jOuOvvuVUpbTTdqW4un4vgK2CK7sLc1XEEIpqTHEcj+hKvq6jW0Ia0BovCCvxRHKe3UmmoXTRJgW/C6aKRWrfKnEtrdzPpuFrLjrWBewuBd7FI8eeqInk602flfbPJMxQLL5oaT/jPZKvF+gUxf2td7E8yuif2n8RwZX83ZZ/F5PTc9P/DwJrGKaS2L9K0GgIfMHHr5Y+XYZ2YFVKB32xj3BMaAu+sbDFx7F0IkiUM3ko4z8/jKkJvjYTD9HDdUaVKcKg/6epmsAXrjJjUd3jlWNWH2Cg1f7oI6KG6ttKp8bHYebClsiZr0SB1cZFazqNrWWapmW1zrOFO9+zmmshDnc3fNGjNVBlCOS1eU69btt1DfEASJIEgKAgrTGPPCAagv0cwe6SPe2+xKnVnZV2XxGhpK7a0TxPs8ELMsRQpiQFwlYkP6ToQxENewZGjWVHqOHp1Z2tkka4igDtAm9bCEHlGQD67gmw6rSH1rbV2lrDdkNDMo68uGWryo5vZAEQVfhz8AlRgll7p0qQcFDAtHSKrd5RIZb4xvDwvDHf4cizkB3ZWej9HM/KvKHyRdi6Yj6PESCKKJ5gOwQQo114s1rBrx9NcLGD9t7UWtiOUDOz1q4db6RljiIvK/mSKEXFatchAjJ98QvCowCphoKV6Ym9vF6gEn16roKxptPnU452hDIbEi/JoqzY/qnVLjkSDBQYee9GMwpRWxCrRpqdRPsYpePJsiM90Ak7ZJKU52ihPd0+koNrC4wOaf9YBggpMlZnlQ4PRJFnQ9a6jAUctSijE2UgitwnfQx0H/XfwVMvZTDHAlbDLb9xwupsVbgz+EAg3ZqS9i+rq1qrwlVqsqYJpDtACNJt8muCPNDHONIjZ2cTZXK2NUmzDUddPLHWda4BLGnhVW39OLUhvqli4BfcGvOkTHeeOW5gDaoDi/ZcL9xMFYm+WErEGQZTuaSPm/MaNfBzPjyi1pYlLIknhs8G0k7aj5KsyZ4qGi8KlpXO3qg2ZA2uWYfV4jE3trfg2UXs70zlIg/akOCIq/A47CY1ScYSPNkdvmLt9JOcrxp25amppKNv+1yzF1nEkl19M15zfSlLiBZ5sZTL1hXVpDHXEcFxazMxqU3s1ub0KDgzgFiTTl11KxDfiqmY/gIOhsYvImqPFJvoFmia62D05Ugj6CgP4YVhCHacKh1Vm7HiCU1P+9bgcFeojgSn+NSeWDqEg7VEu11Dg2AVqCm2FHPH281+LmteW55Lkboji/86uGi0rGGtXXomKLVrQ2tWoRvbl4bS0cZjRN2g770hLfXwjRNtHr5w4ilndke3EGgAcdoi1Rl0HkuAbukhCbbF7kBt7/kLQpRxliV88qCsYdXWK1HlBYnYa5G9KU4kXruAQB8DYwAOHdkptSj72qoQOxSsuzmiQdspH95RDkS2/GdNx8MHEySWSmOxhrvFADEq+/p5jrSLqAXCK75ilaZAAexleGxEwisKIkZb4QVRo8BjzAdbaEWDEPvkI67OwT4sc5WtwDvxrM1ABvZxUl/yQVFA5jSbwNahiqr5NlY1Im0+b4GQnmFbGqvEvYWygORd45loa2tmVBgMw6hhTRN5CWpioBcwJBzYFduAh8s/iGL3aQ1vefn4npWx6PpRHYhxUM86IuPScVKYEvbfqR5lVYIFDudFzYZA3+8nkNHZsHXcq7bqQIOoXTFfSO/pa98VKdisLB5rwSp/RrBva7J0bOSHALuRQBPBFemyfCcN1Y5va07v1wgu5ZupERYOEaJ+aawRT0aWQBmEDBWYPDuEoTqiWVqki5f2dfqKA89Ht1XtoMYHAI6dFvssHyzdMl54r5gMREi2eX877ojM8rbcV5jwMFiFgwibSVnA9+KeGhYgFGDnRC+2XQOJ0KzbGQyFtFTPx6lJAe96IJCxiH8Y0tKg5BmyihpQav1ZxbLmluyYvcJr7S8LYL9bpuG8eWUoHezYYzBv8CuigpupBmQso9EJLbA6pAiIf732yoXUqCa44mnQ5wzOCKtp3b83GaGCfF5xGxB/3JXymRfh3jcYOgvYBMXTKaJ1OufbAXYHD52ctgS+mjaygNuRE9sg4fAoZ6s2RWSsBJV+zVdJI0hq1Kl8A/k2xiGdDnr273puYhkHF45OZSh0zO3WLI0R9JSEYuVUzmr/6qS2PUKaOvtLzkJewLKC4IvBCel1bXkaMu6HDO0Ms/TCCzLGIo+fg+3WUCH84RD3rfUwdgDTfcxUdz3PkAaKdwcWoK6Q0SqNoWFRrfsV0V1isAOeNPZS2xpwjFVHPLaIeh+Kmk2k0h6F1RYhyi5YUfEucL4ys2qdFa8QUTANkLWPe6dZo+7q+UxxaFU6xBfvU1Y20VlJJO1aOXbxzVckEQONXtvBIlBt1TD4veoyz6TmbdMnhr2zTd8RFxsMOp0pkkN3U9i6RH9oWJFeX+qN0agxWPUB4pm3IEo8eilZX8Z3ic0kQ2NrhCBW7da6w/1OHfO0yjqhI23DnA+tozMaHBhcZQh9a2pHct2cCt5q/xPL/tuKxF7zS+4XdrvKD8dTnKsysdMYu28FJqWtoH69ZEYLsg5VqKi0IXvDbesHJD6HKMDSWet62jwWkCL7LeWQDhTk1y8Le9gYQ1lbnbjfyPWMKvPGTiZeHhK03ZS4Q+b2rFmF9g9Ki9f2w+1tvf86iBj/9gukyqKsBoYuU5WPaa3KJTBKjVFIQz4fs1p7fqBGrW/7TEOKFClSpEiRBCzuc98Q98Vrl2uKFClSpPh3kF0ws1MsFApZ8mlHLMghvUBBftr/OfalUDzKaCd0Ax30MgX3KvQy7EDWTuUlz9qXLbg5vKwBCbwjxcAR9+7sYr7EzrVPhFyaj9Wnco4k6pWr1Z7O6Xl2+J44EzfVj+pNliv8rFbfqzTpslq92XgZl9Wn2wz9dpchx5vO4V/k+4Tjuu/uZZ6q5ftqj3ySVEsq3OSdJCHS5snHI7lxuUc+qfSZmypNWPjFsj6Sv+otOVLksj3vSI5kWlIqvd7Tktxz87Q/Rf4eyLUz1TCehSUrHZKl51TPkgmfceUkSW7Iv1siSXNynJH+v9v4jt/bH5vlA+fcnMuyfE+UJE0+ccqEniuS8ijr9qdzT3LlW/dC1SznSWAfoZmahEmh7NznyTtFsz8sHFmPkPFU0SVa3tyEEH0vHhB1Mj50w4l2N7cLl2izGkr03mHLiN76iFafHlyihTCiC0r03SF6d0y02gwheu99q972yrQp3XK9bpDo4001Qzlmlze9p4OMtGS7mV554R5/vOnRwu7ecVQtfUTvMj1bdR/ee2UqyuOm271v0jbTYyrM3fZ65SKj9b6wbzC5ZUl9RDO9mx5T3acco7PI9ShyLtHJZjPJhfDk9gd7zSJrzqRs77MBoj2O1Q4RWC/qBxlp8rs73/F7nV2GENUfgzXa1X+yFJNNkSWnRGnScpY1AKo1LCulRYizGl2wpP4a1RdOxRer9OKbB3K/ou7V6KTb9co8gOWeaMGTnPvvUHUfqWQB1XUyLsixu67vuKu6RKWamQDRDZdlPwOqS4kWuS5L4aouuR8pJH8b7e2Jkvx3Tsbse3gbDcWTLXw2SLSQ8zpN+zaF+0OirB0RvsXTREmaTICofbcQorbK+IhyzdvcnijrVO2+lx7NueZreQZRrnxTJC37wVZdYoN0RnFjG5mlS5SVebNHUnid122V9Pv3tB8KEF0STSo4RLnyAVHuvWmrLjV2LtFbeknaSDMFltU2JndUAqq6NOmGdH2L/3S3dP4jR4hFnEw8okuH6OQ0UW7Ry9zQgtvc3NyQWtVtxbDzs1orMh6TIlekKe72GcuZKqO9aPouN6GJCLnm/jKFzT7VhNpnkqLpXJxSogQKJOUdPVF07seyMpmocNleZrKXqElSZMsZmqK58A7Tn4vsaaIpUqRIkSLF74ae/ZY4Jtrs3XxD9JL4ftQUKVKkSJFYFJr/xBTxJP+xzH/sIwd517HI5il+2WP4OxZieWKHnODxPfthxyJ09j3/6F7kw41p6Hm3EN+asVGIAv2Nxqa55tIJTXHdfM/5Vvig/xd5FjW7Y+HzJ3pOv3VCZ5RM9oNFPpyZDBeLvHs53SP/2IxD/sh4dGcqms7nz2be8aAKtvBVO/68J0oqnaVgRLkNi1cdEM1tPpx5AT3fc4Lq1yXa/XlwoPmLu3HCmg7RyedEFyxUGCRazHMPjl6QEz/tMNh1iWa6Bwfeu1RMBlt1C/km/XCIsph51Q7TM6L6kjXSINEbQs05QE7oeVYi1yWaP5h5YiRzNvtC/u3t56+8/cMhSg695W3BuczH29tH3iaksxNuBJPWeO/BOUG0hHVIVyZ6YFiqVG0Xdv9RyBeL77fOCYfoLZ2J7eZZpDbXJT+yVabHpHPdzxIzBS+4JUAzU924LtFfB3fPN8m41dFWJmremRMMtNEmI2G3Ua5KrU1Add829CKPLKd9ope5NtHeJPDz7m1J8cgsJSOadeo8QJT7oLbWIZqlyfxEF/l7dhXWkp0Ty8mViWbzgdG4a9TZUVv57uyZ9iBR1rQdos03Lkg05ygB8zx0V4Oby+Zvl/4cTN6c5Q5UXReucWdN1Wll7/akj59ol5F3iLJZER/Rgvv1ya/T2fxhK/nTeMhXF9nFTZ76gDnX2BRtrbV/sH7XIZrrdrt3Zbuvvq+SH09Lx452KehFym7fy/TCK4Gu3fCviOIk95ibsK7z1jtIJzGLjp5m6eEFE39TJug92EowYT/sstHLDDTlrdcY6Ezh/pqTdKYrRYoUKVKkSCaaucw3RO6fCGqmSJEiRYoUceN/02atpCbxyocAAAAASUVORK5CYII=" alt="" title="branson image.png" width="213" height="200"><br></div><div><a href="http://bransoncentre.co/caribbean/">http://bransoncentre.co/caribbean/</a></div><div></div>', 'uploads/2015-09-01/8d9850dc79b522edc55400a5c88608f9.jpg', '', 2, 'page', 'our-partners', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('hallstar_01@yahoo.com', '1b9ad79fea2c819c4cc3cb8fcad8ad1517adb458f96be17d88ec91ca55de2959', '2015-03-11 21:11:42'),
('shanicec@spidercentro.com', 'ed49476b7d6e1cd089d09afb9c93b5baadb1887cf2519fd56af4b0563813eaba', '2016-02-03 09:50:27');

-- --------------------------------------------------------

--
-- Table structure for table `pledges`
--

CREATE TABLE IF NOT EXISTS `pledges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `project_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pledges_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `pledges`
--

INSERT INTO `pledges` (`id`, `user_id`, `amount`, `payment_type`, `created_at`, `updated_at`, `project_id`) VALUES
(1, 20, 50.00, 'paypal', '2015-12-08 23:51:51', '2015-12-08 23:51:51', 8),
(2, 14, 30.00, 'paypal', '2015-12-08 23:53:20', '2015-12-08 23:53:20', 3),
(3, 31, 14.00, 'paypal', '2015-12-09 00:15:09', '2015-12-09 00:15:09', 10),
(4, 12, 30.00, 'paypal', '2015-12-09 00:16:28', '2015-12-09 00:16:28', 11),
(5, 9, 80.00, 'paypal', '2015-12-09 01:22:27', '2015-12-09 01:22:27', 7),
(6, 12, 50.00, 'paypal', '2015-12-09 22:12:42', '2015-12-09 22:12:42', 1),
(7, 1, 56.00, 'paypal', '2015-12-09 22:29:10', '2015-12-09 22:29:10', 3),
(8, 31, 80.00, 'paypal', '2015-12-09 22:46:58', '2015-12-09 22:46:58', 10),
(9, 31, 10.00, 'paypal', '2015-12-10 04:31:48', '2015-12-10 04:31:48', 1),
(10, 31, 5.00, 'paypal', '2015-12-10 04:32:38', '2015-12-10 04:32:38', 1),
(11, 31, 50.00, 'paypal', '2015-12-10 05:27:41', '2015-12-10 05:27:41', 1),
(12, 31, 35.00, 'paypal', '2015-12-10 05:38:43', '2015-12-10 05:38:43', 7),
(13, 1, 15.00, 'paypal', '2015-12-10 05:56:15', '2015-12-10 05:56:15', 1),
(14, 9, 20.00, 'paypal', '2015-12-10 05:57:11', '2015-12-10 05:57:11', 11),
(15, 31, 25.00, 'paypal', '2015-12-11 05:38:51', '2015-12-11 05:38:51', 1),
(16, 10, 50.00, 'paypal', '2015-12-11 05:40:07', '2015-12-11 05:40:07', 7),
(17, 14, 15.00, 'paypal', '2015-12-11 05:41:17', '2015-12-11 05:41:17', 11),
(18, 12, 35.00, 'paypal', '2015-12-11 05:42:08', '2015-12-11 05:42:08', 3),
(19, 17, 25.00, 'paypal', '2015-12-11 05:42:51', '2015-12-11 05:42:51', 4),
(20, 18, 40.00, 'paypal', '2015-12-11 05:43:54', '2015-12-11 05:43:54', 4),
(21, 15, 8.00, 'paypal', '2015-12-11 05:44:38', '2015-12-11 05:44:38', 4),
(22, 12, 17.00, 'paypal', '2015-12-11 05:57:36', '2015-12-11 05:57:36', 8),
(23, 2, 15.00, 'paypal', '2015-12-22 01:42:14', '2015-12-22 01:42:14', 7),
(24, 31, 15.00, 'paypal', '2015-12-22 01:43:29', '2015-12-22 01:43:29', 7),
(25, 31, 15.00, 'paypal', '2015-12-22 01:43:41', '2015-12-22 01:43:41', 7),
(26, 31, 1.00, 'paypal', '2015-12-22 01:58:47', '2015-12-22 01:58:47', 8),
(27, 31, 1.00, 'paypal', '2015-12-22 02:00:44', '2015-12-22 02:00:44', 4),
(28, 31, 1.00, 'paypal', '2015-12-22 02:02:10', '2015-12-22 02:02:10', 10),
(29, 31, 1.00, 'paypal', '2015-12-22 02:03:46', '2015-12-22 02:03:46', 1),
(30, 31, 1.00, 'paypal', '2015-12-22 02:04:49', '2015-12-22 02:04:49', 10),
(31, 31, 1.00, 'paypal', '2015-12-22 02:09:57', '2015-12-22 02:09:57', 1),
(32, 31, 1.00, 'paypal', '2015-12-22 02:12:21', '2015-12-22 02:12:21', 11),
(33, 31, 1.00, 'paypal', '2015-12-22 02:18:42', '2015-12-22 02:18:42', 10),
(34, 31, 1.00, 'paypal', '2015-12-22 02:20:14', '2015-12-22 02:20:14', 11),
(35, 31, 1.00, 'paypal', '2015-12-22 02:22:50', '2015-12-22 02:22:50', 4),
(36, 31, 1.00, 'paypal', '2015-12-22 02:23:39', '2015-12-22 02:23:39', 3),
(37, 31, 1.00, 'paypal', '2015-12-22 02:24:53', '2015-12-22 02:24:53', 3),
(38, 31, 1.00, 'paypal', '2015-12-22 02:26:13', '2015-12-22 02:26:13', 7),
(39, 31, 1.00, 'paypal', '2015-12-22 02:26:59', '2015-12-22 02:26:59', 8),
(40, 31, 1.00, 'paypal', '2015-12-22 02:28:15', '2015-12-22 02:28:15', 10),
(41, 31, 1.00, 'paypal', '2015-12-22 02:29:36', '2015-12-22 02:29:36', 8),
(42, 31, 1.00, 'paypal', '2015-12-22 02:30:35', '2015-12-22 02:30:35', 7),
(43, 31, 1.00, 'paypal', '2015-12-22 02:31:37', '2015-12-22 02:31:37', 11),
(44, 31, 1.00, 'paypal', '2015-12-22 02:34:17', '2015-12-22 02:34:17', 3),
(45, 31, 1.00, 'paypal', '2015-12-22 02:36:29', '2015-12-22 02:36:29', 4),
(46, 20, 5.00, 'paypal', '2016-02-02 21:56:44', '2016-02-02 21:56:44', 11),
(47, 20, 0.00, 'paypal', '2016-02-02 21:57:12', '2016-02-02 21:57:12', 11),
(48, 20, 5.00, 'paypal', '2016-02-02 22:49:34', '2016-02-02 22:49:34', 11),
(49, 31, 2.00, 'paypal', '2016-02-02 23:17:37', '2016-02-02 23:17:37', 12),
(50, 20, 30.00, 'paypal', '2016-04-13 21:52:19', '2016-04-13 21:52:19', 1),
(51, 20, 2.00, 'paypal', '2016-04-13 22:06:53', '2016-04-13 22:06:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_picks`
--

CREATE TABLE IF NOT EXISTS `project_picks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `project_picks_user_id_foreign` (`user_id`),
  KEY `project_picks_project_id_foreign` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_uploads`
--

CREATE TABLE IF NOT EXISTS `project_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `project_uploads`
--

INSERT INTO `project_uploads` (`id`, `project_id`, `upload_id`, `created_at`, `updated_at`) VALUES
(3, 4, 33, '2015-08-28 03:14:16', '0000-00-00 00:00:00'),
(4, 4, 34, '2015-08-28 03:17:07', '0000-00-00 00:00:00'),
(7, 1, 44, '2015-08-28 19:20:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `donated_amount` decimal(15,2) NOT NULL,
  `need_amount` decimal(15,2) NOT NULL,
  `end_date` date NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_short` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `staff_picked` tinyint(1) NOT NULL,
  `creator_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator_bio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL,
  `views` int(11) NOT NULL,
  `about_project` text COLLATE utf8_unicode_ci NOT NULL,
  `use_of_funds` mediumtext COLLATE utf8_unicode_ci,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_link` mediumtext COLLATE utf8_unicode_ci,
  `social_media` mediumtext COLLATE utf8_unicode_ci,
  `is_project_to_watch` tinyint(1) DEFAULT NULL,
  `is_featured_project` tinyint(1) DEFAULT NULL,
  `previous_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `location`, `donated_amount`, `need_amount`, `end_date`, `user_id`, `description`, `description_short`, `video_url`, `image_url`, `category_id`, `staff_picked`, `creator_name`, `creator_bio`, `creator_location`, `published`, `amount`, `created_at`, `updated_at`, `start_date`, `views`, `about_project`, `use_of_funds`, `state`, `website_link`, `social_media`, `is_project_to_watch`, `is_featured_project`, `previous_state`) VALUES
(1, 'X-RAY MACHINE ', 'St Ann Parish, Jamaica', 124.00, 0.00, '2016-05-28', 1, '<p class="MsoNormal" width="80%" style="line-height: 1.6;">Busy (2020) Helping Hands Foundation has acquired the\r\nundertaking of upgrading the St. Ann''s Bay Hospital''s Imagining Department. Our\r\nfirst project is to purchase an X-Ray Machine. The current X-ray machine has\r\nbeen in the department for over 30 years and is out of service at regular\r\nintervals, which leaves patients to receive this service at private facilities\r\nwhich is often very expensive.  Estimated\r\ncost of the X-Ray Machine $105,000.00 usd.<o:p></o:p></p>', '<p class="MsoNormal">TO PURCHASE AN X-RAY MACHINE FOR THE ST. ANNS BAY HOSPITAL.<o:p></o:p></p>', 'https://www.youtube.com/watch?v=LbzG6OR3rjw', 'images/background-image.png', 2, 0, 'Busy 2020 Helping Hands Foundation Ltd', '<p class="MsoNormal"></p>', '', 1, 5000.00, '2015-03-10 22:57:31', '2017-05-04 05:26:05', '2015-08-24', 392, '', 'TO PURCHASE AN X-RAY MACHINE FOR THE ST. ANNS BAY HOSPITAL.', 'unsuccessful', NULL, NULL, NULL, NULL, 'unsuccessful'),
(2, 'test', 'test', 0.00, 0.00, '2015-12-05', 1, 'test', 'test', 'https://vimeo.com/122941824', '', 10, 0, 'John Brown', 'Mobile app developer specializes in security', '', 0, 800.00, '2015-03-11 01:11:12', '2017-05-04 05:26:08', '2015-02-24', 76, '', 'to put in my pocket', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(3, 'another one hit the dust', 'test', 94.00, 0.00, '2015-02-27', 1, 'test', 'test', 'https://vimeo.com/141740464', 'uploads/2015-03-10/c37a0bc342b032d67a7cdea1efce642e.jpg', 2, 0, 'John Brown', 'Mobile app developer specializes in securitytest test', 'Mandeville, Manchester', 1, 100.00, '2015-03-11 02:18:14', '2017-05-04 05:26:09', '2015-02-23', 109, '', NULL, 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(4, 'Another one hit the dust Tv shows2', 'Kingston', 76.00, 0.00, '2015-12-01', 2, 'Dance competition TV show where dance group compete for prizes and showcase there talents', 'Dance tv show competition where groups of 2 compete for prizes', 'https://www.youtube.com/watch?v=aPxVSCfoYnU', 'uploads/2015-03-10/c37a0bc342b032d67a7cdea1efce642e.jpg', 12, 0, 'James Isaac', 'Recording artist with a degree in arts. Founded the Moves School of modern Arts', '', 1, 10000.00, '2015-03-11 18:45:55', '2017-05-04 05:26:10', '2015-03-15', 152, '', 'for dance', 'archived', NULL, NULL, NULL, 1, 'unsuccessful'),
(7, 'Build the HIVE', 'Manchester ', 132.00, 0.00, '2015-12-28', 1, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent </span>', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent </span>', 'https://www.youtube.com/watch?v=WbMV9qYIXqM', '', 2, 0, 'John Brown', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orc', '', 1, 999.99, '2015-03-18 20:34:34', '2017-05-04 05:26:11', '2015-03-18', 87, '', 'To Buy equipment for the hive', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(8, 'kool no', 'Manchester Rd, Jamaica', 20.00, 0.00, '2015-12-18', 15, 'kool no thats right kool no..... :)', 'kool no is the maddest poem...... :)', '', 'uploads/2015-04-19/100x100_5c8cefb8630da604c3a64d6cace436d7.jpg', 6, 0, 'George Henry', 'kool no...just kool... :)', 'Manchester Parish, Jamaica', 1, 20000.00, '2015-04-19 22:33:56', '2017-05-04 05:26:11', '2015-04-01', 76, '', NULL, 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(9, 'My Art Project ', 'Mandeville, Jamaica', 0.00, 0.00, '2015-11-30', 18, 'L<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span>', '<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span>', 'https://www.youtube.com/watch?v=3qKmDnJVDjs', 'uploads/2015-04-29/100x100_4afa055317adf575d2fa4bffe9743ab8.jpg', 4, 0, 'Ricardo Smith', 'L<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14p', 'Mandeville, Jamaica', 0, 15000.00, '2015-04-29 23:18:53', '2017-05-04 05:26:11', '2015-05-01', 82, '', NULL, 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(10, 'Clean up di Environment', 'Saint Ann Parish, Jamaica', 84.00, 0.00, '2016-02-29', 31, 'A fund that Clean up communities in St Ann, paint cross walks, &nbsp;replace damaged garbage containers in the town areas etc', 'For a brighten Jamaica', '', '', 5, 0, 'Shanice', 'Philanthropist who gives and supports all in need. From the time of birth it has always been such a great and humbled priviviledge to give to those who are in nee', '', 0, 300.00, '2015-12-03 04:04:10', '2017-05-04 05:26:12', '2015-12-01', 50, '', 'Funds will be used for paint, new garbage containers, compensate workers etc', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(11, 'Blown up', 'Falmouth, Jamaica', 48.00, 0.00, '2016-03-31', 30, 'This is ABOUT RECREATING PICTURES IN A NEW WAY. Tsing art ofall forms to enlighten community', 'To enhance our lives', 'http://www.dailymotion.com/video/x3lemjx_7-stars-cut-from-movies_news', '', 4, 0, 'Head start and Friends', 'Philanthrospist, loves to give back in charity', '', 1, 400.00, '2015-12-03 20:43:32', '2017-05-04 05:26:13', '2015-12-02', 93, '', 'To buy paint', 'archived', NULL, NULL, 1, NULL, 'unsuccessful'),
(12, 'Clean up di Environment', 'Port Antonio, Jamaica', 2.00, 0.00, '2016-04-01', 31, 'With this project we will be cleaning up garbage and sewage in the communities in and around Port Antonio', '<font face="Open Sans">Clean air, breathe clean, clean health</font>', 'https://www.youtube.com/watch?v=m3H8d061mro', '', 2, 0, 'Shanice Clarke', 'Philanthropist', '', 1, 800.00, '2015-12-03 23:32:45', '2017-05-04 05:26:13', '2015-12-03', 13, '', 'Money will be used to buy cleaning supplies (gloves, garbage containers) and to pay employees for the clean up', 'archived', NULL, NULL, 0, NULL, 'unsuccessful'),
(13, 'testing 1 2', 'Las Vegas, NV, USA', 0.00, 0.00, '2016-01-01', 30, '<span style="line-height: 1.42857;"><b>trying a ting u zeet, want some money! Sponsor mi nuh</b></span><br>', 'test mi a test', '', '', 4, 0, 'Head start and Friends', 'Philanthrospist, loves to give back in charity', '', 0, 300.00, '2015-12-09 01:35:30', '2017-05-04 05:26:13', '2015-12-01', 17, '', 'to pay my bills n buy food... Affi survive u zeeet', 'archived', NULL, NULL, 0, NULL, 'unsuccessful'),
(14, 'Christmas giving', 'Las Vegas, NV, USA', 0.00, 0.00, '2016-01-04', 30, 'give to we change aid the unfornate during christmas', 'tis the time of giving', '', '', 2, 0, 'Phillip Lindsay', 'Philanthrospist, loves to give back in charity', '', 0, 500.00, '2015-12-15 01:44:26', '2017-05-04 05:26:13', '2015-12-15', 12, '', 'to buy food n clothes for the unfortunate', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(15, 'abc 123', 'Fremont St, Las Vegas, NV, USA', 0.00, 0.00, '2016-04-02', 30, '<p><br></p>', '<p>dfrr</p>', '', 'uploads/2016-03-22/f5697378e2da2394f007131f65b90f13.png', 4, 0, 'Phillip Lindsay', 'Philanthrospist, loves to give back in charity', '', 0, 4557.00, '2016-02-03 03:53:44', '2017-05-04 05:26:13', '2016-02-01', 3, '', '<p><br></p>', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(16, '123 Test ISJ', 'Las Vegas, NV, USA', 0.00, 0.00, '2016-03-16', 30, 'Tad daa', 'Tag tag', '', 'uploads/2016-03-10/9fde644530626f1dc0b90cd28762e866.png', 4, 0, 'Phillip Lindsay', 'Philanthrospist, loves to give back in charity', '', 0, 5000.00, '2016-03-11 00:17:41', '2017-05-04 05:26:14', '2016-03-10', 1, '', 'Halooo', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(17, 'Test ISJ', 'Las Vegas, NV, USA', 0.00, 0.00, '2016-03-31', 30, '<p><br></p>', '<p><br></p>', '', '', 4, 0, 'John Bigalow', '', '', 1, 300.00, '2016-03-11 02:47:25', '2017-05-04 05:26:14', '2016-03-23', 3, '', '<p><br></p>', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(18, 'TEST', 'Kingston, ON, Canada', 0.00, 0.00, '2016-04-29', 30, 'tah da da daaa', 'TEST', '', 'uploads/2016-03-22/571035272c3ed6b5a13153997467aad0.jpg', 11, 0, 'Phillip Lindsay', 'hfdjhgkb', '', 0, 1150.00, '2016-03-22 22:36:02', '2017-05-04 05:26:07', '2016-03-22', 1, '', 'yaaaa loooo', 'unsuccessful', NULL, NULL, NULL, NULL, 'unsuccessful'),
(19, 'Love is', 'Kingston, ON, Canada', 0.00, 0.00, '2016-04-30', 32, 'Love in our community', 'Love is, just more to life than just words', '', '', 6, 0, 'Shanice DiMaria', 'Love expert andphilanthropist', '', 0, 500.00, '2016-03-24 22:27:50', '2017-05-04 05:26:07', '2016-03-26', 1, '', 'To make concerts of love', 'unsuccessful', NULL, NULL, NULL, NULL, 'unsuccessful'),
(20, 'Testing', 'Kinley Dr, Nellis AFB, NV 89191, USA', 0.00, 0.00, '2016-03-31', 32, 'rftygukhil', 'gchvbk', '', '', 4, 0, 'Shanice DiMaria', 'Love expert andphilanthropist', '', 0, 30.00, '2016-03-24 22:51:40', '2017-05-04 05:26:15', '2016-03-30', 0, '', 'mghc,vhjb/lkn', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(21, 'ta da gdsaaa', 'Los Angeles, CA, USA', 0.00, 0.00, '2016-04-09', 32, 'jgckvhbkj', 'fchgvjhbjk.b ', '', '', 4, 0, 'Shanice DiMaria', 'Love expert andphilanthropist', '', 0, 8000.00, '2016-03-24 23:55:52', '2017-05-04 05:26:15', '2016-03-30', 1, '', ',hgv,jhvbjhb k,', 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(22, 'w', 'Washington, USA', 0.00, 0.00, '2016-04-01', 32, '', 'w', '', '', 4, 0, '', '', '', 0, 0.56, '2016-03-30 23:54:39', '2017-05-04 05:26:15', '2016-03-31', 0, '', NULL, 'archived', NULL, NULL, NULL, NULL, 'unsuccessful'),
(23, 'Tired of this', 'Kingston, Jamaica', 0.00, 0.00, '2016-10-31', 30, 'tadaaa', 'Its about this get ship', '', '', 4, 0, 'Phillip Lindsay', 'hfdjhgkb', '', 0, 500.00, '2016-04-16 21:44:03', '2017-05-04 05:26:07', '2016-04-16', 1, '', 'To spend!!!!!!!!!', 'unsuccessful', NULL, NULL, NULL, NULL, 'unsuccessful'),
(24, 'Test For da Last Time', 'Kingston, Jamaica', 0.00, 0.00, '2016-07-29', 30, 'heelpo', 'jrfcghkvkjhbnljn;km', '', '', 9, 0, 'Phillip Lindsay', 'hfdjhgkb', '', 0, 500.00, '2016-04-19 21:56:27', '2017-05-04 05:26:08', '2016-04-20', 1, '', 'hello', 'unsuccessful', NULL, NULL, NULL, NULL, 'unsuccessful');

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE IF NOT EXISTS `rewards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `estimated_delivery_time` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rewards_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `rewards`
--

INSERT INTO `rewards` (`id`, `project_id`, `amount`, `description`, `created_at`, `updated_at`, `estimated_delivery_time`) VALUES
(1, 1, 50.00, '5% t the first investor who invest 50 or more', '2015-03-10 22:59:28', '2015-03-10 22:59:28', '2015-04-30'),
(2, 4, 100.00, 'Become one of the judes', '2015-03-11 18:46:35', '2015-03-11 18:46:35', '2015-03-08'),
(4, 7, 50.00, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent&nbsp;</span>', '2015-03-18 20:34:55', '2015-03-18 20:34:55', '2015-04-01'),
(5, 9, 0.00, '<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer to</span>', '2015-04-29 23:21:48', '2015-04-29 23:21:48', '2015-05-15'),
(6, 10, 300.00, 'We aim to succeed', '2015-12-03 09:43:48', '2015-12-03 09:43:48', '2016-02-25'),
(7, 11, 400.00, 'My pledge your pledge', '2015-12-03 20:47:23', '2015-12-03 20:47:23', '2016-04-06'),
(8, 12, 40.00, 'Beginners pledge', '2015-12-03 23:38:22', '2015-12-03 23:38:22', '2016-01-01'),
(9, 18, 50.00, '<p>jgfdcgfjc</p>', '2016-03-22 22:44:44', '2016-03-22 22:44:44', '2016-03-24'),
(13, 13, 50.00, '<p>ttghhjj</p>', '2016-03-24 20:43:51', '2016-03-24 20:43:51', '2016-03-29'),
(14, 15, 50.00, '<p>fjgvkhb</p>', '2016-03-24 22:05:11', '2016-03-24 22:05:11', '2016-03-29'),
(16, 15, 7.00, '<p>hgchgvjhbk.l</p>', '2016-03-24 22:15:41', '2016-03-24 22:15:41', '2016-03-26'),
(17, 19, 5.00, '<p>Start it off good</p>', '2016-03-24 22:30:07', '2016-03-24 22:30:07', '2016-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`) VALUES
(1, 'What was the colour of your first car'),
(2, 'Where was your wedding reception held'),
(3, 'What is the name of your favourite Football team'),
(4, 'Where were you born'),
(5, 'What is the first name of your best friend'),
(6, 'Where is your mother''s birthplace'),
(7, 'Which high school did you attend'),
(8, 'What is the name of your favourite place to visit'),
(9, 'What is the name of your favourite pet'),
(10, 'Where did you go on your honeymoon'),
(11, 'Which parish were you born'),
(12, 'Where is your father''s birthplace');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_path` text COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checked` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `image_path`, `caption`, `url`, `checked`, `created_at`, `updated_at`) VALUES
(1, 'ISJ Banner 2 (1).png', '', '/home', 1, '2016-03-11 23:11:41', '2016-03-11 23:11:41'),
(2, 'children.jpg', '', 'http://localhost:8000/campaign/ISJPRJ0011/blown-up.html', 1, '2016-03-24 21:28:06', '2016-03-24 21:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

CREATE TABLE IF NOT EXISTS `updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `updates_project_id_foreign` (`project_id`),
  KEY `updates_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `call_to_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=90 ;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `call_to_action`, `created_at`, `updated_at`, `dir`, `project_id`) VALUES
(7, 'ccfe16ab7230a4fbb2134f3f4ebeb1d8.png', '2015-03-07 22:29:26', '2015-03-07 22:29:26', 'uploads/2015-03-07/', 0),
(8, 'c37a0bc342b032d67a7cdea1efce642e.jpg', '2015-03-10 22:12:03', '2015-03-10 22:12:03', 'uploads/2015-03-10/', 0),
(9, 'c37a0bc342b032d67a7cdea1efce642e.jpg', '2015-03-10 22:15:15', '2015-03-10 22:15:15', 'uploads/2015-03-10/', 0),
(10, 'fdd15cce365d2fe832fc9963969497bd.jpg', '2015-03-15 23:06:23', '2015-03-15 23:06:23', 'uploads/2015-03-15/', 0),
(11, '755b662e71cb719c0d2afda72b7df4c7.png', '2015-03-16 07:31:27', '2015-03-16 07:31:27', 'uploads/2015-03-16/', 0),
(12, '08dfd5cb049d158f1ec04132702a97a3.jpg', '2015-03-19 22:33:02', '2015-03-19 22:33:02', 'uploads/2015-03-19/', 0),
(13, '3ba62f092399f7de4b853de20337ca83.jpg', '2015-03-20 02:00:46', '2015-03-20 02:00:46', 'uploads/2015-03-19/', 0),
(14, 'ab1afe0e2111f348028812ec2c50b48a.png', '2015-04-12 19:26:10', '2015-04-12 19:26:10', 'uploads/2015-04-12/', 0),
(15, '5c8cefb8630da604c3a64d6cace436d7.jpg', '2015-04-19 22:33:52', '2015-04-19 22:33:52', 'uploads/2015-04-19/', 0),
(16, '4afa055317adf575d2fa4bffe9743ab8.jpg', '2015-04-29 23:18:46', '2015-04-29 23:18:46', 'uploads/2015-04-29/', 0),
(17, '73f945b302c4c8685678f964e2d2033b.png', '2015-06-22 20:54:52', '2015-06-22 20:54:52', 'uploads/2015-06-22/', 0),
(18, 'de1c9fc806869c70f767145674eb406e.jpg', '2015-07-14 00:53:16', '2015-07-14 00:53:16', 'uploads/2015-07-13/', 0),
(19, '3688e7557bd7f438809f3f4ff9b4b526.jpg', '2015-07-14 00:56:42', '2015-07-14 00:56:42', 'uploads/2015-07-13/', 0),
(20, 'c365181892bb53d1f8c23f762d5c071b.png', '2015-07-14 01:04:28', '2015-07-14 01:04:28', 'uploads/2015-07-13/', 0),
(21, 'e92eb2f9bbe5336c13818c3eabceb898.png', '2015-07-23 01:41:55', '2015-07-23 01:41:55', 'uploads/2015-07-22/', 0),
(22, 'e3939d2f3f2c758442937b5da5027674.png', '2015-07-23 01:45:48', '2015-07-23 01:45:48', 'uploads/2015-07-22/', 0),
(23, '948af6489a8de6e066e1ffb9ef9bec0e.jpg', '2015-07-23 07:44:39', '2015-07-23 07:44:39', 'uploads/2015-07-23/', 0),
(24, '9fa49f67f34296a8854c68e9e7e61e0e.png', '2015-08-03 23:40:06', '2015-08-03 23:40:06', 'uploads/2015-08-03/', 0),
(25, '96dd20bf7b8596ba5badd364f235e457.png', '2015-08-04 00:10:23', '2015-08-04 00:10:23', 'uploads/2015-08-03/', 0),
(26, '7e6d76b22f12eafb7ffb66114009e1aa.jpg', '2015-08-04 00:11:04', '2015-08-04 00:11:04', 'uploads/2015-08-03/', 0),
(27, 'a289f1841a47f25ace34d825c7a3ba4c.png', '2015-08-19 01:40:37', '2015-08-19 01:40:37', 'uploads/2015-08-18/', 0),
(28, '8e7886c2d44de9794b9b02b7742231a0.png', '2015-08-25 08:53:43', '2015-08-25 08:53:43', 'uploads/2015-08-25/', 0),
(29, 'a41fa8e12b69782b75f8b177b3413a09.jpg', '2015-08-25 14:06:00', '2015-08-25 14:06:00', 'uploads/2015-08-25/', 0),
(30, '7a38d1e94d5c71349130b4276d10d227.jpg', '2015-08-25 16:19:20', '2015-08-25 16:19:20', 'uploads/2015-08-25/', 0),
(31, '313ba58e516609b8d2f634b67c9b6c23.png', '2015-08-25 16:36:06', '2015-08-25 16:36:06', 'uploads/2015-08-25/', 0),
(32, 'c565b780b6d03d9d63c73ef7586a509d.jpg', '2015-08-25 16:40:34', '2015-08-25 16:40:34', 'uploads/2015-08-25/', 0),
(33, '77b15d87c53fc0fd6e7d6b630622528d.png', '2015-08-28 02:14:16', '2015-08-28 02:14:16', 'uploads/2015-08-27/', 0),
(34, '17fb80ba1cc641708e651fdc2ce021ec.png', '2015-08-28 02:17:07', '2015-08-28 02:17:07', 'uploads/2015-08-27/', 0),
(35, '89fe46a43292f232fba80e4f533d388d.jpg', '2015-08-28 02:18:22', '2015-08-28 02:18:22', 'uploads/2015-08-27/', 0),
(36, '5c353078e43450caf4f0c6c73a6f2568.jpg', '2015-08-28 13:00:43', '2015-08-28 13:00:43', 'uploads/2015-08-28/', 0),
(37, '7225a5de57dea5b18f849ea4a10ee141.png', '2015-08-28 13:42:29', '2015-08-28 13:42:29', 'uploads/2015-08-28/', 0),
(38, '76110d1f5f6ba7add9cc60e354990ce3.jpg', '2015-08-28 14:43:21', '2015-08-28 14:43:21', 'uploads/2015-08-28/', 0),
(39, 'a14910533fd418b7f1b4901c88187ced.jpg', '2015-08-28 17:41:33', '2015-08-28 17:41:33', 'uploads/2015-08-28/', 0),
(40, 'b0ceedf752f70069691416be8a310365.jpg', '2015-08-28 17:52:14', '2015-08-28 17:52:14', 'uploads/2015-08-28/', 0),
(41, '1350d0fcbaaa2d7b9cbc0af6f5d3ded8.png', '2015-08-28 18:02:40', '2015-08-28 18:02:40', 'uploads/2015-08-28/', 0),
(42, 'b7495f44019c135b2806fba1fd631664.jpg', '2015-08-28 18:08:00', '2015-08-28 18:08:00', 'uploads/2015-08-28/', 0),
(43, 'cbaab00d74767a2f031626472ac51825.jpg', '2015-08-28 18:11:49', '2015-08-28 18:11:49', 'uploads/2015-08-28/', 0),
(44, 'c172ba026a8ea2c8337b64cb64c20f52.jpg', '2015-08-28 18:20:53', '2015-08-28 18:20:53', 'uploads/2015-08-28/', 0),
(45, '20d42881ee4d99d14c9dad32bd4243a7.png', '2015-08-28 19:01:47', '2015-08-28 19:01:47', 'uploads/2015-08-28/', 0),
(46, '56f277f5e32c2ecfb16c9c105ed5b8b8.png', '2015-08-28 19:08:11', '2015-08-28 19:08:11', 'uploads/2015-08-28/', 0),
(47, 'd4a22bd073ff9cba2cfb90f9f44b3371.jpg', '2015-08-28 19:12:12', '2015-08-28 19:12:12', 'uploads/2015-08-28/', 0),
(48, 'f33b677941e65249a818a20bceaac25b.png', '2015-08-28 19:19:58', '2015-08-28 19:19:58', 'uploads/2015-08-28/', 0),
(49, 'ca72977d3d16f8fa5acca37931c3d58b.jpg', '2015-08-28 19:52:49', '2015-08-28 19:52:49', 'uploads/2015-08-28/', 0),
(50, '7a608754c04bd8c4b3eba4ac89c09442.jpg', '2015-08-28 21:15:36', '2015-08-28 21:15:36', 'uploads/2015-08-28/', 0),
(51, '7c1dfa3fcc34e8867012ea4a3e06a17f.jpg', '2015-08-28 21:23:38', '2015-08-28 21:23:38', 'uploads/2015-08-28/', 0),
(52, '7f0216b02a53436c7ada71d4aa89cfa7.jpg', '2015-08-29 04:01:51', '2015-08-29 04:01:51', 'uploads/2015-08-29/', 0),
(53, '984e5d5dcd8f1b66c775ccfc8786b103.jpg', '2015-08-29 08:54:57', '2015-08-29 08:54:57', 'uploads/2015-08-29/', 0),
(54, '23930903398a520da680e762d4eca8b8.png', '2015-08-29 08:57:19', '2015-08-29 08:57:19', 'uploads/2015-08-29/', 0),
(55, '731d08c906a1d5b10a712f84e616658e.jpg', '2015-08-30 17:20:21', '2015-08-30 17:20:21', 'uploads/2015-08-30/', 0),
(56, 'a7ce23bb376e7f9e20a036f609056a2f.jpg', '2015-08-30 17:25:21', '2015-08-30 17:25:21', 'uploads/2015-08-30/', 0),
(57, '58b33a9971566795a8bf571c9e5625d5.jpg', '2015-08-30 17:34:24', '2015-08-30 17:34:24', 'uploads/2015-08-30/', 0),
(58, 'be2937d2c2958f178fab0ca46544f202.jpg', '2015-08-30 17:40:10', '2015-08-30 17:40:10', 'uploads/2015-08-30/', 0),
(59, '44dbe666e6a1fee635bfee593129175b.jpg', '2015-08-30 17:43:11', '2015-08-30 17:43:11', 'uploads/2015-08-30/', 0),
(60, '7389563d085ba17fd78079ecd12dd557.jpg', '2015-08-30 17:48:03', '2015-08-30 17:48:03', 'uploads/2015-08-30/', 0),
(61, 'df28e2242e206d869571acd43e2d4f6e.jpg', '2015-08-30 17:49:28', '2015-08-30 17:49:28', 'uploads/2015-08-30/', 0),
(62, '85bf11ccff5dd5d090d33b158d0c8057.jpg', '2015-08-30 17:57:37', '2015-08-30 17:57:37', 'uploads/2015-08-30/', 0),
(63, 'b15d8e28f1e067ce58eadec89300f130.jpg', '2015-08-30 17:59:22', '2015-08-30 17:59:22', 'uploads/2015-08-30/', 0),
(64, 'd63f5e6f5d23d272c158a67d3161877d.jpg', '2015-08-31 00:50:13', '2015-08-31 00:50:13', 'uploads/2015-08-30/', 0),
(65, '89fc0dca27527e75b532348553b186d1.png', '2015-08-31 00:55:02', '2015-08-31 00:55:02', 'uploads/2015-08-30/', 0),
(66, 'eec198b6d189f023ad17c54e0e939452.jpg', '2015-08-31 01:10:39', '2015-08-31 01:10:39', 'uploads/2015-08-30/', 0),
(67, 'a335ffd9dbbcfa3cdce6babbb4d02c54.jpg', '2015-08-31 01:12:42', '2015-08-31 01:12:42', 'uploads/2015-08-30/', 0),
(68, 'd8c17f80d49f1ed5e90d581fb5b967fb.jpg', '2015-08-31 01:33:15', '2015-08-31 01:33:15', 'uploads/2015-08-30/', 0),
(69, '8d9850dc79b522edc55400a5c88608f9.jpg', '2015-09-01 19:46:13', '2015-09-01 19:46:13', 'uploads/2015-09-01/', 0),
(70, '41fc594114ddfa5ace22e02eeb1f8ca0.jpg', '2015-09-01 19:48:42', '2015-09-01 19:48:42', 'uploads/2015-09-01/', 0),
(71, 'e1b0e1ece31c392acacd288e48c2ffbf.jpg', '2015-09-02 14:44:06', '2015-09-02 14:44:06', 'uploads/2015-09-02/', 0),
(72, '4d22d8573fddcef84cf8a283489df231.jpg', '2015-09-02 14:47:15', '2015-09-02 14:47:15', 'uploads/2015-09-02/', 0),
(73, '352d3dd39b00197fda3440e27cefb0da.jpg', '2015-09-02 14:54:41', '2015-09-02 14:54:41', 'uploads/2015-09-02/', 0),
(74, 'd16b3e963e6c518069d0c32314e5fb97.jpg', '2015-09-02 14:57:39', '2015-09-02 14:57:39', 'uploads/2015-09-02/', 0),
(75, 'b7199e3f39846d2bc1189415353e5863.jpg', '2015-09-02 14:59:02', '2015-09-02 14:59:02', 'uploads/2015-09-02/', 0),
(76, 'b1e81df5c588a60a14e9a325db96a4eb.jpg', '2015-09-02 15:01:29', '2015-09-02 15:01:29', 'uploads/2015-09-02/', 0),
(77, '03588e3c30c8772f97c83463ef95a455.jpg', '2015-09-02 15:01:55', '2015-09-02 15:01:55', 'uploads/2015-09-02/', 0),
(78, '4e4d8b3ed25dd8461a02a836f9db5f10.jpg', '2015-09-02 15:18:04', '2015-09-02 15:18:04', 'uploads/2015-09-02/', 0),
(79, 'd7996463db3fa5d777347cf3a50d288b.jpg', '2015-09-03 00:24:17', '2015-09-03 00:24:17', 'uploads/2015-09-02/', 0),
(80, 'background-image.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'resources/images/', 0),
(81, 'b61de1e9dd96b8f0a5923cb0262f7901.jpg', '2016-03-11 00:09:12', '2016-03-11 00:09:12', 'uploads/2016-03-10/', 0),
(82, '9b23a8b47e68596f2ffb8c6c57202226.png', '2016-03-11 00:16:27', '2016-03-11 00:16:27', 'uploads/2016-03-10/', 0),
(83, '9fde644530626f1dc0b90cd28762e866.png', '2016-03-11 00:16:50', '2016-03-11 00:16:50', 'uploads/2016-03-10/', 0),
(84, 'f5697378e2da2394f007131f65b90f13.png', '2016-03-22 23:52:43', '2016-03-22 23:52:43', 'uploads/2016-03-22/', 0),
(85, '571035272c3ed6b5a13153997467aad0.jpg', '2016-03-23 00:06:43', '2016-03-23 00:06:43', 'uploads/2016-03-22/', 0),
(86, 'a8fff358ef1109fba51a0cfbab28e9fe.png', '2016-04-20 21:58:25', '2016-04-20 21:58:25', 'uploads/2016-04-20/', 0),
(87, '2c7fda42bf46a2a896521cfdcc181310.png', '2016-04-20 22:33:14', '2016-04-20 22:33:14', 'uploads/2016-04-20/', 0),
(88, 'c8a5e91d626fc539d8b643723eed58ad.png', '2016-04-22 02:21:04', '2016-04-22 02:21:04', 'uploads/2016-04-21/', 0),
(89, '62875f43d652c309e0ec2342ec9cadee.jpg', '2016-04-26 23:59:37', '2016-04-26 23:59:37', 'uploads/2016-04-26/', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mr',
  `email_validate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_email` tinyint(1) NOT NULL DEFAULT '0',
  `security_question1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_question2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_question3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('regular','admin','supervisor') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'regular',
  `website_link` mediumtext COLLATE utf8_unicode_ci,
  `social_media` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `avatar`, `facebook_id`, `remember_token`, `created_at`, `updated_at`, `title`, `email_validate`, `confirmed_email`, `security_question1`, `security_question2`, `security_question3`, `security_answer1`, `security_answer2`, `security_answer3`, `level`, `website_link`, `social_media`) VALUES
(1, 'Romarioh', 'Hall', 'hallstar01@yahoo.com', '$2y$10$HNoHHs9Z17KeeQC0Te7E9.PIJoWEDVmbh5/E22vJP3DR1jhohb1lW', 'uploads/2015-03-19/08dfd5cb049d158f1ec04132702a97a3.jpg', '', 'FAfezgSfZ0mvCFRWMBJGhgIoGmvkINswdBr5bMF2MXt00FS0aEIJE0YrElGs', '2015-03-03 05:15:13', '2016-04-13 21:46:54', 'mr', 'f551cc6cfe57cdee0d9c690bfb99b8c8', 0, 'What was the colour of your first car', 'Where were you born', 'What is the first name of your best friend', 'red', 'hospital', 'rushio', 'supervisor', 'www.shanicejclarke.16mb.com', 'www.facebook.com|||'),
(2, 'Phillip', 'Lindsay', 'hallstar01@gmail.com', '$2y$10$qCsIzMp9.AxMYJUJH7Ft0OUADmPnL9KujdnaiQSOP5HeIjaKA0ZBS', 'uploads/2015-08-25/313ba58e516609b8d2f634b67c9b6c23.png', '', 'Ls51NIqmYj7vUuRgayHuvHV9XUQtjM0rFW70F7G4chS9V6bdiixBnFdG4DrC', '2015-03-11 04:04:17', '2015-12-13 03:26:11', 'mr', 'c5a538befc1ec7d635b186944b91b6f0', 0, 'What was the colour of your first car', 'What is the name of your favourite Football team', 'Where were you born', 'white', 'arsenal', 'kingston', 'supervisor', '', '|||'),
(9, 'Rushio', 'Billings', 'rushiobillings@gmail.com', '$2y$10$N.5cpkedLai1siYXzRCVqehHxOkNsIyPRnHpvaPIEU6X8TkOX1WES', '', '', 'dZgyd4Yr3aPEo7jUQJgOC5CMfPgQ0giNiLoqmIfzSNYZLCWFuUhUg1ywRsWv', '2015-03-11 21:25:34', '2015-03-12 02:27:26', 'mr', '311aec1396b094572b35c49a74d3b692', 0, '', '', '', '', '', '', 'admin', NULL, NULL),
(10, 'Rushio', 'Billings', 'rushiobillings@yahoo.com', '$2y$10$bmod/nLnYSlMrttLz9KTjODaH487SqVl3IghtCFkLKUITLQwvj0iC', 'uploads/2015-08-18/a289f1841a47f25ace34d825c7a3ba4c.png', '10204798496177033', 'Nb9Gc04nZAvn7Bc6v5mg8Nc2Oej10ULf5VyFitSiv4rfsTWop84C7mdbcvtA', '2015-03-19 22:50:57', '2015-08-19 01:40:53', '', '', 0, 'What was the colour of your first car', 'Where were you born', 'What is the name of your favourite place to visit', 'hello', 'hi', '3434', 'admin', NULL, NULL),
(11, 'Kornel-Sims', 'Lewis', 'cornel_lewis@yahoo.com', '', 'uploads/2015-03-19/3ba62f092399f7de4b853de20337ca83.jpg', '1067615696585862', NULL, '2015-03-20 01:59:50', '2016-03-11 23:36:17', '', '', 0, 'Where were you born', 'What is the first name of your best friend', 'Which parish were you born', '123456', '123535', '2222', 'regular', NULL, NULL),
(12, 'Anquane Ann', 'Campbell', 'tajwane@gmail.com', '', 'https://graph.facebook.com/v2.2/715813185206475/picture?type=normal', '715813185206475', NULL, '2015-03-25 02:17:31', '2016-03-22 23:17:01', '', '', 0, 'What was the colour of your first car', 'Where was your wedding reception held', 'What is the name of your favourite Football team', 'blue', 'mandeville', 'barcelona', 'regular', NULL, NULL),
(13, 'Romario', 'Hall', 'hallstar_01@yahoo.com', '', 'https://graph.facebook.com/v2.2/10206202194436344/picture?type=normal', '10206202194436344', 'QCpnTvIxzmnunm3QJUZetyNcwjXyK0jTXAHVi3y6GhMrG3YuvE0A9xujN8kY', '2015-03-25 18:23:54', '2015-03-25 20:47:42', 'mr', '', 0, 'What was the colour of your first car', 'Where were you born', 'Which parish were you born', 'Blue', 'university hospital', 'kingston', 'regular', NULL, NULL),
(14, 'Michael', 'Claire', 'mwclaire@msn.com', '', 'https://graph.facebook.com/v2.2/10153134065208818/picture?type=normal', '10153134065208818', NULL, '2015-03-27 02:22:05', '2015-03-27 02:22:28', 'mr', '', 0, 'Where was your wedding reception held', 'What is the first name of your best friend', 'Where were you born', 'xcds', 'fhhhd', 'skj;sj;j;a', 'regular', NULL, NULL),
(15, 'George', 'Henry', 'kgkinggeorge@yahoo.com', '$2y$10$WIPXPvOMtKct77JBqFB4GOfhyTUgdtqVBcoU3BWtyFNclgq4dUWPC', '', '', 'Ch9fAxYb7MvCSa3oNM3JF2hlldYefMLA9JjbAwQ9IG45YJYuLpcEv1TMesJC', '2015-04-17 17:17:14', '2015-04-19 22:36:33', 'mr', '9bab9a4ab781ab5dd7492cad6d8f6376', 0, 'Where were you born', 'What is the name of your favourite pet', 'Which parish were you born', 'trelawny', 'dingo', 'trelawny', 'regular', NULL, NULL),
(17, 'dwayne', 'brown', 'dwayneb@jnbs.com', '$2y$10$gVDwyffA2GZtQo0yl5/JdOr17mO3gdZQqRwgLeNPju2EU3ommkAOS', '', '', 'pK1x0FELgB6d0YYzU0XFoGGiwO5TOTgDVvTDDvxbreQw6pAcPZxeL6mqUZjP', '2015-04-29 22:27:26', '2016-03-11 23:51:53', '', '9d55db06cdf99a5b7582ef52423f6118', 0, 'What was the colour of your first car', 'Where were you born', 'What is the name of your favourite pet', 'white', 'kingston', 'dog', 'supervisor', NULL, NULL),
(18, 'Ricardo', 'Smith', 'jodarina1@hotmail.com', '$2y$10$F0pfMIZTqKT4zw6F2d17K.gq9E.l0kZVBPtNM9fDoA4aCkYlahk6O', '', '', NULL, '2015-04-29 23:14:34', '2015-04-29 23:22:59', 'mr', 'aa5a82db3f9ee1122a37707bf2e87b73', 0, 'What was the colour of your first car', 'Where was your wedding reception held', 'What is the name of your favourite Football team', 'Red', 'yard', 'beatles', 'regular', NULL, NULL),
(19, 'George', 'Henry', 'kgkinggeorge@gmail.com', '$2y$10$T2XuHf/Gb2..7TuwjIqIq.fK417r3cV663bMKi/vDDAvd4D1bm37G', '', '', 'VJqlfT0tWeqPaygOpLb1lQt31KnWnVt38N3gBi5UeHGlCdK3s0tU93LWhafd', '2015-05-07 19:33:30', '2015-05-07 19:33:43', 'mr', '5c0a67b0290684390707505e9690e615', 0, '', '', '', '', '', '', 'regular', NULL, NULL),
(20, 'George', 'Baker', 'georgeh@spidercentro.com', '$2y$10$HNoHHs9Z17KeeQC0Te7E9.PIJoWEDVmbh5/E22vJP3DR1jhohb1lW', '', '', 'wFB630Vj84Ne5d4N1LnoROONAV0Lc7lhO9LFAxFyZePc92CWEJbBp8E2uGi8', '2015-05-07 19:34:26', '2015-07-01 23:12:18', 'mr', '772d45303b21d9c1c7d07f3aa6df0e19', 1, 'What was the colour of your first car', 'Where were you born', 'Where was your wedding reception held', 'swift', 'Trelawny', 'mandeville', 'admin', NULL, NULL),
(23, 'dwayne', 'brown', 'crazydwayne@gmail.com', '$2y$10$jma.7fNmo01jYKzqN1FCr.SnVSu9nWZxEBDLj9KJ0U5ES.pd883M.', '', '', 'Qt2w4MKed6NMlVQ97l2guqbDlvI4fg2t6gV5SDaFkJ5LVd9t3gyoQmT9tdp3', '2015-07-01 21:38:36', '2015-09-03 02:51:00', '', 'db5e2f4c673532d97baeb4e53e134763', 1, 'Where were you born', 'Which parish were you born', 'What is the first name of your best friend', 'kingston', 'kingston', 'charlie', 'supervisor', NULL, NULL),
(28, 'Kimberly', 'Manradge', 'kimberlym@spidercentro.com', '$2y$10$L8uLq/l95k6.VncPu6c04.QCHfikm8VrEP7ToffESHdjIt0gSrAKu', '', '', NULL, '2015-08-19 04:39:51', '2015-09-03 02:50:37', '', '6d4b2df3c0c4d2eef00c5fae91c309ad', 1, '', '', '', '', '', '', 'supervisor', NULL, NULL),
(30, 'Phillip', 'Lindsay', 'PLindsay@jnbs.com', '$2y$10$RsoQtaii/oRopsjy2FandO3dcmz1go2Y2OGO/KIg5MUEY48AV4gWO', 'uploads/2016-03-22/571035272c3ed6b5a13153997467aad0.jpg', '', 'mMdXSVsqq2WIhiUJVALrV2EylC8rPKuxmZVs2K0hdKZ0uM86wSTwWcpdzTCM', '2015-09-03 02:52:14', '2016-04-28 21:37:41', 'mr', '195d1245bed337838e75b238617707e9', 1, 'Where were you born', 'Where is your father''s birthplace', 'Where is your mother''s birthplace', 'jamaica', 'jamaica', 'jamaica', 'supervisor', 'www.shanicejclarke.16mb.com', '|ww.twitter.com||'),
(31, 'Shanice', 'Clarke', 'shanicec@spidercentro.com', '$2y$10$4TKaL/CoksPgbfpXCb0Dv.35VMdDDZ8iZCUbNeuuOJvWoWvEWPIO2', '', '', 'JWqsaajM2wHzjUYwNKz2IqxIEx7dYCLlIS30GFot11K1uAV8OuEpzP26KioN', '2015-12-03 00:31:24', '2016-03-11 23:47:13', '', 'a3118cf137686496c258ff9c3a20eb18', 1, 'Where were you born', 'Where is your father''s birthplace', 'What is the first name of your best friend', 'trelawny', 'trelawny', 'mitsue', 'admin', 'http://shanicejclarke.16mb.com/', '|https://twitter.com/shanicejclarke||https://plus.google.com/u/0/113082435575294123727'),
(32, 'Shanice', 'DiMaria', 'shanice_clarke31@yahoo.com', '$2y$10$XLIzyuNK5.SMJfp/3OuFVeFp1SXo40AhnXSNXw1YZJWjOPsdQa1zW', '', '', 'Oi93HK9ENzaiQjKt1pFza2EwE0LIAkhCgFPzPuKVmAOIeCM10zFE5VJ1qVt3', '2016-02-20 02:36:10', '2016-04-13 23:11:01', 'mrs', '', 0, 'Where were you born', 'Where is your mother''s birthplace', 'Where is your father''s birthplace', 'jamaica', 'jamaica', 'jamaica', 'regular', '', '|||'),
(33, 'Ryan', 'DiMaria', 'ryanjdimaria@gmail.com', '$2y$10$NtDSIekBFCA94Let72qH4.n7mxK2Lzk4HCEwNOftR87tSw.G/cDHK', '', '', 'Je4fVapEeezSP1iUi4tHShV4hRzsssSpjANhhIGmlFtISRzdmNmWDEoVHmjg', '2016-03-11 05:39:51', '2016-03-11 05:48:27', 'mr', '', 0, '', '', '', '', '', '', 'regular', NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_follow`
--
ALTER TABLE `category_follow`
  ADD CONSTRAINT `category_follow_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_follow_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_page_section_id_foreign` FOREIGN KEY (`page_section_id`) REFERENCES `page_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pledges`
--
ALTER TABLE `pledges`
  ADD CONSTRAINT `pledges_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_picks`
--
ALTER TABLE `project_picks`
  ADD CONSTRAINT `project_picks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_picks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rewards`
--
ALTER TABLE `rewards`
  ADD CONSTRAINT `rewards_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `updates`
--
ALTER TABLE `updates`
  ADD CONSTRAINT `updates_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `updates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
--
-- Database: `isupportja`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE IF NOT EXISTS `banner_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(85) COLLATE utf8_unicode_ci NOT NULL,
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `upload_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `title`, `banner_caption`, `image_url`, `user_id`, `upload_id`, `created_at`, `updated_at`) VALUES
(1, 'Default Banner', '', 'uploads/2015-03-16/755b662e71cb719c0d2afda72b7df4c7.png', 0, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Default Banner2', '', 'uploads/2015-08-29/23930903398a520da680e762d4eca8b8.png', 0, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `amount` decimal(5,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cart_user_id_foreign` (`user_id`),
  KEY `cart_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `project_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 30, 11, 2.00, '2015-10-01 01:01:39', '2015-10-01 01:01:39');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `homepage` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`, `homepage`) VALUES
(1, 'Technology', '0000-00-00 00:00:00', '2015-09-13 18:53:06', 1),
(2, 'Health', '0000-00-00 00:00:00', '2015-08-05 08:31:13', 3),
(3, 'Music', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Art', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'Science', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'Poetry', '0000-00-00 00:00:00', '2015-08-05 08:31:25', 8),
(7, 'Theatre', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 'Fashion', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(9, 'Games', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, 'Comics', '0000-00-00 00:00:00', '2015-07-24 02:05:56', 0),
(11, 'Crafts', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, 'Dance', '0000-00-00 00:00:00', '2015-08-05 08:31:19', 4);

-- --------------------------------------------------------

--
-- Table structure for table `category_follow`
--

CREATE TABLE IF NOT EXISTS `category_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_follow_user_id_foreign` (`user_id`),
  KEY `category_follow_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=98 ;

--
-- Dumping data for table `category_follow`
--

INSERT INTO `category_follow` (`id`, `user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(4, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 10, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 10, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 10, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 11, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 11, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 11, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 11, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 11, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 11, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 11, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 11, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 11, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 11, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 12, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 12, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 13, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 13, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 14, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 14, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 14, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 14, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 14, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 14, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 15, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 17, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 18, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 18, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 18, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 18, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 18, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 18, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 23, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 30, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 30, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 30, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 30, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 30, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 30, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 30, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 30, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 30, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 30, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 30, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 30, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `google_maps_info`
--

CREATE TABLE IF NOT EXISTS `google_maps_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) NOT NULL,
  `place_id` varchar(255) NOT NULL,
  `log` decimal(10,6) NOT NULL,
  `lat` decimal(10,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `place_id` (`place_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `google_maps_info`
--

INSERT INTO `google_maps_info` (`id`, `location`, `place_id`, `log`, `lat`, `created_at`, `updated_at`) VALUES
(1, 'Manchester Parish, Jamaica', 'ChIJ2WhaWlm4244RRc0ndo73kdY', -77.516079, 18.066965, '2015-04-20 05:35:00', '2015-04-19 18:35:00'),
(2, 'Mandeville, Jamaica', 'ChIJt_eI1DC-244R7dSl7GBNWPo', -77.500000, 18.033333, '2015-04-30 06:18:53', '2015-04-29 19:18:53'),
(3, 'St Ann Parish, Jamaica', 'ChIJe9l9kzVS2o4RllOWGihszuE', -77.240515, 18.328143, '2015-08-25 23:28:10', '2015-08-25 12:28:10'),
(4, 'Jamaica', 'ChIJnXHPxhsq2o4R-g4StcDRoFk', -77.297508, 18.109581, '2015-09-24 23:25:54', '2015-09-24 11:25:54'),
(5, 'Japan', 'ChIJLxl_1w9OZzQRRFJmfNR1QvU', 138.252924, 36.204824, '2015-10-01 12:18:15', '2015-10-01 00:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `heading`
--

CREATE TABLE IF NOT EXISTS `heading` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `heading`
--

INSERT INTO `heading` (`id`, `name`, `section`, `created_at`, `updated_at`) VALUES
(1, 'Staff Picks', 'first_head', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'What''s Popular', 'second_head', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=320 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `action`, `created_at`, `updated_at`) VALUES
(6, 2, 'generate report for registered users', '2015-04-03 01:45:32', '0000-00-00 00:00:00'),
(7, 2, 'download csv report for registered users', '2015-04-03 01:45:47', '0000-00-00 00:00:00'),
(8, 2, 'generate report for donor', '2015-04-03 01:46:28', '0000-00-00 00:00:00'),
(9, 2, 'download csv report for donor', '2015-04-03 01:46:38', '0000-00-00 00:00:00'),
(10, 2, 'generate report for project views', '2015-04-03 01:50:46', '0000-00-00 00:00:00'),
(11, 2, 'generate report for project views', '2015-04-12 01:57:43', '0000-00-00 00:00:00'),
(12, 1, 'generate report for registered users', '2015-04-20 01:07:06', '0000-00-00 00:00:00'),
(13, 1, 'add another one hit the dust to homepage', '2015-04-20 01:30:09', '0000-00-00 00:00:00'),
(14, 1, 'generate report for registered users', '2015-04-20 01:30:46', '0000-00-00 00:00:00'),
(15, 1, 'modified project kool no', '2015-04-20 01:37:13', '0000-00-00 00:00:00'),
(16, 1, 'generate report for fully funded', '2015-04-20 01:37:45', '0000-00-00 00:00:00'),
(17, 1, 'generate report for registered users', '2015-04-20 01:39:06', '0000-00-00 00:00:00'),
(18, 1, 'download pdf all logs', '2015-05-05 23:11:53', '0000-00-00 00:00:00'),
(19, 1, 'download pdf all logs', '2015-05-05 23:13:08', '0000-00-00 00:00:00'),
(20, 1, 'download csv all logs', '2015-05-06 02:39:50', '0000-00-00 00:00:00'),
(21, 20, 'generate report for registered users', '2015-05-08 00:54:10', '0000-00-00 00:00:00'),
(22, 20, 'generate report for registered users', '2015-05-13 23:25:25', '0000-00-00 00:00:00'),
(23, 2, 'generate report for registered users', '2015-05-29 03:01:57', '0000-00-00 00:00:00'),
(24, 17, 'Login into system', '2015-06-30 05:17:55', '0000-00-00 00:00:00'),
(25, 2, 'generate report for registered users', '2015-07-09 04:40:39', '0000-00-00 00:00:00'),
(26, 2, 'edited page home', '2015-07-14 03:51:21', '0000-00-00 00:00:00'),
(27, 2, 'edited page home', '2015-07-14 03:53:21', '0000-00-00 00:00:00'),
(28, 2, 'edited page home', '2015-07-14 03:54:03', '0000-00-00 00:00:00'),
(29, 2, 'edited page home', '2015-07-14 03:54:40', '0000-00-00 00:00:00'),
(30, 2, 'reset page home', '2015-07-14 03:55:18', '0000-00-00 00:00:00'),
(31, 2, 'edited page home', '2015-07-14 03:57:41', '0000-00-00 00:00:00'),
(32, 2, 'reset page home', '2015-07-14 03:57:59', '0000-00-00 00:00:00'),
(33, 2, 'Login into system', '2015-07-15 20:59:00', '0000-00-00 00:00:00'),
(34, 2, 'add Build the HIVE to homepage', '2015-07-15 21:38:58', '0000-00-00 00:00:00'),
(35, 2, 'add Anothr one hit the dust Tv shows2 to homepage', '2015-07-15 21:39:07', '0000-00-00 00:00:00'),
(36, 2, 'add Helpus to homepage', '2015-07-15 21:45:17', '0000-00-00 00:00:00'),
(37, 2, 'add another one hit the dust to homepage', '2015-07-15 21:45:27', '0000-00-00 00:00:00'),
(38, 2, 'add Build the HIVE to homepage', '2015-07-15 21:45:33', '0000-00-00 00:00:00'),
(39, 2, 'add another one hit the dust to homepage', '2015-07-15 21:45:39', '0000-00-00 00:00:00'),
(40, 2, 'create new user account for Kimberly Manradge', '2015-07-23 08:16:36', '0000-00-00 00:00:00'),
(41, 2, 'delete user account for Kimberly Manradge', '2015-07-23 08:21:01', '0000-00-00 00:00:00'),
(42, 2, 'create new user account for Kimberly Manradge', '2015-07-23 08:21:52', '0000-00-00 00:00:00'),
(43, 2, 'delete user account for Kimberly Manradge', '2015-07-23 08:56:33', '0000-00-00 00:00:00'),
(44, 2, 'create new user account for Kimberly Manradge', '2015-07-23 09:00:07', '0000-00-00 00:00:00'),
(45, 2, 'created page Test', '2015-07-23 11:11:30', '0000-00-00 00:00:00'),
(46, 2, 'created page Fast 7 Soundtrack', '2015-07-23 11:24:36', '0000-00-00 00:00:00'),
(47, 2, 'edited page Edited Test', '2015-07-23 19:06:50', '0000-00-00 00:00:00'),
(48, 2, 'delete page Edited Test', '2015-07-23 19:19:02', '0000-00-00 00:00:00'),
(49, 2, 'generate report for registered users', '2015-07-23 21:16:17', '0000-00-00 00:00:00'),
(50, 2, 'generate report for donor', '2015-07-23 21:16:23', '0000-00-00 00:00:00'),
(51, 2, 'generate report for project views', '2015-07-23 21:16:59', '0000-00-00 00:00:00'),
(52, 2, 'generate report for project donation', '2015-07-23 21:17:14', '0000-00-00 00:00:00'),
(53, 2, 'generate report for partial funded', '2015-07-23 21:17:26', '0000-00-00 00:00:00'),
(54, 2, 'generate report for fully funded', '2015-07-23 21:17:32', '0000-00-00 00:00:00'),
(55, 2, 'generate report for donor', '2015-07-23 21:17:37', '0000-00-00 00:00:00'),
(56, 2, 'generate report for registered users', '2015-07-23 21:17:53', '0000-00-00 00:00:00'),
(57, 2, 'add Helpus to homepage', '2015-07-23 22:00:25', '0000-00-00 00:00:00'),
(58, 2, 'add Build the HIVE to homepage', '2015-07-23 22:02:17', '0000-00-00 00:00:00'),
(59, 2, 'generate report for registered users', '2015-07-23 22:29:37', '0000-00-00 00:00:00'),
(60, 2, 'generate report for donor', '2015-07-23 22:29:52', '0000-00-00 00:00:00'),
(61, 2, 'generate report for registered users', '2015-07-23 22:30:06', '0000-00-00 00:00:00'),
(62, 2, 'generate report for donor', '2015-07-23 22:33:32', '0000-00-00 00:00:00'),
(63, 2, 'generate report for fully funded', '2015-07-23 22:34:24', '0000-00-00 00:00:00'),
(64, 2, 'generate report for partial funded', '2015-07-23 22:34:33', '0000-00-00 00:00:00'),
(65, 2, 'generate report for project donation', '2015-07-23 22:34:40', '0000-00-00 00:00:00'),
(66, 2, 'generate report for project views', '2015-07-23 22:35:43', '0000-00-00 00:00:00'),
(67, 2, 'download csv report for project views', '2015-07-23 22:39:16', '0000-00-00 00:00:00'),
(68, 2, 'download pdf report for project views', '2015-07-23 22:40:00', '0000-00-00 00:00:00'),
(69, 2, 'add Helpus to homepage', '2015-07-24 00:10:06', '0000-00-00 00:00:00'),
(70, 2, 'delete user account for anthony lindsay', '2015-08-04 03:42:59', '0000-00-00 00:00:00'),
(71, 2, 'created page Netballers', '2015-08-04 04:28:17', '0000-00-00 00:00:00'),
(72, 2, 'created page Facebook', '2015-08-05 03:11:45', '0000-00-00 00:00:00'),
(73, 2, 'created page Instagram', '2015-08-05 03:12:40', '0000-00-00 00:00:00'),
(74, 2, 'created page YouTube', '2015-08-05 03:15:48', '0000-00-00 00:00:00'),
(75, 2, 'created page Twitter', '2015-08-05 03:16:50', '0000-00-00 00:00:00'),
(76, 2, 'delete page facebook', '2015-08-05 03:17:02', '0000-00-00 00:00:00'),
(77, 2, 'created page Google+ ', '2015-08-05 03:18:16', '0000-00-00 00:00:00'),
(78, 2, 'created page Contact Us', '2015-08-05 03:30:31', '0000-00-00 00:00:00'),
(79, 2, 'delete page Contact Us', '2015-08-05 03:32:59', '0000-00-00 00:00:00'),
(80, 2, 'add Helpus to homepage', '2015-08-05 04:31:03', '0000-00-00 00:00:00'),
(81, 2, 'add another one hit the dust to homepage', '2015-08-05 04:31:13', '0000-00-00 00:00:00'),
(82, 2, 'add Another one hit the dust Tv shows2 to homepage', '2015-08-05 04:31:19', '0000-00-00 00:00:00'),
(83, 2, 'add kool no to homepage', '2015-08-05 04:31:25', '0000-00-00 00:00:00'),
(84, 2, 'generate report for registered users', '2015-08-05 04:32:11', '0000-00-00 00:00:00'),
(85, 2, 'generate report for registered users', '2015-08-05 04:32:11', '0000-00-00 00:00:00'),
(86, 2, 'generate report for registered users', '2015-08-05 04:32:17', '0000-00-00 00:00:00'),
(87, 2, 'generate report for project views', '2015-08-05 04:32:27', '0000-00-00 00:00:00'),
(88, 2, 'generate report for registered users', '2015-08-05 04:56:05', '0000-00-00 00:00:00'),
(89, 2, 'generate report for donor', '2015-08-05 04:56:08', '0000-00-00 00:00:00'),
(90, 2, 'generate report for fully funded', '2015-08-05 04:56:14', '0000-00-00 00:00:00'),
(91, 2, 'generate report for project donation', '2015-08-05 04:56:18', '0000-00-00 00:00:00'),
(92, 2, 'generate report for project views', '2015-08-05 04:56:21', '0000-00-00 00:00:00'),
(93, 2, 'generate report for registered users', '2015-08-05 04:56:59', '0000-00-00 00:00:00'),
(94, 2, 'download pdf report for registered users', '2015-08-05 04:58:20', '0000-00-00 00:00:00'),
(95, 2, 'generate report for registered users', '2015-08-05 04:59:38', '0000-00-00 00:00:00'),
(96, 2, 'download csv report for registered users', '2015-08-05 04:59:57', '0000-00-00 00:00:00'),
(97, 2, 'created page Testing 1 3', '2015-08-17 21:15:42', '0000-00-00 00:00:00'),
(98, 2, 'created page Testing 1 3', '2015-08-17 21:15:49', '0000-00-00 00:00:00'),
(99, 2, 'delete page Testing 1 3', '2015-08-17 21:17:46', '0000-00-00 00:00:00'),
(100, 2, 'delete page Testing 1 3', '2015-08-17 21:17:56', '0000-00-00 00:00:00'),
(101, 2, 'edit user account for Rushio Billings', '2015-08-17 21:19:09', '0000-00-00 00:00:00'),
(102, 2, 'edit user account for Anquani Campbell', '2015-08-17 23:14:58', '0000-00-00 00:00:00'),
(103, 2, 'edit user account for Anquane Campbell', '2015-08-17 23:15:28', '0000-00-00 00:00:00'),
(104, 2, 'delete user account for Kimberly Manradge', '2015-08-17 23:36:11', '0000-00-00 00:00:00'),
(105, 2, 'create new user account for Kimberly Manradge', '2015-08-17 23:36:50', '0000-00-00 00:00:00'),
(106, 2, 'created page Test Page', '2015-08-17 23:44:58', '0000-00-00 00:00:00'),
(107, 2, 'edit user account for Anquane Campbell', '2015-08-19 07:35:49', '0000-00-00 00:00:00'),
(108, 2, 'edit user account for Anquani Campbell', '2015-08-19 07:36:02', '0000-00-00 00:00:00'),
(109, 2, 'edit user account for Anquane Campbell', '2015-08-19 07:36:41', '0000-00-00 00:00:00'),
(110, 2, 'edit user account for Kornel Lewis', '2015-08-19 07:36:56', '0000-00-00 00:00:00'),
(111, 2, 'delete user account for Kimberly Manradge', '2015-08-19 07:37:54', '0000-00-00 00:00:00'),
(112, 2, 'create new user account for Kimberly Manradge', '2015-08-19 07:39:51', '0000-00-00 00:00:00'),
(113, 2, 'generate report for fully funded', '2015-08-21 21:06:42', '0000-00-00 00:00:00'),
(114, 2, 'generate report for project views', '2015-08-21 21:06:48', '0000-00-00 00:00:00'),
(115, 2, 'generate report for donor', '2015-08-21 21:06:54', '0000-00-00 00:00:00'),
(116, 2, 'generate report for registered users', '2015-08-21 21:07:00', '0000-00-00 00:00:00'),
(117, 2, 'generate report for partial funded', '2015-08-21 21:07:07', '0000-00-00 00:00:00'),
(118, 2, 'generate report for project donation', '2015-08-21 21:07:13', '0000-00-00 00:00:00'),
(119, 2, 'delete page Fast 7 Soundtrack', '2015-08-25 21:56:02', '0000-00-00 00:00:00'),
(120, 2, 'created page What is ISupportJamaica?', '2015-08-25 22:04:03', '0000-00-00 00:00:00'),
(121, 2, 'delete page Test Page', '2015-08-25 22:04:39', '0000-00-00 00:00:00'),
(122, 2, 'edited page Faq', '2015-08-25 22:06:27', '0000-00-00 00:00:00'),
(123, 2, 'created page Contact Us', '2015-08-25 22:09:28', '0000-00-00 00:00:00'),
(124, 2, 'edited page Contact Us', '2015-08-25 22:10:49', '0000-00-00 00:00:00'),
(125, 2, 'edited page Contact Us', '2015-08-25 22:12:30', '0000-00-00 00:00:00'),
(126, 2, 'edited page Contact Us', '2015-08-25 22:13:13', '0000-00-00 00:00:00'),
(127, 2, 'created page How It Works', '2015-08-25 22:17:46', '0000-00-00 00:00:00'),
(128, 2, 'create new user account for Phillip Lindsay', '2015-08-25 22:18:17', '0000-00-00 00:00:00'),
(129, 2, 'created page Start A Campaign', '2015-08-25 22:23:27', '0000-00-00 00:00:00'),
(130, 2, 'delete page Earned it', '2015-08-25 22:25:32', '0000-00-00 00:00:00'),
(131, 2, 'edited page Faq', '2015-08-25 22:34:34', '0000-00-00 00:00:00'),
(132, 2, 'created page Background', '2015-08-25 22:44:18', '0000-00-00 00:00:00'),
(133, 2, 'created page Terms and Conditions', '2015-08-25 22:53:30', '0000-00-00 00:00:00'),
(134, 2, 'edited page FAQ', '2015-08-25 23:09:09', '0000-00-00 00:00:00'),
(135, 2, 'edited page FAQ', '2015-08-25 23:21:47', '0000-00-00 00:00:00'),
(136, 2, 'generate report for registered users', '2015-08-27 11:43:06', '0000-00-00 00:00:00'),
(137, 2, 'download pdf report for registered users', '2015-08-27 11:43:19', '0000-00-00 00:00:00'),
(138, 2, 'generate report for registered users', '2015-08-27 11:43:58', '0000-00-00 00:00:00'),
(139, 2, 'generate report for registered users', '2015-08-27 11:44:08', '0000-00-00 00:00:00'),
(140, 2, 'generate report for registered users', '2015-08-27 11:44:10', '0000-00-00 00:00:00'),
(141, 2, 'generate report for registered users', '2015-08-27 11:44:12', '0000-00-00 00:00:00'),
(142, 2, 'edited page Start A Campaign', '2015-08-28 10:05:09', '0000-00-00 00:00:00'),
(143, 2, 'edited page FAQ', '2015-08-28 21:39:53', '0000-00-00 00:00:00'),
(144, 2, 'edited page FAQ', '2015-08-28 21:42:32', '0000-00-00 00:00:00'),
(145, 2, 'Login into system', '2015-08-28 21:43:26', '0000-00-00 00:00:00'),
(146, 2, 'edited page FAQ', '2015-08-28 21:48:40', '0000-00-00 00:00:00'),
(147, 2, 'edited page FAQ', '2015-08-28 22:23:44', '0000-00-00 00:00:00'),
(148, 2, 'edited page home', '2015-08-28 22:26:31', '0000-00-00 00:00:00'),
(149, 2, 'edited page home', '2015-08-28 22:37:02', '0000-00-00 00:00:00'),
(150, 2, 'reset page home', '2015-08-28 22:37:39', '0000-00-00 00:00:00'),
(151, 2, 'edited page home', '2015-08-28 22:38:11', '0000-00-00 00:00:00'),
(152, 2, 'edited page home', '2015-08-28 22:43:24', '0000-00-00 00:00:00'),
(153, 2, 'edited page FAQ', '2015-08-28 22:46:25', '0000-00-00 00:00:00'),
(154, 2, 'edited page FAQ', '2015-08-29 01:36:33', '0000-00-00 00:00:00'),
(155, 2, 'edited page home', '2015-08-29 01:38:49', '0000-00-00 00:00:00'),
(156, 2, 'reset page home', '2015-08-29 01:39:13', '0000-00-00 00:00:00'),
(157, 2, 'edited page Contact Us', '2015-08-29 01:41:39', '0000-00-00 00:00:00'),
(158, 2, 'edited page Contact Us', '2015-08-29 01:52:19', '0000-00-00 00:00:00'),
(159, 2, 'edited page Contact Us', '2015-08-29 01:53:05', '0000-00-00 00:00:00'),
(160, 2, 'edited page Contact Us', '2015-08-29 01:54:04', '0000-00-00 00:00:00'),
(161, 2, 'edited page Contact Us', '2015-08-29 01:54:42', '0000-00-00 00:00:00'),
(162, 2, 'edited page Contact Us', '2015-08-29 01:56:24', '0000-00-00 00:00:00'),
(163, 2, 'edited page home', '2015-08-29 02:00:26', '0000-00-00 00:00:00'),
(164, 2, 'edited page home', '2015-08-29 02:03:28', '0000-00-00 00:00:00'),
(165, 2, 'edited page Contact Us', '2015-08-29 02:08:07', '0000-00-00 00:00:00'),
(166, 2, 'edited page Contact Us', '2015-08-29 02:08:46', '0000-00-00 00:00:00'),
(167, 2, 'edited page Terms and Conditions', '2015-08-29 02:11:58', '0000-00-00 00:00:00'),
(168, 2, 'edited page Contact Us', '2015-08-29 03:01:50', '0000-00-00 00:00:00'),
(169, 2, 'edited page Contact Us', '2015-08-29 03:05:39', '0000-00-00 00:00:00'),
(170, 2, 'edited page Contact Us', '2015-08-29 03:08:14', '0000-00-00 00:00:00'),
(171, 2, 'edited page Contact Us', '2015-08-29 03:09:31', '0000-00-00 00:00:00'),
(172, 2, 'edited page Contact Us', '2015-08-29 03:10:09', '0000-00-00 00:00:00'),
(173, 2, 'edited page Terms and Conditions', '2015-08-29 03:12:16', '0000-00-00 00:00:00'),
(174, 2, 'edited page Terms and Conditions', '2015-08-29 03:16:43', '0000-00-00 00:00:00'),
(175, 2, 'edited page Terms and Conditions', '2015-08-29 03:17:30', '0000-00-00 00:00:00'),
(176, 2, 'edited page Background', '2015-08-29 03:20:00', '0000-00-00 00:00:00'),
(177, 2, 'edited page Background', '2015-08-29 03:20:37', '0000-00-00 00:00:00'),
(178, 2, 'edited page Background', '2015-08-29 03:28:07', '0000-00-00 00:00:00'),
(179, 2, 'edited page Background', '2015-08-29 03:51:51', '0000-00-00 00:00:00'),
(180, 2, 'edited page Background', '2015-08-29 03:52:53', '0000-00-00 00:00:00'),
(181, 2, 'edited page FAQ', '2015-08-29 04:10:46', '0000-00-00 00:00:00'),
(182, 2, 'edited page Background', '2015-08-29 04:15:03', '0000-00-00 00:00:00'),
(183, 2, 'edited page What is ISupportJamaica?', '2015-08-29 04:38:52', '0000-00-00 00:00:00'),
(184, 2, 'edited page What is ISupportJamaica?', '2015-08-29 04:45:04', '0000-00-00 00:00:00'),
(185, 2, 'edited page What is ISupportJamaica?', '2015-08-29 04:49:56', '0000-00-00 00:00:00'),
(186, 2, 'edited page What is ISupportJamaica?', '2015-08-29 04:50:43', '0000-00-00 00:00:00'),
(187, 2, 'edited page What is ISupportJamaica?', '2015-08-29 04:51:50', '0000-00-00 00:00:00'),
(188, 2, 'edited page FAQ', '2015-08-29 04:54:45', '0000-00-00 00:00:00'),
(189, 2, 'edited page Contact Us', '2015-08-29 04:56:04', '0000-00-00 00:00:00'),
(190, 2, 'edited page Terms and Conditions', '2015-08-29 05:01:38', '0000-00-00 00:00:00'),
(191, 2, 'created page Partners / Angel Investors', '2015-08-29 05:20:12', '0000-00-00 00:00:00'),
(192, 2, 'edited page Partners / Angel Investors', '2015-08-29 05:20:35', '0000-00-00 00:00:00'),
(193, 2, 'edited page Partners / Angel Investors', '2015-08-29 05:21:13', '0000-00-00 00:00:00'),
(194, 2, 'edited page Partners / Angel Investors', '2015-08-29 05:23:41', '0000-00-00 00:00:00'),
(195, 2, 'edited page Partners / Angel Investors', '2015-08-29 05:27:09', '0000-00-00 00:00:00'),
(196, 2, 'edited page Partners / Angel Investors', '2015-08-29 11:53:11', '0000-00-00 00:00:00'),
(197, 2, 'edited page Partners / Angel Investors', '2015-08-29 11:55:32', '0000-00-00 00:00:00'),
(198, 2, 'edited page Contact Us', '2015-08-29 11:56:53', '0000-00-00 00:00:00'),
(199, 2, 'edited page Background', '2015-08-29 11:57:40', '0000-00-00 00:00:00'),
(200, 2, 'edited page home', '2015-08-29 12:00:50', '0000-00-00 00:00:00'),
(201, 2, 'edited page home', '2015-08-29 12:01:52', '0000-00-00 00:00:00'),
(202, 2, 'edited page Contact Us', '2015-08-29 16:55:00', '0000-00-00 00:00:00'),
(203, 2, 'edited page home', '2015-08-29 16:56:53', '0000-00-00 00:00:00'),
(204, 2, 'edited page home', '2015-08-29 16:57:21', '0000-00-00 00:00:00'),
(205, 2, 'edited page FAQ', '2015-08-29 19:14:37', '0000-00-00 00:00:00'),
(206, 2, 'edited page Partners / Angel Investors', '2015-08-31 00:53:53', '0000-00-00 00:00:00'),
(207, 2, 'edited page Partners / Angel Investors', '2015-08-31 00:56:30', '0000-00-00 00:00:00'),
(208, 2, 'edited page Partners / Angel Investors', '2015-08-31 00:59:00', '0000-00-00 00:00:00'),
(209, 2, 'edited page Partners / Angel Investors', '2015-08-31 01:00:01', '0000-00-00 00:00:00'),
(210, 2, 'edited page FAQ', '2015-08-31 01:20:25', '0000-00-00 00:00:00'),
(211, 2, 'edited page FAQ', '2015-08-31 01:25:25', '0000-00-00 00:00:00'),
(212, 2, 'edited page FAQ', '2015-08-31 01:28:51', '0000-00-00 00:00:00'),
(213, 2, 'edited page FAQ', '2015-08-31 01:29:27', '0000-00-00 00:00:00'),
(214, 2, 'edited page FAQ', '2015-08-31 01:30:34', '0000-00-00 00:00:00'),
(215, 2, 'edited page FAQ', '2015-08-31 01:32:35', '0000-00-00 00:00:00'),
(216, 2, 'edited page FAQ', '2015-08-31 01:34:28', '0000-00-00 00:00:00'),
(217, 2, 'edited page FAQ', '2015-08-31 01:40:14', '0000-00-00 00:00:00'),
(218, 2, 'edited page Terms and Conditions', '2015-08-31 01:43:15', '0000-00-00 00:00:00'),
(219, 2, 'edited page Terms and Conditions', '2015-08-31 01:48:08', '0000-00-00 00:00:00'),
(220, 2, 'edited page Terms and Conditions', '2015-08-31 01:49:34', '0000-00-00 00:00:00'),
(221, 2, 'edited page Terms and Conditions', '2015-08-31 01:57:41', '0000-00-00 00:00:00'),
(222, 2, 'edited page Partners / Angel Investors', '2015-08-31 01:59:28', '0000-00-00 00:00:00'),
(223, 2, 'edited page Partners / Angel Investors', '2015-08-31 02:00:16', '0000-00-00 00:00:00'),
(224, 2, 'edited page Terms and Conditions', '2015-08-31 02:00:55', '0000-00-00 00:00:00'),
(225, 2, 'edited page FAQ', '2015-08-31 02:23:51', '0000-00-00 00:00:00'),
(226, 2, 'edited page Contact Us', '2015-08-31 08:43:48', '0000-00-00 00:00:00'),
(227, 2, 'edited page Contact Us', '2015-08-31 08:44:20', '0000-00-00 00:00:00'),
(228, 2, 'edited page Partners / Angel Investors', '2015-08-31 08:45:15', '0000-00-00 00:00:00'),
(229, 2, 'edited page What is ISupportJamaica?', '2015-08-31 08:50:17', '0000-00-00 00:00:00'),
(230, 2, 'edited page What is ISupportJamaica?', '2015-08-31 08:51:12', '0000-00-00 00:00:00'),
(231, 2, 'edited page What is ISupportJamaica?', '2015-08-31 08:55:06', '0000-00-00 00:00:00'),
(232, 2, 'edited page What is ISupportJamaica?', '2015-08-31 08:56:05', '0000-00-00 00:00:00'),
(233, 2, 'edited page What is ISupportJamaica?', '2015-08-31 08:57:02', '0000-00-00 00:00:00'),
(234, 2, 'delete user account for Phillip Lindsay', '2015-08-31 08:59:19', '0000-00-00 00:00:00'),
(235, 2, 'delete user account for Lisandra Rickards', '2015-08-31 08:59:36', '0000-00-00 00:00:00'),
(236, 2, 'delete user account for yuyuyuy jkjkj', '2015-08-31 08:59:46', '0000-00-00 00:00:00'),
(237, 2, 'edited page Terms and Conditions', '2015-08-31 09:11:28', '0000-00-00 00:00:00'),
(238, 2, 'edited page Terms and Conditions', '2015-08-31 09:12:45', '0000-00-00 00:00:00'),
(239, 2, 'edited page Terms and Conditions', '2015-08-31 09:13:06', '0000-00-00 00:00:00'),
(240, 2, 'edited page Background', '2015-08-31 09:33:19', '0000-00-00 00:00:00'),
(241, 2, 'edited page Background', '2015-08-31 09:33:59', '0000-00-00 00:00:00'),
(242, 2, 'edited page Our Partners', '2015-08-31 09:52:49', '0000-00-00 00:00:00'),
(243, 2, 'created page Successful Campaigns', '2015-08-31 10:01:07', '0000-00-00 00:00:00'),
(244, 2, 'edited page Our Partners', '2015-08-31 21:55:07', '0000-00-00 00:00:00'),
(245, 2, 'delete page Our Partners', '2015-08-31 21:55:58', '0000-00-00 00:00:00'),
(246, 2, 'created page Our Partners', '2015-08-31 21:59:28', '0000-00-00 00:00:00'),
(247, 2, 'edited page Our Partners', '2015-08-31 22:00:01', '0000-00-00 00:00:00'),
(248, 2, 'edited page Our Partners', '2015-08-31 22:00:54', '0000-00-00 00:00:00'),
(249, 2, 'edited page Our Partners', '2015-08-31 22:01:15', '0000-00-00 00:00:00'),
(250, 2, 'edited page Our Partners', '2015-08-31 22:03:05', '0000-00-00 00:00:00'),
(251, 2, 'edited page Our Partners', '2015-08-31 22:07:05', '0000-00-00 00:00:00'),
(252, 2, 'edited page Our Partners', '2015-08-31 22:07:56', '0000-00-00 00:00:00'),
(253, 2, 'edited page Our Partners', '2015-09-02 03:46:17', '0000-00-00 00:00:00'),
(254, 2, 'edited page Contact Us', '2015-09-02 03:48:59', '0000-00-00 00:00:00'),
(255, 2, 'edited page Contact Us', '2015-09-02 03:49:34', '0000-00-00 00:00:00'),
(256, 2, 'edited page FAQ', '2015-09-02 03:50:12', '0000-00-00 00:00:00'),
(257, 2, 'edited page Our Partners', '2015-09-02 03:51:16', '0000-00-00 00:00:00'),
(258, 2, 'edited page Background', '2015-09-02 03:51:43', '0000-00-00 00:00:00'),
(259, 2, 'edited page Contact Us', '2015-09-02 22:44:09', '0000-00-00 00:00:00'),
(260, 2, 'edited page Contact Us', '2015-09-02 22:47:19', '0000-00-00 00:00:00'),
(261, 2, 'edited page Contact Us', '2015-09-02 22:54:44', '0000-00-00 00:00:00'),
(262, 2, 'edited page Contact Us', '2015-09-02 22:57:42', '0000-00-00 00:00:00'),
(263, 2, 'edited page Contact Us', '2015-09-02 22:59:05', '0000-00-00 00:00:00'),
(264, 2, 'edited page Contact Us', '2015-09-02 23:01:32', '0000-00-00 00:00:00'),
(265, 2, 'edited page Contact Us', '2015-09-02 23:02:00', '0000-00-00 00:00:00'),
(266, 2, 'edited page Contact Us', '2015-09-02 23:18:07', '0000-00-00 00:00:00'),
(267, 2, 'edited page Terms and Conditions', '2015-09-03 08:24:19', '0000-00-00 00:00:00'),
(268, 2, 'edit user account for Kimberly Manradge', '2015-09-03 10:50:37', '0000-00-00 00:00:00'),
(269, 2, 'edit user account for dwayne brown', '2015-09-03 10:51:00', '0000-00-00 00:00:00'),
(270, 2, 'create new user account for Phillip Lindsay', '2015-09-03 10:52:14', '0000-00-00 00:00:00'),
(271, 30, 'edited page Contact Support', '2015-09-04 00:11:47', '0000-00-00 00:00:00'),
(272, 30, 'edited page Contact Support', '2015-09-04 00:15:30', '0000-00-00 00:00:00'),
(273, 30, 'edited page Contact Us', '2015-09-04 00:16:03', '0000-00-00 00:00:00'),
(274, 30, 'created page How It Works', '2015-09-04 00:18:15', '0000-00-00 00:00:00'),
(275, 30, 'delete page How It Works', '2015-09-04 00:19:23', '0000-00-00 00:00:00'),
(276, 30, 'edited page Contact Us', '2015-09-08 12:03:38', '0000-00-00 00:00:00'),
(277, 30, 'edited page Terms and Conditions', '2015-09-08 12:08:06', '0000-00-00 00:00:00'),
(278, 30, 'edited page Our Partners', '2015-09-08 12:13:52', '0000-00-00 00:00:00'),
(279, 30, 'edited page Our Partners', '2015-09-09 00:23:34', '0000-00-00 00:00:00'),
(280, 30, 'edited page Our Partners', '2015-09-09 00:24:46', '0000-00-00 00:00:00'),
(281, 30, 'edited page Our Partners', '2015-09-09 00:25:39', '0000-00-00 00:00:00'),
(282, 30, 'edited page Our Partners', '2015-09-09 00:28:29', '0000-00-00 00:00:00'),
(283, 30, 'edited page Our Partners', '2015-09-09 00:29:07', '0000-00-00 00:00:00'),
(284, 30, 'edited page Terms and Conditions', '2015-09-09 00:38:05', '0000-00-00 00:00:00'),
(285, 30, 'edited page Background', '2015-09-09 02:31:14', '0000-00-00 00:00:00'),
(286, 30, 'edited page Successful Campaigns', '2015-09-09 02:35:00', '0000-00-00 00:00:00'),
(287, 30, 'edited page Successful Campaigns', '2015-09-09 02:37:45', '0000-00-00 00:00:00'),
(288, 30, 'edited page home', '2015-09-09 02:50:26', '0000-00-00 00:00:00'),
(289, 30, 'edited page Background', '2015-09-09 02:55:29', '0000-00-00 00:00:00'),
(290, 30, 'edited page What is ISupportJamaica?', '2015-09-09 02:58:10', '0000-00-00 00:00:00'),
(291, 30, 'edited page Background', '2015-09-12 09:02:25', '0000-00-00 00:00:00'),
(292, 30, 'edited page What is ISupportJamaica?', '2015-09-12 09:05:44', '0000-00-00 00:00:00'),
(293, 30, 'edited page What is ISupportJamaica?', '2015-09-13 18:10:28', '0000-00-00 00:00:00'),
(294, 30, 'edited page What is ISupportJamaica?', '2015-09-13 18:14:28', '0000-00-00 00:00:00'),
(295, 30, 'edited page What is ISupportJamaica?', '2015-09-13 18:17:45', '0000-00-00 00:00:00'),
(296, 30, 'edited page Background', '2015-09-13 18:19:06', '0000-00-00 00:00:00'),
(297, 30, 'edited page Background', '2015-09-13 18:19:36', '0000-00-00 00:00:00'),
(298, 30, 'edited page What is ISupportJamaica?', '2015-09-26 00:28:51', '0000-00-00 00:00:00'),
(299, 30, 'edited page Background', '2015-09-26 00:30:40', '0000-00-00 00:00:00'),
(300, 30, 'edited page FAQ', '2015-09-26 00:47:22', '0000-00-00 00:00:00'),
(301, 30, 'edited page Successful Campaigns', '2015-09-30 03:05:01', '0000-00-00 00:00:00'),
(302, 30, 'edited page What is ISupportJamaica?', '2015-09-30 04:56:36', '0000-00-00 00:00:00'),
(303, 30, 'edited page What is ISupportJamaica?', '2015-09-30 08:33:51', '0000-00-00 00:00:00'),
(304, 30, 'edited page What is ISupportJamaica?', '2015-09-30 08:47:02', '0000-00-00 00:00:00'),
(305, 30, 'edited page What is ISupportJamaica?', '2015-09-30 22:14:28', '0000-00-00 00:00:00'),
(306, 30, 'edited page What is ISupportJamaica?', '2015-09-30 23:45:33', '0000-00-00 00:00:00'),
(307, 30, 'edited page What is ISupportJamaica?', '2015-09-30 23:49:05', '0000-00-00 00:00:00'),
(308, 30, 'edited page FAQ', '2015-09-30 23:52:16', '0000-00-00 00:00:00'),
(309, 30, 'edited page What is ISupportJamaica?', '2015-10-01 11:31:29', '0000-00-00 00:00:00'),
(310, 30, 'edited page What is ISupportJamaica?', '2015-10-01 11:54:42', '0000-00-00 00:00:00'),
(311, 30, 'edited page What is ISupportJamaica?', '2015-10-01 11:55:26', '0000-00-00 00:00:00'),
(312, 30, 'edited page What is ISupportJamaica?', '2015-10-01 11:59:10', '0000-00-00 00:00:00'),
(313, 30, 'edited page What is ISupportJamaica?', '2015-10-01 12:01:17', '0000-00-00 00:00:00'),
(314, 30, 'edited page What is ISupportJamaica?', '2015-10-01 12:07:13', '0000-00-00 00:00:00'),
(315, 30, 'edited page Contact Us', '2015-10-02 05:00:16', '0000-00-00 00:00:00'),
(316, 30, 'edited page Contact Us', '2015-10-02 05:01:36', '0000-00-00 00:00:00'),
(317, 30, 'edited page What is ISupportJamaica?', '2015-10-02 05:20:39', '0000-00-00 00:00:00'),
(318, 30, 'edited page What is ISupportJamaica?', '2015-10-02 05:26:31', '0000-00-00 00:00:00'),
(319, 30, 'edited page What is ISupportJamaica?', '2015-10-02 05:26:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2015_02_17_162954_create_users_table', 1),
('2015_02_19_201818_create_projects_table', 1),
('2015_02_19_211215_create_pledges_table', 1),
('2015_02_19_211410_create_rewards_table', 1),
('2015_02_19_211421_create_cart_table', 1),
('2015_02_24_134534_add_user_initial', 1),
('2015_02_24_140912_add_email_validate', 1),
('2015_03_03_033505_create_category_table', 2),
('2015_03_03_193109_add_security_questions', 3),
('2015_03_03_195549_add_security_question', 4),
('2015_03_03_201913_dump_security_questions', 4),
('2015_03_03_225631_category_follow', 5),
('2015_03_03_232437_add_few_categories', 5),
('2015_03_04_163454_create_uploads_table', 5),
('2015_03_04_164438_create_uploads_modify_table', 6),
('2015_03_04_165849_create_uploads_projectid_table', 7),
('2015_03_04_175023_create_project_startdate_table', 8),
('2015_03_04_192619_add_reward_dates', 9),
('2015_03_04_184557_update_pledges_table', 10),
('2015_03_04_212443_create_order_table', 11),
('2015_03_04_212637_create_order_items_table', 11),
('2015_03_04_213844_add_status_field_to_order_table', 11),
('2015_03_04_214041_add_anonymous_field_to_order_table', 11),
('2015_03_04_221755_add_project_id_order_item_table', 11),
('2015_03_06_195908_create_project_view_column', 11),
('2015_03_10_220147_add_user_accee_level', 12),
('2015_03_14_195827_create_page_section_table', 13),
('2015_03_14_200247_add_page_section_table', 13),
('2015_03_14_200603_create_pages_table', 13),
('2015_03_14_211127_add_discover_pages_table', 14),
('2015_03_15_002716_add_pages_columns', 15),
('2015_03_16_015824_add_page_section_other', 16),
('2015_03_16_020053_add_page_home', 17),
('2015_03_16_132402_create_projects_pick', 18),
('2015_03_16_142145_add_category_homepage_column', 19),
('2015_09_08_140936_create_banner_images_table', 20),
('2015_09_09_151617_add_about_project_column', 21),
('2015_09_12_091122_create_heading_table', 22),
('2015_09_12_091334_add_headings_table', 22);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `created_at`, `updated_at`, `order_id`, `amount`, `project_id`) VALUES
(1, '2015-03-25 09:24:28', '2015-03-25 09:24:28', 1, 500.00, 1),
(2, '2015-04-03 05:54:57', '2015-04-03 05:54:57', 2, 50.00, 1),
(3, '2015-04-18 00:19:34', '2015-04-18 00:19:34', 3, 900.00, 2),
(4, '2015-05-09 04:11:21', '2015-05-09 04:11:21', 4, 50.00, 4),
(5, '2015-07-02 04:32:52', '2015-07-02 04:32:52', 5, 100.00, 9),
(6, '2015-07-02 04:35:10', '2015-07-02 04:35:10', 7, 1.00, 7),
(7, '2015-07-02 04:35:10', '2015-07-02 04:35:10', 7, 50.00, 2),
(8, '2015-08-05 08:24:28', '2015-08-05 08:24:28', 9, 10.00, 9);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `paypal_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `anonymous` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `user_id`, `paypal_token`, `paypal_user_id`, `status`, `anonymous`) VALUES
(1, '2015-03-25 09:24:28', '2015-03-25 09:24:28', 12, '', '', 0, 0),
(2, '2015-04-03 05:54:57', '2015-04-03 05:54:57', 10, '', '', 0, 1),
(3, '2015-04-18 00:19:34', '2015-04-18 00:19:34', 15, '', '', 0, 0),
(4, '2015-05-09 04:11:21', '2015-05-09 04:11:21', 15, '', '', 0, 1),
(5, '2015-07-02 04:32:52', '2015-07-02 04:32:52', 17, '', '', 0, 1),
(6, '2015-07-02 04:32:57', '2015-07-02 04:32:57', 17, '', '', 0, 1),
(7, '2015-07-02 04:35:10', '2015-07-02 04:35:10', 17, '', '', 0, 0),
(8, '2015-07-02 04:35:13', '2015-07-02 04:35:13', 17, '', '', 0, 0),
(9, '2015-08-05 08:24:28', '2015-08-05 08:24:28', 2, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_sections`
--

CREATE TABLE IF NOT EXISTS `page_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `page_sections`
--

INSERT INTO `page_sections` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'About us'),
(2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Help'),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Discover'),
(4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Hello'),
(5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_section_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_page_section_id_foreign` (`page_section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Technology', '', '', '', '', 3, 'static', '', '', 0),
(3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Health', '', '', '', '', 3, 'static', '', '', 0),
(4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Music', '', '', '', '', 3, 'static', '', '', 0),
(5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Art', '', '', '', '', 3, 'static', '', '', 0),
(6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Science', '', '', '', '', 3, 'static', '', '', 0),
(7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Poetry', '', '', '', '', 3, 'static', '', '', 0),
(8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Theatre', '', '', '', '', 3, 'static', '', '', 0),
(9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Fashion', '', '', '', '', 3, 'static', '', '', 0),
(10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Games', '', '', '', '', 3, 'static', '', '', 0),
(11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Comics', '', '', '', '', 3, 'static', '', '', 0),
(12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Crafts', '', '', '', '', 3, 'static', '', '', 0),
(13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Dance', '', '', '', '', 3, 'static', '', '', 0),
(14, '2015-03-15 11:29:05', '2015-09-30 23:52:16', 'FAQ', 'Frequent asked questions', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">What is Crowdfunding<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">It''s\r\nthe practice of funding a project or venture by raising small\r\namounts of money from a large group of people, typically via the internet.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><br></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">What type of crowdfunding do you offer<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<ul style="margin-top:0in" type="disc">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Donations<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Partnership (Coming\r\n     Soon)<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lending (Coming\r\n     Soon)</span></li>\r\n</ul><div><font face="Roboto, serif"><span style="line-height: normal;"><br></span></font></div>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Can overseas project seek funding via ISupport<i>? </i></span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> <o:p></o:p></span></p>\r\n\r\n<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Only\r\npermits locally based projects and projects that are clearly faced with a goal\r\nand an actual end point; we are open to all project creators globally that\r\nsupports the growth of our country. <o:p></o:p></span></p><div><span style="line-height: 1.42857143;"><br></span></div>', 'uploads/2015-09-25/1400x600_b092c43b2a99344181c9cfe0c6a88cad.png', '', 2, 'page', 'faq', '', 0),
(16, '0000-00-00 00:00:00', '2015-09-09 01:50:26', 'home', '', '', 'uploads/2015-09-08/e72f0910b02728149346be868d1ec587.png', 'Flag', 5, 'static', 'home', '', 0),
(20, '2015-08-04 08:28:17', '2015-08-04 08:28:17', 'Netballers', '', '<h1 class="yt watch-title-container" style="text-align: left; margin-top: 0px; margin-bottom: 13px; padding: 0px; border: 0px; font-size: 24px; display: table-cell; vertical-align: top; width: 610px; color: rgb(34, 34, 34); line-height: normal; word-wrap: break-word; font-family: Roboto, arial, sans-serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="background-color: transparent;">Goal Rush Down Under - One Goal Campaign</span><br></h1>', '', '', 4, 'blog', 'netballers', 'https://www.youtube.com/watch?v=O3FsKjtvLUU', 2),
(21, '2015-08-05 07:11:45', '2015-08-05 07:11:45', 'Facebook', '', '', '', '', 4, 'link', 'https://www.facebook.com/ISupportJamaica', '', 0),
(22, '2015-08-05 07:12:40', '2015-08-05 07:12:40', 'Instagram', '', '', '', '', 4, 'link', 'https://instagram.com/isupportjamaica/', '', 0),
(23, '2015-08-05 07:15:48', '2015-08-05 07:15:48', 'YouTube', '', '', '', '', 4, 'link', 'https://www.youtube.com/user/isupportjamaica', '', 0),
(24, '2015-08-05 07:16:50', '2015-08-05 07:16:50', 'Twitter', '', '', '', '', 4, 'link', 'https://twitter.com/ISupportJamaica', '', 0),
(25, '2015-08-05 07:18:17', '2015-08-05 07:18:17', 'Google+ ', '', '', '', '', 4, 'link', 'https://plus.google.com/+iSupportJamaica/posts', '', 0),
(30, '2015-08-25 21:04:03', '2015-10-02 05:20:39', 'What is ISupportJamaica?', 'About Us', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ISUPPORT\r\nJAMAICA is a project which seeks to provide an easy and creative way for\r\nJamaicans in the Diaspora and elsewhere and friends of Jamaica to provide\r\nfinancial support to projects, both microenterprise and nonprofit, that the JN\r\nSmall Business Loans Ltd. consider worthy and reliable. Jamaicans are known for\r\ntheir indomitable spirit in business, sports, music and any other area they\r\nchoose to become involved in. As Benjamin Disraeli said, "The secret of\r\nsuccess in life is for a man to be ready for his opportunity when it\r\ncomes." Unfortunately, this opportunity tends to hinge on the catalyst of\r\nmoney, and thousands of small entrepreneurs are unable to realize their true\r\nbusiness potential because of their inability to access financing from traditional\r\nsources. The inherent perceived risk and the lack of adequate collateral are\r\ncited as the primary causes for this problem.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">In\r\nan attempt to provide a tourniquet for this inherent business practice problem,\r\nI SUPPORT''s mission is to deliver innovative and accessible credit in a timely\r\nand profitable manner to micro and small entrepreneurs in Jamaica who have\r\nlimited access to loans from traditional banking sources. The objectives of\r\nthis project are to assist micro entrepreneurs to increase their income and\r\nimprove the quality of their lives, create new jobs and to support the\r\ndevelopment of the Jamaican micro enterprise sector.<br>\r\n<br>\r\nI SUPPORT also provides a platform for anyone who wishes to donate money to\r\nnon-profits in Jamaica.</span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-size: 24pt; font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lending to an entrepreneur<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nloan process will be simple and rewarding. Persons who give and those who\r\nreceive will be able to express their appreciation by posting testimonials on\r\nthe website. This essentially will reflect a good rapport between the demand\r\nand supply connection and consequently can lead to additional interest from\r\nother persons. Additionally, the entrepreneur will receive a rebate on the last\r\nloan payment as an incentive to list on the site.</span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; line-height: normal; font-stretch: normal;">\r\n\r\n<p class="MsoNormal" style="color: rgb(62, 77, 92); font-family: ''Open Sans'', sans-serif; margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Giving to local charities and community groups</span></p></h1><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Social\r\nentrepreneurship facilitates the framework for effecting social change by using\r\ntraditional business practices to gain something more than profit: namely, a\r\nbetter world. I SUPPORT makes it easy for individual philanthropists who want\r\nto contribute to worthwhile causes in communities across Jamaica. People around\r\nthe world will be able to donate to worthy and responsible initiatives located\r\nin what could be their home parish, or home town, and will feel a sense of pride\r\nin knowing that they have helped Jamaica take a step in the right direction. In\r\ncreating this causal link between idea and execution and by extension dreams\r\nand funding, I SUPPORT JAMAICA fosters a viable and thriving entrepreneurial\r\nspirit that is at the core of the Jamaican social landscape.<br>\r\n<br>\r\nI SUPPORT JAMAICA is an initiative of JN Small Business Loans Ltd.<o:p></o:p></span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; font-family: ''Open Sans'', sans-serif; line-height: normal; color: rgb(62, 77, 92); font-stretch: normal;"><p class="MsoNormal" style="margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Jamaica National Small Business Loans (JNSBL)<o:p></o:p></span></p></h1><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;"><img src="https://www.isupportjamaica.com/images/jnsbl_logo.png" alt="JN Small Business Loans" style="border-radius: 0px !important;" align="left" width="100"></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">JNSBL provides innovative and accessible credit to small business people. Through\r\nour involvement with them we have increased their productivity, improved their\r\neconomic and social status and contributed to their sustained growth and\r\ndevelopment. We are the micro finance institution of choice in Jamaica. Our products\r\nand services are delivered island wide and are accessible in most remote areas.\r\nWe use research to assist in determining the changing needs of our clients and\r\nto inform us in the development and launching of new products. We are\r\nrecognized as the organization that provides excellent business management\r\nsupport in the communities in which we operate. We are well positioned to\r\napproach the future with confidence and expect to be vigorous participants in\r\nthe development of Jamaica.<o:p></o:p></span></p><h1 style="margin-top: 0.67em; margin-bottom: 0.67em; font-size: 2em; font-family: ''Open Sans'', sans-serif; line-height: normal; color: rgb(62, 77, 92); font-stretch: normal;"><p class="MsoNormal" style="margin-bottom: 7.5pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-size: 24pt; font-family: Roboto, serif; color: rgb(61, 61, 61); background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">JN FOUNDATION<o:p></o:p></span></p></h1><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;"><img src="https://www.isupportjamaica.com/images/jnf.png" alt="JN Foundation" style="border-radius: 0px !important;" align="left" width="100"></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nJamaica National Building Society (JN) Foundation works with internal and\r\nexternal partners to identify develop and provide technical and financial\r\nsupport to projects and programmes that focus on issues relating to rural development,\r\nhealth, housing, education, youth, community, crime and safety. Established in\r\n1990 as the charitable arm of the Jamaica National Building Society (JNBS), JN\r\nFoundation''s mandate is to manage and execute the philanthropic efforts of the\r\nSociety, and contribute to the developmental needs of Jamaica. Built on the\r\nconcept of mutuality, JNBS gives back to the Jamaican people and communities\r\nacross the island, by providing financial and technical support to projects and\r\nprogrammes both at the community and at the national level.<o:p></o:p></span></p>', 'uploads/2015-10-01/aec9f60ff71e479c6ba8d64f990b5eca.jpg', '', 1, 'page', 'what-is-isupportjamaica', '', 0),
(31, '2015-08-25 21:09:28', '2015-10-02 05:01:36', 'Contact Us', 'Contact Us', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nget in touch with the <b>ISUPPORTJAMAICA</b> team, give us a call,\r\ndrop us a line via email or send us a letter through the regular mail.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Tel:</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> 1(876) 926-1344 or 733-7100 <b>Ext:</b> 2737\r\n& 2703 - 4<br>\r\n<b>Customer Service Agents: </b>Jamaica 1-888-991- 4065 , USA & Canada\r\n1-800-462-9003 & UK 0-800-328-0387    <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b>Email: <a href="https://www.isupportjamaica.com/ISupportJamaica@jnbs.com">ISupportJamaica@jnbs.com</a></b><br>\r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n                     \r\n            <b>   </b><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Mailing Address</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ISUPPORTJAMAICA</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><br>\r\n2 - 4 Constant Spring Road<br>\r\nKingston 10<br>\r\nJamaica<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">If\r\nyou have any questions about using PayPal and the payment process, you can call\r\n888-445-5032 (Toll Free US) or 402-952-8811 (International), Monday through\r\nFriday, 8 a.m. to 5 p.m. CST for help in completing your transaction.<o:p></o:p></span></p><p style="color: rgb(62, 77, 92); font-family: ''Open Sans'', Arial, sans-serif; font-size: 13px; line-height: normal; border-radius: 0px !important;">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Don''t\r\nforget, you can keep up-to-date with what we''re up to, by following us on\r\nFacebook and Twitter.<o:p></o:p></span></p>', 'uploads/2015-10-01/a228fe2378b84499ccb31af9e938206c.jpg', '', 2, 'page', 'contact-us', '', 0),
(33, '2015-08-25 21:23:27', '2015-08-25 21:23:27', 'Start A Campaign', '', '', '', '', 1, 'link', 'http://isupport.spidercentro.com/create/general', '', 0),
(34, '2015-08-25 21:44:18', '2015-09-26 00:30:40', 'Background', 'Background', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif;">The website, <a href="http://isupport.spidercentro.com/page/edit/www.isupportjamaica.com"><b>www.isupportjamaica.com</b></a>,\r\nwas introduced in June 2013 at the Diaspora Conference held in St. James.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif;">ISupportJamaica.com</span></b><span style="font-family: Roboto, serif;"> is the Caribbean’s\r\nfirst English Speaking crowdfunding website and it''s the leading crowdfunding platform\r\nin Jamaica.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif;">Social\r\nMission</span></b><span style="font-family: Roboto, serif;"> Community\r\nEmpowerment<o:p></o:p></span></p><p class="MsoNormal">\r\n\r\n\r\n\r\n\r\n\r\n</p><ul type="disc">\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Embrace\r\n     Technology & Innovation<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Encourage Entrepreneurial Habits<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="line-height: 20pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family:"Roboto","serif";mso-fareast-font-family:\r\n     "Times New Roman";mso-bidi-font-family:"Times New Roman"">Build Patriotism<o:p></o:p></span></li>\r\n</ul><p></p>', 'uploads/2015-09-25/0eca4da316b15c2b920b298f1efb3616.png', '', 1, 'page', 'background', '', 0);
INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(35, '2015-08-25 21:53:30', '2015-09-08 23:38:05', 'Terms and Conditions', 'Terms and Conditions', '<p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Grant Programme</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nIsupportjamaica.com website also promotes community-based projects and seeks\r\ndonations from the Jamaican Diaspora and globally to support and develop such\r\nphilanthropic projects. If you continue to browse and/or use this Website you\r\nare agreeing to comply with and be bound by the these terms and conditions and\r\nany other terms and conditions linked to this Agreement (including the Privacy\r\nPolicy), which govern isupportjamaica.com''s relationship with you.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Isupportjamaica.com\r\nis a registered Website (“the Website) owned and operated by JN Small Business\r\nLoans Limited (“JNSBL”) a micro finance Company registered and operating in\r\nJamaica with registered office located 32 ½ Duke Street, Kingston. This link\r\n(“the Grant Link”) which deals with grant funding is administered by JNBS\r\nFoundation. JNBS Foundation is a charitable organisation established in 1990 to\r\nmanage and execute the philanthropic programmes of the Jamaica National\r\nBuilding Society (“JNBS”) and its group of companies. Since its establishment\r\nthe Foundation has managed in excess of J$350,000,000.00 of grant funds\r\nprovided by the JNBS Group, international aid agencies, NGOs, local private\r\nsector organizations and other grantors.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nterm "Foundation” or "us" or "we" or “our” refers to\r\nJNBS Foundation. The term "you” or “your” refers to the user or viewer of\r\nthe Website. Words denoting one gender include all other genders.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">These\r\nterms and conditions constitute an Agreement between you and the Foundation\r\nrelating to, and govern, all businesses transacted by you using the Grant Link\r\nof this Website. By using the Grant Link of this Website, you agree to be bound\r\nby this Agreement, whether or not you participate in isupprtjamaica.com\r\nprogramme (“the Programme") by providing a grant or otherwise use the\r\nWebsite (in each such capacity, a "User"). If you wish to become a\r\nGrantor or otherwise become a User of the Website, you must comply with all the\r\nterms and conditions herein and all terms and conditions linked to this\r\nAgreement.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">This\r\nAgreement may be modified by the Foundation in its sole discretion from time to\r\ntime and such modifications will be effective once posted on isupportjamaica.com\r\nWebsite. You should review the Website and this Agreement regularly and your\r\ncontinued participation in the Grant Programme and use of the Website after any\r\nsuch modifications become effective will indicate your acceptance of, agreement\r\nwith and willingness to be bound by such modifications.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">How the Grant Programme Works</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will identify community based projects deserving of funding. The\r\nFoundation will assist the Project Sponsors to post a full description and\r\nother pertinent details of the Project on the Isupportjamaica.com Web Site.\r\nThrough the Web site Project Sponsors will solicit grants to fund their\r\nProjects. The Foundation will manage the Web Site Grant Link which matches\r\ndonors with Project Sponsors.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">A\r\npotential donor will have the opportunity to review the various community\r\nprojects posted on the Web Site. For each project the minimum target funding\r\n(“Target Amount”) will be specified. The Target Amount is the minimum amount\r\nsought to be raised to successfully undertake the particular Project.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">A\r\ndonor who identifies a Project to support will make a donation to the\r\nFoundation. The Foundation will hold the funds donated and if the Target Amount\r\nis achieved within the time stated the Foundation will then disburse the funds\r\nto the Project Sponsors. If the Target Amount is not reached within the\r\ndonation period then donors to that Project will be informed and will have the\r\noption to assign their donation to another Project.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Where\r\nthe Target Amount is not reached within the donation period and the donors have\r\nbeen notified and have not taken the option to assign their donation to another\r\nProject within six months of being notified the Foundation reserves the right\r\nto assign the donation to another Project or to allocate the donation to assist\r\nin offsetting the administrative costs associated with the Isupportjamaica.com\r\ninitiative.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Project Updates</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Donors\r\nwill have the opportunity to track the progress of their sponsored project via\r\nregular e-dates. Subject to the Privacy Policy donors shall be at liberty to\r\ngrant a testimonial which may be posted on the Web Site or in the Foundation\r\nFacebook page or other e-media outlet.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acknowledgement and Agreement of Project Sponsors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">By\r\nsubmitting a Project for sponsorship on the Isupportjamaica.com Web site each\r\nProject Sponsor will be deemed to undertake to the Foundation and to any Donor\r\nthat:<o:p></o:p></span></p><ol style="margin-top:0in" start="1" type="a">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">(a) the information\r\n     provided to the Foundation about the Project is accurate and complete in\r\n     all material respects;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">donated funds\r\n     disbursed to the Project Sponsor will be applied solely towards the\r\n     Project and for no other purpose whatsoever;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will keep and\r\n     maintain accurate and complete accounting records of all project\r\n     expenditures;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will submit such\r\n     accounting and other records to spot audits, without prior notice, by the\r\n     Foundation or by any accountant or other agent appointed by the\r\n     Foundation;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will provide\r\n     regular update of the progress of the Project and after completion regular\r\n     reports of the utilization and contribution being made by the Project in\r\n     the community;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">he will provide\r\n     prompt notice to the Foundation of the occurrence of any event which is\r\n     likely to adversely affect the smooth implementation and execution of the\r\n     Project or utilization in the community;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">disbursed funds will\r\n     not be handled in any corrupt manner and that where third party goods or\r\n     services have to be acquired such acquisition will be made at the best\r\n     price reasonably obtainable without favour and free of partisan influences;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">any breach of these\r\n     terms may result in the Project Sponsor being black-listed and each\r\n     Project Sponsor waives any claim against the Foundation for defamation or\r\n     otherwise arising form being black-listed and barred from promoting any\r\n     further Project on the web site unless the Project Sponsor can positively\r\n     prove that the statement made by the Foundation was known to be untrue but\r\n     was nevertheless willfully made to deliberately harm the Project Sponsor.<o:p></o:p></span></li>\r\n</ol><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nfact that a Project Sponsor has been black-listed does not necessarily mean\r\nthat he is guilty of some act of dishonesty. It only signifies that the Project\r\nSponsor has failed to comply with the terms of the sponsorship on the\r\nIsupportjamaica.com Web Site. The Foundation reserves the right to continue the\r\npromotion of a worthwhile Project under different leadership even though the\r\noriginal Project Sponsor has been black-listed for breach of the terms.\r\n      <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acknowledgement and Agreement of Donors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will endeavour to vet Projects and use reasonable efforts, within\r\nthe limits of its resources, to post Projects for grant funds which have the\r\nrequisite management and institutional support to ensure that the grant funds\r\nare used as intended and that the Project will ensure for the benefit of the\r\nintended beneficiaries. Notwithstanding the Foundation best efforts it must be recognized\r\nthat not all projects will succeed. Each Donor acknowledges and agrees as\r\nfollows:<o:p></o:p></span></p><ol style="margin-top:0in" start="1" type="a">\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that the funds\r\n     donated by him is a grant to the Foundation and not a loan will not earn\r\n     interest nor will it be repaid;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that the Foundation\r\n     gives no guarantee and makes no representation or warranty as to the\r\n     implementation or execution of any Project;<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that whilst the\r\n     Foundation will endeavour to guard against misuse of donated funds it\r\n     shall not be liable to any donor if a Project Sponsor actually\r\n     mis-allocates, misuses or appropriates donated funds to non-approved\r\n     purposes; and<o:p></o:p></span></li>\r\n <li class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">that whilst the\r\n     Foundation will undertake spot checks on a Project it does not have the\r\n     resources to undertake detailed Project audits.<o:p></o:p></span></li>\r\n</ol><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Communication</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Subject\r\nto applicable laws, communications may be posted on this Website, or another\r\nwebsite disclosed to donors, or sent to the e-mail address provided by them.\r\nAll communications will be deemed to have been received by a donor immediately\r\nafter the Foundation sends it to the donor by e-mail or posts the communication\r\non the Website, whether or not the donor has received the e-mail or retrieved\r\nor read the communication from the Website. A communication by e-mail is\r\nconsidered to be sent at the time that it is directed by the sender''s e-mail\r\nserver to the e-mail address provided by the intended recipient. A\r\ncommunication posted to a website is considered to be sent at the time it is\r\npublicly available. Each Donor hereby agrees that these are reasonable\r\nprocedures for sending and receiving communications and accepts such procedures\r\nof communication.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Eligible Donors</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nbe a donor you must be either an individual at least 18 years of age or a\r\nlegally constituted body such as a corporation and be able to form legally\r\nbinding contracts under applicable law. You must have a valid postal /mailing\r\naddress and an active e-mail address. You must have either (i) a valid credit\r\ncard or debit card or (ii) a deposit account with a reputable financial\r\ninstitution. Your mailing address must match the billing address for your\r\ncredit card, debit card and/or deposit account (as applicable). Other\r\nrestrictions may apply.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Lenders Commitment</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">To\r\nparticipate in the granting of funds to the Foundation on the\r\nisupportjamaica.com Web Site you must provide complete and correct information\r\nabout yourself sufficient to verify your identity. Such information must\r\ninclude, at a minimum, your name, address, date of birth, and social security\r\nnumber or taxpayer identification number. The Foundation reserves the right to\r\naccept or reject your request to participate in the Grant Programme. You\r\nrepresent and warrant that all information you provide to isupportjamaica.com\r\nor the Foundation including but not limited to information about yourself, your\r\nsource of funds, will be complete and accurate in all respects. The Foundation\r\nwill be entitled to rely on any such information, and you agree to update your\r\npersonal information if it changes. The Foundation also reserves the right to\r\ntake steps to verify the information you provide. Your ability to use or\r\notherwise participate in the Grant Programme may be terminated and any pending\r\ntransactions may be cancelled if you provide false information. In addition,\r\nyou may be subject to civil and criminal penalties.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">In\r\nthe event that you provide incorrect banking information you may be required to\r\npay a fee to cover bank charges incurred as a result of the error and you may\r\nalso be required to submit to the Foundation, documentation to verify your bank\r\naccount number.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">If\r\nany false, inaccurate or incomplete information is provided by you to the\r\nFoundation or if the Foundation has reasonable grounds to suspect that the\r\ninformation is untrue or incomplete the Foundation has the right to suspend or\r\ncancel your access to the Programme.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Neither\r\nthe Foundation nor the owners or operators of the Isupportjamaica.com website\r\nwill be liable for any loss, damage or injury arising from your failure to\r\nprovide accurate information.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Accessing Your Dashboard</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nwill create a unique user ID and password to access your Dashboard. You should\r\ncontact ISupportJamaica immediately if you suspect your password or user ID has\r\nbeen compromised.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Security Procedures</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Access\r\nto your Dashboard on isupportjamica.com and your ability to initiate grants\r\nwill be verified based on security procedures that are offered as part of the\r\nservice. Each time you attempt to access your Dashboard, you will be asked to\r\nprovide your unique user ID and password. Providing this information will give\r\nyou full Dashboard access with the ability to initiate a grant and review the\r\nProjects you have supported. You agree to follow any additional on-screen\r\ninstructions with regard to Dashboard security. You also agree to update and\r\nupgrade your Internet browser software on a regular basis as such updates or\r\nupgrades may become available from time to time. You acknowledge and agree that\r\nthis security procedure is commercially reasonable and that in selecting a user\r\nID and password as your security procedure you have elected not to use any\r\nother commercially reasonable procedures that may be available.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Confidentiality of Security Procedures</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree not to divulge your password or user ID to any other person, and you\r\nagree not to use another person''s password or user ID. If, notwithstanding the\r\nforegoing prohibition, you give your password to another person or persons, you\r\nwill be deemed to have authorized them to use your password and Account for any\r\nand all purposes, without limitation. You agree that the Foundation will not\r\nhave any responsibility or liability to you or any other person for any loss or\r\ndamage which you or anyone else may suffer if you disclose your password to any\r\nother person, including any loss or damage arising out of the disclosure of\r\nthis information by the recipient to another person.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Forbidden Transactions</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree that you will not use isupportjamaica.com for the improper, unlawful, or\r\nfraudulent transfers of funds ("Forbidden Transactions"), and any\r\nfunds donated by you to the Foundation will not be from any Forbidden\r\nTransactions. Forbidden Transactions include but are not limited to attempts to\r\n(a) to use  funds gained from using stolen, unauthorized, or otherwise\r\ncompromised credit card, check card, debit card, bank account, or Account\r\ninformation, (b) using funds from an account with insufficient funds (c) to\r\nseek reimbursement of funds that have already been disbursed, (d) to launder\r\nfunds, (e) to use the service in connection with gambling or pornography, (f)\r\nto fund terrorist organizations or individuals identified from time to time by\r\nappropriate governing authority, (g) to make corrupt payments or gifts or (g)\r\nto violate applicable local laws, laws of Jamaica, credit card association\r\nrules and regulations, or the laws of any other relevant jurisdiction.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Ownership and Use of Data</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree that the Foundation and the owners of the Isupportjamaica.com website\r\nwill own and possess the data that you provide in connection with your grant.\r\nThe Foundation and the owner of the Web site agree that they will not disclose\r\nor sell your personal information to third parties.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nacknowledge, consent and agree that the Foundation and the Owner of the Web\r\nSite may, at its sole discretion and to the extent permitted by law, access,\r\nread, preserve and disclose your account information, usage history and\r\nsubmitted content in order to: (a) comply with any applicable law, regulation,\r\nlegal process, or governmental request; (b) respond to claims that any content\r\nviolates the rights of third parties, including intellectual property rights;\r\n(c) enforce this Agreement and investigate potential violations thereof; (d)\r\ndetect, prevent, or otherwise counter fraud, security, or technical issues; (e)\r\nrespond to your requests for customer service; or (f) protect the rights,\r\nproperty or safety of the Web Site, the Foundation, its users and the general\r\npublic.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">By\r\ndefault, you may receive periodic newsletter emails from the Foundation or the\r\nowners of the Web site. The frequency of these newsletters may vary. You can\r\nchoose not to receive these newsletters through a preference on the Website.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation will not disclose your personally identifiable activity to any third\r\nparty without your consent. The Foundation reserves the right to record and\r\ndisplay projects details, testimonials and the like on the Website and display\r\nthe general regions where project beneficiaries are located.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Indemnity</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree to indemnify and hold the Foundation and its parent company, Jamaica\r\nNational Building Society, respective affiliates, subsidiaries, officers,\r\nagents, co-branders and other partners, directors, and employees, harmless from\r\nany claim or demand, including reasonable attorneys'' fees, made by any third\r\nparty due to or arising out of your use of isupportjamaica.com Website, your\r\nviolation of this Agreement, or your violation of any rights of another.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Resale of service</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nagree not to reproduce, duplicate, copy, sell, resell or exploit for any\r\ncommercial purposes, any portion of the Website, use of the Website, or access\r\nto the Website provided hereunder.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The Foundations’ intellectual property</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nacknowledge that the isupportjamica.com service provided hereunder, including\r\nbut not limited to the content of this Website, text, graphics, links, buttons,\r\nlogos, and images, as well as all other copyrights, trademarks, service marks,\r\nlogos, and product and service names are owned exclusively by JN Small Business\r\nLoans Limited. ("Isupportjamaica.com Intellectual Property"). You\r\nagree not to display, use, copy, or modify Isupportjamaica.com Intellectual\r\nProperty in any manner. You are authorized solely to view and retain a copy of\r\nthe pages of this Website for your own personal, non-commercial use. You\r\nfurther agree not to: (a) use any robot, spider, scraper or other automated\r\ndevice to access the Website or (b) remove or alter any author, trademark or\r\nother proprietary notice or legend displayed on this Website (or printed pages\r\nthereof).<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Any use, copying or reproduction of the Foundation’s trademarks or logos\r\ncontained in this site, without prior written permission of the Foundation is\r\nstrictly prohibited.</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation is not responsible, and shall have no liability, for any incorrect\r\nor inaccurate Content posted on the Website or any liability, cost or expense\r\nyou may incur in connection with the Programme, whether caused by any User, the\r\nFoundation’s Officers, Project Sponsors or other person or by any of the\r\nequipment or programming associated with or utilized in the Programme. The\r\nFoundation is not responsible for the conduct, whether online or offline, of\r\nany User of the Website or any other person.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nFoundation cannot always foresee or anticipate technical or other difficulties.\r\nThese difficulties may result as a loss of data or personalization settings, or\r\nother service interruptions. For this reason, you agree that access to the\r\nWebsite is provided as is without any warranties of any kind. The Foundation\r\ndoes not assume responsibility for timeliness, errors or inaccuracies, deletion\r\nor failure to store any User data, communications or personalized settings. You\r\nagree that you assume the risk for use of the on-line application service on\r\nthis Website. The Foundation shall not be liable for any special, direct,\r\nincidental or consequential damages.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The Website, including any Content or information contained within it or\r\nany service or advice provided in connection with the Programme, is provided\r\n"as is" with no representations or warranties of any kind, express or\r\nimplied, including, but not limited to, implied warranties of merchantability,\r\nfitness for a particular purpose and non-infringement. You assume total\r\nresponsibility and risk for your use of the Website and related services.</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Business Days</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nphrase "business days" as used herein means Monday through Friday,\r\nexcluding public holidays. However, access to the website will generally still\r\nbe available online on non-business days.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Assignments and Transfers</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nmay not assign or otherwise transfer your rights under your Account or the\r\nAgreement to any other person or entity.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Tax Deductibility</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">You\r\nunderstand that you are solely responsible for determining the proper tax\r\ntreatment for any grant you make through the Website and the Programme. The\r\nFoundation will, on request, provide receipt of other proof of grant to a\r\nDonor. The Foundation has not and will not provide any tax or legal advice to\r\nyou in connection with any Grant or Loan you might make<b>.</b> This\r\nAgreement does not attempt to define the tax implications of participants in\r\nthe Programme. If you participate in the Programme, you should consult with\r\nyour own accountant, tax or legal advisor.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><a name="0.1_01000003"></a><a name="0.1_01000004"></a><b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Acceptable\r\nUse</span></b><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">The\r\nWebsite is for use in connection with the isupportjamaica.com Grant Programme\r\nand may not be used by you in connection with any other commercial endeavors\r\nexcept as previously approved in writing by the Foundation. Use of the Website\r\nand participation in the Programme shall be in strict compliance with this\r\nAgreement, all other procedures and guidelines set forth on the Website and\r\napplicable law. You may not engage in advertising to, or solicitation of, any\r\nUser, donor or any other Person to buy or sell any products or services through\r\nthe Website. You may not transmit any chain letters or junk email to any User,\r\ndonor or any other Person. Illegal and/or unauthorized uses of the Website,\r\nincluding collecting the name, email address or any other personal or\r\nconfidential information of any User, donor or any other Person by electronic\r\nor other means for any reason, including, without limitation, the purpose of\r\nsending unsolicited email and unauthorized framing of or linking to the\r\nWebsite, will be investigated and appropriate legal action will be taken,\r\nincluding, without limitation, civil, criminal and injunctive redress.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Although\r\nthe Foundation assumes no obligation to monitor the conduct of any User off the\r\nWebsite, it is a violation of this Agreement to use any information obtained\r\nfrom the Website in order to harass, abuse, or harm another person, or in order\r\nto contact, advertise to, solicit, or sell to any User, donor or other person\r\nwithout their prior explicit consent. In order to protect such persons from\r\nsuch advertising or solicitation, the Foundation reserves the right to restrict\r\nthe number of emails that a User may send to others through the Website in any\r\n24-hour or other period to a number that', 'uploads/2015-09-08/1d95aae8a4b038959ccf949adf2c444a.jpg', '', 2, 'page', 'terms-and-conditions', '', 0);
INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `name`, `title`, `content`, `banner`, `banner_caption`, `page_section_id`, `type`, `link`, `video_url`, `user_id`) VALUES
(37, '2015-08-31 09:01:07', '2015-09-30 03:05:01', 'Successful Campaigns', 'Successful Campaigns', 'Gennex', 'uploads/2015-09-29/7dc76d7245c7c764429f8b24bc631796.jpg', '', 1, 'page', 'successful-campaigns', '', 0),
(38, '2015-08-31 20:59:28', '2015-09-08 23:29:07', 'Our Partners', 'Partners / Angel Investors', '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYwAAABXCAYAAAAXt3ISAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAKGFJREFUeF7tXQl4VNXZDggCsopWRBQErWDRglRlERUEi4gKFkUUKU9RUPsjUKwVEUVxrSsPVbFuaCsVSEJYshEg7BB2AmELEMhCICQhIWSDEL7/e8/MhZvJzD3nzkwmEziH5z4hmbuc8947573nW94vhHTTCGgENAIaAY2AAgIhCvvoXTQCGgGNgEZAI0CaMPRDoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAKaMPQzoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAKaMPQzoBHQCGgENAJKCGjCUIJJ76QR0AhoBDQCmjD0M6AR0AhoBDQCSghowlCCSe+kEdAIaAQ0Apow9DOgEdAIaAQ0AkoIaMJQgknvpBHQCGgENAJBTxinT5+mkpISKisr03dLI6AR0Ai4ReBM+Rk6x/90q1oEgpIwzp07R4sWLaLhw4dThw4dqHXr1tStWzd66623KDk5uWoR0WfXCGgEghIBEEJWaRYl5SfRiqwV9POhn+mNxDdo8NrBdGfcnZRenB6U/b6YOhV0hFFUVERjxoyhkJAQt1ubNm1owYIFF9M90GPRCGgELBBYemwpvbTtJRq0ZhB1WdyFfhPxGwoJ4/lhPm8RvC3kLTSEDhUd0jhWMQJBRRjl5eU0efJkj2RhkEiTJk1o48aNVQyNPr1GQCMQDAhM2D6BQlYzKSxyEgTIYg5vs50bk0XduXXpcNHhYOjuRd2HoCIMmJuaNm0qJQwQx7Bhwy7qG6MHpxHQCDgQmJg4kUKiTQRhEIUmjIA/IiFwKO/duzfgW0FBQaXBzp07V4ksQBjt27enkydPBhwwfUH7CJw4fYL2FvAzZrWd2ktHio/YP7k+4qJH4PUdr2vCCJK7HALTjid/QVX+ffbs2ZUg+PLLL5X7Al9GWlpakMCou2GFwNPrnnbYml3fDM2/h4dQu0XtKK1I31P9NFVEQBNG8DwRIRs2bFCepP1JIL/++mslFGbNmqXcl5tvvplOnDgRPEgGQU9KS0spJyeHDh48SHFxcQQCHjlyJH388cfV1rtNuZuoaQSbGUEYcyXbghCatm9atfVVXzg4EdCEETz3JahWGDt37qQGDRookcagQYOCB8Vq6gmCBCIjI2natGkisqx///4EIgWGtWvXPo/jI488Uk09JBq3dZzDWWm1ujA+mxdCt0ffTqfKTlVbf/WFgw8BTRjBc0+CijDOnDlDL774opQw6tatS0uXLg0eFKupJ1hRXHXVVVK8nnzyyWrp4aHCQxdCIFUIg1cgtcJq0X8O/ada+qsvGpwIaMIInvsSVIQBWI4fP05/+tOfPE6CiKKaMWNG8CBYjT1BwELLli2DljCmJk11mKLMIZAy4uCY+r7L+1JpeWk1IqsvHUwIaMIInrsRdIQBaAoLC+mbb76hvn37ijfoK664gtq2bUvPPfccrVmzJnjQq+aegDBatWoVlISRU5pDt0bdKnd2uwmRrDevHiFZSzeNABDQhBE8z0G1EYY7p7crLMXFxZSZmUnp6emUnZ0dPKgFSU+CmTB+OvSTIxsXjm7ZqsL1c3Z+j0gYESQo625UNwKaMKr7Dly4frURhruw2uCBpWb0JFgJo6S8hPot76fu7HYlDCaaZvOaibwN3TQCmjCC5xlQJow333yT9uzZ47dNJ935/hAEK2GsylpFl829zHp1YeXXwGfs+5i4faLvIOkz1HgENGEEzy1UJoxvv/02eHqteyIQUCWMp59+OqCIjUwY6RCGszBFXTaHCcWKNPj49lHthTqpbpc2Apowguf+KxMGksAC0ZB4piJVgjoZdhuUcA8fPkyrVq0STvWJEyfSn//8Z0Kewv333y8k1Lt27Uq9e/emIUOG0IQJE2j69OkihPfAgQN06pR6fgAk2lNSUigiIoKwOnvmmWeEE7979+7iOn369BHXxfXx+cyZM2nt2rUiex1EoNJUCeOhhx5SwhS4HznimzwHQmmvjbiWQjinwi1hsE+jaXhTemnTSw7C8EQa/Pda82vRzJSZKlDY2gd5HpDC3pq7leZnzKcvk78UekUjNoygx1c/Tv1X9Kdey3pR1yVdqceSHtRneR8asnYIvbLtFdGf7XnbA5Ircrb8LKUWpVLM0Rh6f9f79NzG5+jhFQ/TvUvvpa5xXen+ZfeL359c+ySN2jiK3kl6h35I+YHijsbR7vzdlFmcSSVn1Z4lOwAWlhXS9hPb6dsD34r7+PDKh6nH0h7UbUk36h3fW/Tn1W2v0v9S/yfMiqVnfYt4CxRhZJZk0pKjS+jD3R/SXzb8hR5a8ZAYF56DXvG9xDiHrh1Kr25/lWbsnyECM1JOpVDBmcoyR3bwrEn7Bh1hvPfee9KoH2Scwzym0g4dOkRz5syhSZMm0YMPPkhXX3210vlds9obNWpE9913HyUmJkovi2S6Z599lpo3b277Wtdeey0NGDCA3nnnHVq/fr3ltVQJw06G/lNPPSUdn9UOn+759EJWt7sVBpuaMBnvKdhDt0Td4plYcCw7v/vG96Wz58761Cd8oVccW0Ff7/+axmwdIya16xZc5yA0VjoVznmWJhEhwNiwOsLG1xebIaONfXhfKKP2WtGLpidPp6wS/6+A8k7niVyUoQlDHXksRh9Bwua+Gf9Hn/EZ+od9mZTRx5sib6IBKwfQxB0TRe2IxPxEn7A8WnxUkOsDKx6gunPqOnAz+uQBq4ZhDemxNY+J8YBovGlVTRjrc9bT2K1jqW1kWwd+wNHduMzPgRNnvPzct+w++sf2f1DkkUif8PUGm0AfE3SEMXUqx+57qIVh/vvu3bstscKkjTf4m266Sel8KtesU6eOWJ14aliFwPyDxEKV88n2eeGFFwJOGL4k+RWUFYg3clGfwJM5irO+3016V4zr+Q3PWzvGnbLVa7PX+vS9wJtw4+jGjsnfLJFtSJUYKx1ZvoixH45zTpC3x9xOc1Pn+tQ/4+Cy8jKxQkDNBzFpoa9GpJnVagxYm8eA/hkTHya5SAcRtlzYkg4UHrDd1/Jz5aJfHWM6XiBSXEOyQhSfox94Hng8eDZiM2NtX7+qCOPUmVOCTJsv5Bc7YATMZeMyY23gDMJeHEJ3x91NxWeLbY+vJh0QdIThrxUG3vBlE7Ldz++44w6h1eSuJSQk0K23ct6BAtmp7gOzmVULthUGlvO1w1iSxFMoLU8eDcIaiGppaGFpYVQntI5jUnFHMJhweMIbtWGUT98pEFTIEucEZzfE12p/9I+Jo25YXfrn7n/61MdDpw7Rk+uedGABMvImHNmqr86CQzvyd9jqJ1ZQwxOGXyhYJCNVT31wkuwV4VfQR7s+slVOtSoIA+a6P674owNrT+ZTO88Kn+eFjS8QTNEXc7toCQO+AdWJWXW/J554wu2zkJSURFDPVT2Pyn7Qgtq2bVuNIgzYfS2d3fylgp27qKxIjAtveHBsWyrZ8ttb64Wt6XCh98VxPtr9kbU8tp2Jwd2+/GZae15tmpHsnQLBltwthJWKeMv1N1EY/XWuppJOJinPZ8D8/vj7z1e0s51P4wErTNBv7XhLuR/+Joz80/n04PIHHXh7S4DmseEcvIr6/uD3ymOqqTtetIQxYsQIv07gmOThV3Bt0HOC6J8KCdjZB/U+IJNSU1YYaYVp1CKihWNZ72m1wJP/a9tfqzAkOBAt5UOcqwzYzr1tVU4YGC+P7eoFVxMmfzst+WQy3RZ3m/c5K6pkZ5MwUMPkgfgHHP3yx6Rq7idWO7ypEqy/CWNyIlf1hJnMX+TMY2kY3pBWHfdsrrbzTATzvsqE8cMPPwRkHP4ySfmbMOC/WLRoUSUM4FA3K8PaIQWrfYcOHUpnz1o7e4PJJPX5ns8v2IDdTWLwR7CjdF32ugoYrs9eT3VD2edj9eVlsxQc1UgI9KYpEwbIznB2Y0LBBpOFJxJ0HSfvj9ofsPmrNERr9Ynvo04WwAj2cvTJ3D/0GaasXz2QNfppgzDOscFo9MbRapMqrovrG/0x+iIjMh7HVRFXiYgzWfMnYSB67OoIDnyRmaFAkrjvrlgbJixzmVge820xtxGCAi72pkwYY8eOpfj4eJ+33NxcS0wDTRjNmjUT1fvuvvtuEVqLDaGvt912WwVhv/r16wuJEnPDhI6JXZUkbrzxRhFS26tXLxFxhet06tSJbrjhhkqO8g8++ED67FUFYWC1ZLfB0YfIJ0tnN39B71x8ZyVRQUTOSI/lyQ6JgGuOe6cjpkoYrRa0os6xnYXZDJEvCFv9w+I/OCYYTISyN1KeRBqENhAhpypN9EvlvE5fCVZuv438regfwjzRP/y/U2wnunLeldb9s0EY89LmOQjIk2/JcPzy5Flvbj1hTgNe9y67lzpGd6Tac9iPhXHJViZsEhq2bhiVnSuzhMufhPHZ3s+sn1ODXJnQWsxvIZ5Z4Ay84bTvEtuF2i5qSw1DG14IoogNocdWPaZyy2v8PsqEoTopyvZD9JJVCyRhIFR28eLFIvcA0urmBmJDCG14eDghcuuNN94g19wPEMgtt3BoqMTRDQHFzz//nPbv30+oYWE0OMiQ8Y6IL+CCQkePP/64EBRE/oasqRJG586dxfVVtqioKNllK30OJ3b9efWtJxg2bUzdOdXtuT/e87F8guEJ6MVNL9ruGw6QEoZzMo06EiXyK8wrBOQxbMjdIByatebUkmev80QzJWmKtJ/IV7lhwQ1ycUZnSO2jqx+lsPQwUZHQ3D/xDJ05KXIDhE3eyunM45T5MDD+O2LvsPZFAS9++x64ZiBFH4kmmK+Mll2aTRHpEWKClSoVw5QzryFtytkUEMJAePbA1QOtMeex1Zlbh8ZuGUtbTmyplEMC/9vBUwdp2bFl9NW+r2jkppHUJqoNvbVT3ScjfTiCeIeAE0ZMTEzQEAZyMmRmH3NnXSMgtmzZQvXq1ZMSBnJAVBtIKTU1VSj2ypoqYSBirCrbuC1cJMkqlBbRUeENyFN47I68HdQ8nEMbrUw/PBHfuPBGSi+quMpTGZcqYewr2Gd5ur9u+qvDlGH15szE2G9FP6lZSpCkFWbON93Lwi8TYciy5Dfx5hztO2HAcWspGomxMwZjN4+l0+Wek2eRS/L4mselGf8w+UzeMTkghIHVbIeoDtbPGd8/RE/ZaRmFGRVI086xNW3fS5ow8Oafn5/v9T1DZrZsdYHPv/76a6+vYXWgKmH4klsh6zjeKBHFZDnZ88TYc2lPy8StB+M5asVKTsRpU/52v32JGlXCkIWcphamCjOFjNgwKVkRW9HZImFKktY5ZzxQsVClqY7RaoVRXFYsEiUtKyQyccNEg31lLaMog9otbGc9Tqep0io5018mqfwz+XRNxDXW9y8qhJ5Z+4xsaJfs55owfCAM1OZQIQz4KpKTk/3+kAUDYcw8OPN8drFVdJTsLfKb/d84vshWb+9MPMhclr1tuwKtOpnKCAPnFSYNK4cpm1ngT0D2sKeWmJfoyJS28hHwxNwhugMdKVaTalEdoxVhoP5643mc4GiRF1MrvBaFpoYqP8u475arMr7nzSOa075Tnld3ASUM7s91C6+jpUcDWI/lzEGiMlZmPsOb+Mm/q7ayTKJy57E4Hht+J9/UETxdXhOGD4SxefNmQvSUCmkg4xx+kLCwMELeBsJxfW3VTRh4K4Szz9K0Akfw3AbCD2DVYBe+Mpwdt1aTKH+GxK+tJ7bagk51MlUhjHd2vmP9xsx9hNMXMhGemjD7YJwWOlow1UxKVDdlqo7RijCQES9bXfwu+neE4liqbe3xtdbkyDjUmVOHwtPDPZ7SX4SBvJ+bI2+WR71BXj+8Gf1181/pv4f+SyBS5G5UTeOJPa0d0dEQohTe8BO/q0z4pfw9yG5LlOE8Fsdjy8I2jInD/xpXmjB8IIyDBw9S69ZsjrGZ3X399dfTXXfdRUgEhJPfcLzbFVSsbsJASCRCIy1NNPw2ftfiu+hMecWgAtcvH8gHAn+WE5YzJ+PNnW/a+u6qTqYqhPFTyk/yvBFeHcxOne2xj69sf8X6rZsnrCvCrqDFRxcrj1N1jFaE8cKmF6zNgrzCg38G5raM4gylbUPOBkdEkacXATjQebyf7/28ygkDz+CDK9j0idBkWdgv+uuUZoGeV+fFnemRVY8IyX0EH+AFx1ttrIoDZcJIbcO1qXmST+VN/OTfZYRxjr9POWOIinn/w85jcTw2EEgab2f9X3Qu4IQhi8IJZJSUrz4MVAR8+OGHbROGK8EgjwN9gfbVv//9b+XqgtVNGJ/s+USeQ8B2eExmKg0CdbUjOCTTyizFX/aOUR1tfVlVJ1MVwliYsVBOGDzR/Jzys8chP7XuKeuoMCbZGxbeYMvBrzpGT4SByRS11C0nU74vWA0gdNjOVms2R5d5mqCdLwGvJ75e5YSBC4ikPRCBjDDMGfLm/Byn2bRRWCPquaynUBGWBUtYP/terjDO7GJSqOMgCGNlYfwEgWTwZ6d+Vfna2dpHmTBQV/vKK6/0eYNUuFWrSYSBccChbXeFIdsfOSEypVpcuzoJA+GmXRd3tbbn89tjvfB6FJMZI6JIZNvG3I10XQSryFpFS8GEwWGPkCVXbaqTqQphLDm2RB4uyo7TH1N+9Ng9yGZbmvGYFCGZYsdXozpGT4SBt2VRg12WpGgIChp5Gio/ZZMzT+Avb3k5IISBRNFGEY0ckWCyfnlSLDCLDnIo841RN9L/Dv9P9XF02c9LwihZTXTEubpwJQz8nsvbkTuIzvlu+jZ3WJkwkCOA3ARfN9d8B1eUaxphIMoKooQyErD7ebt27WjHDmuhuOokjDXZa0gUQbLyOfAXrvbs2sIJ3Hxec+kG8xZkuVUSvp7f+Lyy0JvqZKpCGMuzlvtMGKivYUkYvCqDr8BOUx2jJ8KA0vD1C66XE4Y3k6zsGJ50R28YHRDCwEX+ksCaZ/6UPGF/U/3w+l6qFntJGBk9iI6ZVheHXMgDv+djleF5pWvn+TL2VSYMmXKqNxd3d0xNIwyMIS4ujpo0aeJ30kDBJZi9PLXqJIyXN78snziNicKpHSTe6mSbLDsY52STTetF6oKEqpNpoAgDfgAhOeFpInWuMErK1KVQVMfoiTAQcipqb8hWGLLJ35vPefKGg9lT85fT2zg/kiZByNI8GDtjYZJHIiYKKtlrXhBG4Rz2dTRy+CnOry5q8f+ZIMyrDTi/M/7AvgyOpPJTUyaMQFXcq4mEgXvx888/U+PGHJJo0wEu23/2bM/O0+oiDETJwI8gK8Pq1ZJf5UvqtHv/J+U/Sl8D1ck0UIQxZB07960IAz4MnnzsJCmqjtETYSBbXFRKlBEGsDfqiPjjJ1aovMJANcNAEQauA9NUu0jOEUF2vFXEmsrzaJJKsRqH+/F5QRi5f3c4uw86CQNO7vRbiPJYTghOc7MvA1FXZalK3xOVnTRh+BAl5Qrw8uXLRVU/GQnY+Rwy7Z409quLMOalz6O6EZI8AtUvmrf78Vv4o6seVXnGlaVBAkUYSMazzE1wRkmhvohq85UwIAkCnSoZYVg6sL25lyAgvpfv7XovoISBi+0+uZuGrR9G9UNZ1ga5NUa1RaOQkt3x8EvA7dG3C6kW9WaTMM5yXs7xOy+E4YIc4MvI+D1RAQvEZruYqfDZiSnq3ZHsqQnDj4QBrCHpsXr1anr33XepX79+QmvKF3NVly5dPEZNgTAgXCgjIH9mep9XMrUTaWL3i6eyP3+prwy7UilCRXUyDRRhoLSrUUrVY7KjgmSG+butOkZPKwzIfNwXf591EAOH1aKOxJ6Te0SJXX9uuac9i5L62yRlxg0vY0ik/Nf+f9HgtYNFVUEhNulc+QgSkYlOGs8rnknOJUrISbAxQdshDC7OVPAd+yZMqwv4KmB6KvyF8y7y+P8sa5JjIg18duR6NkupJYDKOq4Jw8+E4Qo4KvStXLmSYNIbPXo0/fa3/BZnw2yFuuB79yJzs3JD8h+UdmXnGzhwoOw5UP78WMkxajW/lfRNtMrMUeZwR34rhCaTrKlOpoEiDKjuCnysJiIeG1RRVQtHfbqXa6n7oCWFiVNaAIvt9FDv9VZmXnafPH2uShjwTfjaIC6IRD1EuY3fMp7uirvLEYShQhzO6KnQdPVMeJFvYSdxL5Un/0yXVQQIomS5Y+jHBjsIxezLKODf8yrX8vEGK00YVUwYrjcFCrfwd3TsyD4ABeJAJvn27dvd3ltEnKlEaEFKHasRf7RfD/9q/XZsTOhGvWdffspWGrzKgfYRQnytWrARRm5prlrGMU/Qg9YMopwSeWb1lJ1TKITDeX1Rq522b5q1IxhKrhH2Qpr98cxNTJxoTYbOUOukfPVqgqr9glYa6pD3W8mBCjLpGqcMvVVIdeXr2iCMsxlsemrpMEEZhIDw2ZwhHD7rNIOVbmMCasI+DdM+J2CW+j/ex/c5QBNGgAnDeGDS0tKoa1fOY1AgjU2b3Ms/Q2n3gQe4KprkHDBb+UvL6om1T8ilyI03MogJ+rJJQnZhNmgc0Zg25mysUYSBzkLt1VKO3HCk8kqjZ3xPkTkO8UNzw2oP2eCQEGkdyYoDVngp1MNAxTjkzViufPhNu1tcN5t2etXp2f1+yOyXSbdDen5RRuUCZ75d+cLREFscsZ6reMoKL7ET/etkO2KjNggjmyPJXFcPp0AYnPFtNORdpHAElVkuRCTy8Xba/YunHYw0YfhIGAh7LSuzLgDj6YZMm8ZvdJLJHlngW7d61k4aNWqU9By1atWiH3/0nEim+sAgZFCYo6ykFdhhiyzYr/d/TUuPLfV6Q7KfqHMti9phuzqybWvSCgN9hVmqYQRLZqgkkDmLEcEpfc/Se0TpVKj//i7mdyLj2pCw8LXiHiTJu8bxS4xVBBeIjPvzbMKzVaivVPFufrj7Q0efrLS3eCIftXGU9FFGRruK0q67EyVkJ1ivMpwrDHu1vRUJo4TFLLN5dWEmAqw0jrauLAFy8iuH8xv+DaxEQBgwYxXHSvGR7aAJw0fCeP3112nw4ME0a9Ys2rWL0/VttJdeekk62V9zzTWi8JKn9sknLM+hsEr5/e9/T4cPH5b2Li8vz+M+QpxOVg+CyeTOOI7i8EN7L+k9eaw89wd2daus6GAzSRnQDF8/XD2BzCjPapRCxU8Qt2wVZvb5KBRQQrSStFoe+sKk0XdFX0LxLLttb8Fe+u7Ad7TtxDalQ2ccmGFdowNjZByazG9imWGPi/1y+Bfqs7qPWAVAWcAOefxw8Afr5x/Ck3NrE6IIbbW0m+Tig8WsKwZfhNk3AfJIvY6T8zj0/tRPzo0T9U6wzArCaQ3CMEQN0zvZ6pa7nTVh+EAYqJbXo0eP8xM2RAX79OlDL7/8spAMQUIfSAQFkeD8zs7OppSUFFq2bBmh5C0q/skm+549e1rW7IiOjpaew7gGysF+9dVXosIf+pKVlSUc6rGxsaLaH6K6nnvuObdFpfBm9sQaNkdZRUc5i+u8ucOeOKCnpxjRJjA5yRRsL597OcGc4qkFK2HsP7VfaEYFJJ9FwSQF/FDDos3CNnKBPmcuDEQSH1n5CH2y+xNBHhDlyyrJouMlx4XDfmf+TiENM2P/DEKhLSQttonk8/Nz9PbOt5UmsJijMVR/Loe+ylZjvBqtF1aPhq4bSrMO8QvcyV2VTGeo2hgSywTDK5ar5l1F9yy7h6Aa8Nmezwg6YSivi5U0+o/IrbTCNBH19OGuD6nlgpbWzyK/vKBeCsas3KAom37jBUe2O7Xac5y8e7RfRWc3SAB5GFg9CB+FaUNklJGjYRAMNKeyGzCpcOitD00Thg+EgckWVfs8TfqXX365IIWmTZue1+BCiK1KlT7jnK+++qrl7c3IyCBIp8uIx/w5+mTogiHZEP00Pm/btm2l2uXoQHJBspD2kNnJ64TWISiU+qPh7a9PvERGwzlx/W3r32ocYaDDkRmR1Hg+k6KKgqosCMDqc0XCQJ9gThQrSZXVCyZxmIt4X9T3bhLeREjBYGsa3lSYJ0XtD8OvZayMeLzPrFMrVISytNctYI0xmf/gV2efefUDn0ajyEb0wW5OZnM2hA53X9r9wioB5k703dl/CCs2DGtITcIcY8DzjjEIsx/GqWAe7R3fm6yKQVV6SHM5YTGv9oXVgFt5czZbHbyisoy5Ow0pT38DgRTB3+FZs0vlO6sJwwfCgCy5nYna7r4gGtTckLXx48f7rR8NGzYkFIZybaLAkYI5qvuS7qIutr/aR7s+kl+Xv/CoQ513Js/tZYN1hWF0NjQtlFos4kp+mExVY/7tkocNwigtL6Wha4c6HM0qUi1GiLCz/ngl+Rf8Hdc3n4sJo8viLsp6YA8tZ8FGq4qMrnhgcudJ/seDF3x3R4qOOOT4XVcqRva6rP8yQuZrQv7eVoPD2mxqckcYJ6dzfkV9x2rCWFlgxYBwWqsNWlPmlQZ8HsdZo+y0PdO5eTyaMHwgDJhx7JKAnf1fe+01pWcPK52WLXm5rODLUNkH/hjXJvSPZG/BbGaYsmOKUp9Vd0KSWJNQ1umyMkfwZFQ3vC7FHnXv1At2wgAWkKros4JXUyBlEIchV2Hl6DUmOkykuDd4w/Y0qdkgDPTneOlxR3/8KdJn7huP8/qF1yvLnyzIWEC1QlkmXWXVg+vw84LVgTmJbnHmYnmlQ7tEbOzPOKHMcPFZN9pvp+OJSt0l8/HKIeexiol2gjDYRGWuh3H0TxVJBWRxiCOhMntbb1CrdZU/BzmVVH4hVP0+asLwgTCeeorrGvhpknY9D5LtCgrUK2Z9//33hGgof/Rn8uTJFZ6fXfm7hL3XcknOX9CG4Q29coLKHtb+K/orkZUnHZ+aQBjAACY41AQZsHrABZu9WbICphMjTBl/5zdaVPcbsGoACQVcK5MNEwb2hRyGasssznRUVMQ1ZeYguxMtnhcurLTs2DKl7pSdK6PRG0c7CEyFNLi/0ONCzovRxHOAFw/VVZPKmEDE3KeOsR1pf4Gb4JRcdkAXYmXAZqecsSw7zqZTYzvG0jZYBZjNSK4rjNNb2PfAOlHm3AvoRaUrqBmXMjHAt+F6/uMjGRLvIjs1YXhJGEVFRQQntz8maPM5MOmPHDmS4FC327DiMfsjvO0bikKZZejhEBRvsFZfNJ7M7lx8J9lRWFUdn4hOcYaWenyD5skTNSSguuraagphGP1GIiKS0GamzKTxW8fToNWD6N5l99LdS+6m7nHdqd/yfiKEdPq+6bQ5d7NIXPxi3xfS5DbY5+0W+8G53056m65awC8Mzgp0PmfxGzU1eCVlVWnP9T6ixOqz6591PIuy55E/77GEJcBNTZBfjJ8Iw/DJ8HPfe3lv2pXnxsxTxol2RzgUFqSAN33kUJg3hL66OqfxeaGptkZRWEWhQSPi6djj8q/PaTZnZ7WoqGoriKeZ/FgPe4QkJHBsscJb8hdffOH1RewcOGUKZ6wq9EcWwjp0KNtgJedp0KABWYWRWvUbdTCGDBlCKCwlu47q50jk++UX1oTxoc2fP18p+9uqT926dSMQIhoK6/SK70Uhcc4vqfFlNf/Emy/bu1/brmZCszs8RBOJFQ4ymd1dH3+DGYdJxV1I47tJ71JIvKT/PAaUnJU15JYICQ7DDOSuP8tC6NuD38pO5dfPEZlmmenNb9Z444YD2ZuGQIYRCSOEU/v82LHqwBu7q3/CSDo0+wbgUzBjxauEzvGdbSfbIVoPRNk+liVxjHtg9MNsxuO/Pb/h+fNDhQba5B2T6ZrIaxyrJWPl5vR1nB+D60uRoVTgLCVrFilsF9WOPtj1ged8lNwJjtWFKyl4ckzn8b5Z3VgTKsvR73OFbBvkwABEPRnHIFQWk/4ZxZViLq9qkNxnHA9J9GOcCV7kuYa61fMRgiI90COSbb5OZKoP6YwZM6R9QV9RT9uqwf4vGxMmaDtmH9frIWlv586dBHMQCApCg4iaUnnLx0qiWbNmhEJJTz/9NIWFhYniVP5oCJdF/RJIgrRo0YIgL2JFECA91CZ/9NFH6bvvvhN5H4ZCLpbZqMmNLyje4N1uMe3p1uhbxdtuVbTyc+U0bvM4ar/cog/R7emWJbfQO7sqa+Ygf6T9Suv+t+cxqLx9b8jdQO2X8Ln4eh7xWNGewtO9+0J6gx/weWotm0clNTaQlIeqh760xBOJ9NGej+ieJfdQq0WtHKYzZ16GWIGA1LHh/079JajBQtCv3aJ29McVf6QpSVOEKQrht9629MJ0+j7le+q/sr+ojdI4vDEJJV0QAK69hHXGdlfUGSsrL6Pkk8mEIAPUL+8U04muXXCtIwrKnOcCZ7/rOJg4EPGFjHpUTPwu5btKmfeVxpLPdcohQ26sJDDZu26YyCHjUQqHNK+IzprmAKxQsOJAdJOxMsH5Utl/UXZADbp8Lo9cwsfAd4FzgJQIpPGQ2vEue+Fo3fyEAPSaEhMTCW/5IL7333+fkNj3yiuv0Lhx42jChAk0adIkQrIdCBhlWH0hLJVu79u3j0JDQ+mzzz4T10YfEFWFcN2pU6cKYkG+yPHjx92eDpORrTBBlU5V4T41qa/+ggGkfs18fnO2CgzgSXR4wnDC/fRXg1RJ1JEoQgTd1KSpYoU5ftt4QogzanTj7RtkDckOKMIWnFH3ydnpI3ImVmatFP6fT/d8SpN3TiZIyMvEJIEFCCQ6M1qQzz/3/JPe2PEG/X3734UpEOOYuH0iIdMcZlGvSC6fSSv7bseEjZWC64YJPJVNRHnjmASOVRw2cjRyOaw+nz/LdW4l/LNYze9z/mR5rIRQYDpHMf/fbPayAbYmDBtg6V01Av5GwB8ENzJhpHXIKcwqTBiYTHWrBgTKstnMtJQnejdbGf/NDxpPgRqVJoxAIa2voxFwg8C/kv9F43eMF9nQR4uP2sIImciIDDtfAc9TVA+baRqHNab1OaxHpJtGwAcENGH4AJ4+VCPgCwJw4IqiRRxQUHt2bbo99nYavHqwkMz4b8p/aU32Gtp7cq+wlUNmGzZ/mJ/ijsYR5MyR+CacsLJkP/Zt9F3el3A93TQCviCgCcMX9PSxGgEfEMgqzaLfRPzG4ahFhA+ifhDtxb/DgQtnMVYGSEAz5DYQpQTtLHGMLNTYKcoHQgmkE94HSPShQY6AJowgv0G6excvAnCiQr+oUiKailSFavIZ+y4gGonEN900Ar4ioAnDVwT18RoBLxFAednzeQwqWcV29+HVCmpmII9FN42APxDQhOEPFPU5NAJeIAC1VmGCsksEsv1h3uKVxa0xnBuTUzW5MV4MVx9yESCgCeMiuIl6CDUPAUQ4dYjq4EgQM7KJZUQg+9xZ8Q3+jYFrBtK+k/tqHjC6x0GNgCaMoL49unMXKwKoxz1s3TBqGtG0olSFWebCkNxwJ1eBz7CSMEtu8O89l/cUGlTIatZNI+BvBDRh+BtRfT6NgCICRWVFtDt/N806PIvGbB4jxPJQga9ZeDOqO5eLDiFz24icMktV4G9MGChYhAzvTrGdaMzWMRSbGUuoy62bRqCqENCEUVXI6vNqBLxAAMl7qI0RlhZG0/dPF5IbkxInCZkKbJDfQK1zlDxFSVHUC/FHtrgXXdWHXIIIaMK4BG+6HrJGQCOgEfAGAU0Y3qCmj9EIaAQ0ApcgApowLsGbroesEdAIaAS8QUAThjeo6WM0AhoBjcAliIAmjEvwpushawQ0AhoBbxD4fyWKHJTWr78fAAAAAElFTkSuQmCC" alt="" title="First Angel.png"><div><a href="http://firstangelsja.com/">http://firstangelsja.com/</a><br></div><div><br></div><div><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAADZCAMAAAAdUYxCAAAAz1BMVEX///9gXl+KiYnk5ORYVlYAAADsACHsAB3sACDsACrsACXsACzsACjn5+fsABjsABzsABXtGjf5+fnw8PD0j5h4dnftEDJycXH+8vOMi4v2pKvxY3G2tbVLSEnyb3v96Ora2tr4vMGioaH5yMz719r1maHzf4m6ubk4NjaAf3/uKkLxYW+rqqr3sLbycn7uNEr0iZKdnJzwVGT83uD3rbPNzc1DQEHvTF3yeoXwUGDGxsbrAADuL0YfGhv60NTvQFMzMDFoZ2cYExQ0MTIoJSVwd6MOAAASRElEQVR4nO1dCXuiPBfFqmEJhkVNXVqR4lK1at2t0+q07/z/3/QlYREUW/QbxqHDeZ5WhQTuSW5yb25C4LgUKVKkSJEiRazYzp/ru76m9VeN0rB1bWliQqX2ogoydCFKAprWjGtL9dthTVVCUuLV/mpQrw9e+kgBIpTVlXltyX4rrDEPRdR/Nn01aFjtFZIhwMPryfWbURkgLMmlWciZOSkB0N/+eZnigCVAGc5Pnd2ueKx2/qQ8caGkYrX0WQJLE+Vx5U+JExsaAGohShvAGmGY9P63LknTr1MNVSwnm+laAqMo6bYIYz1uYWJEjZci8SRMVfgSryxxoqWKg6hphwqoxSlLrMD4NXriBkRJ7XrXonJGD6MDMaHmtKUKZ3l3NVlNZpXuzu1eBDmRrdTk1TNNY0fexSNKvBjL5zY5CyRRd7fK+Z0oAlYcosSLgXR+H7qT2jFIEi8q6rktlGAN1jGIEi9qIIIvf4gSiOxI/TUYCxcEgy4qnevCQOiCXAkkOhcuUcJ28lR3wF8S23tOXmfEo0sCBgPwaXDpL8RM0S7JpoGkhbOHSsTAQgAVxH8VRvvb0FFOxnE/gSkJv12SmLFDl0yVraXEdbqCeklIT5KSNg1joP4FuRI4SrNQ/YJcUzF5fhG6wB4aauKMC/eMLmhsI/Ei23tV1NH5kQJDlS4xSdfF7gIHsA5hDJLEDAmdbV1aKkhehXKIPzvLDl5ika4MA43PzTIHKIELGVroXINY4WEjFlHihYXOHbtMoZTEaeAhej4zg4AS5ytQ1NB5cWgDiUlUXI4rofMsxRgmzydiWJ+niCNZTVpgwUHjLA9wrvCJnBYlmJ5jE7eqnMwGylFXN7oqGgAmcvaXoR/dp9c1iJMWVtjjjHU0Y8gneGlc9LnuFVQT6OK60CPPpE1FNZEekYMKihiHrstq0uKbARiKGCkd4ZnAsbYPMwVHSUZ4JtVRcNBSogQLBonnyW35CAGGl+Tz5Cx+9WWanZjsfojB/JKoPoaJtisOTP6LkFFFg2oC18IdYch/Ps1pQJjEkN8x5vyn466WgKWEDrQPUPuUqKVCnGA/3o+a8Em0c6jCfnLHZUG0hdPLomqqmNxx9iHawsmVuiUkJ/gxnkOUThJdC9Ilc/5/K56FE4H6BkjeWr/PcIroQOLPnKr4y9EJJzqVL1nC8TejI4QRIjyT92TA5wglOhWTPyw7RBjRgYy+HU9iRI6I1r8jT250tI56LSnfrX1SHBEtCacsa7JxSHSIoj7onTAcEN2qyVu1GQ2NAFGDh2evOkoIgkQ1DL/L+PMQAaIvMKkLFL6Gn2gJXLJ0NyHwdUYtVfyeHS6Dj2gfJ3QFUSTsiSZzzWZk7IniRK7ZjIy1S9QEFzznnSB4RBvydwqFHcMjKibvQZaz4MaMKkj5rj6RDZdoS7jkgfYE4Z8jOuOvvuVUpbTTdqW4un4vgK2CK7sLc1XEEIpqTHEcj+hKvq6jW0Ia0BovCCvxRHKe3UmmoXTRJgW/C6aKRWrfKnEtrdzPpuFrLjrWBewuBd7FI8eeqInk602flfbPJMxQLL5oaT/jPZKvF+gUxf2td7E8yuif2n8RwZX83ZZ/F5PTc9P/DwJrGKaS2L9K0GgIfMHHr5Y+XYZ2YFVKB32xj3BMaAu+sbDFx7F0IkiUM3ko4z8/jKkJvjYTD9HDdUaVKcKg/6epmsAXrjJjUd3jlWNWH2Cg1f7oI6KG6ttKp8bHYebClsiZr0SB1cZFazqNrWWapmW1zrOFO9+zmmshDnc3fNGjNVBlCOS1eU69btt1DfEASJIEgKAgrTGPPCAagv0cwe6SPe2+xKnVnZV2XxGhpK7a0TxPs8ELMsRQpiQFwlYkP6ToQxENewZGjWVHqOHp1Z2tkka4igDtAm9bCEHlGQD67gmw6rSH1rbV2lrDdkNDMo68uGWryo5vZAEQVfhz8AlRgll7p0qQcFDAtHSKrd5RIZb4xvDwvDHf4cizkB3ZWej9HM/KvKHyRdi6Yj6PESCKKJ5gOwQQo114s1rBrx9NcLGD9t7UWtiOUDOz1q4db6RljiIvK/mSKEXFatchAjJ98QvCowCphoKV6Ym9vF6gEn16roKxptPnU452hDIbEi/JoqzY/qnVLjkSDBQYee9GMwpRWxCrRpqdRPsYpePJsiM90Ak7ZJKU52ihPd0+koNrC4wOaf9YBggpMlZnlQ4PRJFnQ9a6jAUctSijE2UgitwnfQx0H/XfwVMvZTDHAlbDLb9xwupsVbgz+EAg3ZqS9i+rq1qrwlVqsqYJpDtACNJt8muCPNDHONIjZ2cTZXK2NUmzDUddPLHWda4BLGnhVW39OLUhvqli4BfcGvOkTHeeOW5gDaoDi/ZcL9xMFYm+WErEGQZTuaSPm/MaNfBzPjyi1pYlLIknhs8G0k7aj5KsyZ4qGi8KlpXO3qg2ZA2uWYfV4jE3trfg2UXs70zlIg/akOCIq/A47CY1ScYSPNkdvmLt9JOcrxp25amppKNv+1yzF1nEkl19M15zfSlLiBZ5sZTL1hXVpDHXEcFxazMxqU3s1ub0KDgzgFiTTl11KxDfiqmY/gIOhsYvImqPFJvoFmia62D05Ugj6CgP4YVhCHacKh1Vm7HiCU1P+9bgcFeojgSn+NSeWDqEg7VEu11Dg2AVqCm2FHPH281+LmteW55Lkboji/86uGi0rGGtXXomKLVrQ2tWoRvbl4bS0cZjRN2g770hLfXwjRNtHr5w4ilndke3EGgAcdoi1Rl0HkuAbukhCbbF7kBt7/kLQpRxliV88qCsYdXWK1HlBYnYa5G9KU4kXruAQB8DYwAOHdkptSj72qoQOxSsuzmiQdspH95RDkS2/GdNx8MHEySWSmOxhrvFADEq+/p5jrSLqAXCK75ilaZAAexleGxEwisKIkZb4QVRo8BjzAdbaEWDEPvkI67OwT4sc5WtwDvxrM1ABvZxUl/yQVFA5jSbwNahiqr5NlY1Im0+b4GQnmFbGqvEvYWygORd45loa2tmVBgMw6hhTRN5CWpioBcwJBzYFduAh8s/iGL3aQ1vefn4npWx6PpRHYhxUM86IuPScVKYEvbfqR5lVYIFDudFzYZA3+8nkNHZsHXcq7bqQIOoXTFfSO/pa98VKdisLB5rwSp/RrBva7J0bOSHALuRQBPBFemyfCcN1Y5va07v1wgu5ZupERYOEaJ+aawRT0aWQBmEDBWYPDuEoTqiWVqki5f2dfqKA89Ht1XtoMYHAI6dFvssHyzdMl54r5gMREi2eX877ojM8rbcV5jwMFiFgwibSVnA9+KeGhYgFGDnRC+2XQOJ0KzbGQyFtFTPx6lJAe96IJCxiH8Y0tKg5BmyihpQav1ZxbLmluyYvcJr7S8LYL9bpuG8eWUoHezYYzBv8CuigpupBmQso9EJLbA6pAiIf732yoXUqCa44mnQ5wzOCKtp3b83GaGCfF5xGxB/3JXymRfh3jcYOgvYBMXTKaJ1OufbAXYHD52ctgS+mjaygNuRE9sg4fAoZ6s2RWSsBJV+zVdJI0hq1Kl8A/k2xiGdDnr273puYhkHF45OZSh0zO3WLI0R9JSEYuVUzmr/6qS2PUKaOvtLzkJewLKC4IvBCel1bXkaMu6HDO0Ms/TCCzLGIo+fg+3WUCH84RD3rfUwdgDTfcxUdz3PkAaKdwcWoK6Q0SqNoWFRrfsV0V1isAOeNPZS2xpwjFVHPLaIeh+Kmk2k0h6F1RYhyi5YUfEucL4ys2qdFa8QUTANkLWPe6dZo+7q+UxxaFU6xBfvU1Y20VlJJO1aOXbxzVckEQONXtvBIlBt1TD4veoyz6TmbdMnhr2zTd8RFxsMOp0pkkN3U9i6RH9oWJFeX+qN0agxWPUB4pm3IEo8eilZX8Z3ic0kQ2NrhCBW7da6w/1OHfO0yjqhI23DnA+tozMaHBhcZQh9a2pHct2cCt5q/xPL/tuKxF7zS+4XdrvKD8dTnKsysdMYu28FJqWtoH69ZEYLsg5VqKi0IXvDbesHJD6HKMDSWet62jwWkCL7LeWQDhTk1y8Le9gYQ1lbnbjfyPWMKvPGTiZeHhK03ZS4Q+b2rFmF9g9Ki9f2w+1tvf86iBj/9gukyqKsBoYuU5WPaa3KJTBKjVFIQz4fs1p7fqBGrW/7TEOKFClSpEiRBCzuc98Q98Vrl2uKFClSpPh3kF0ws1MsFApZ8mlHLMghvUBBftr/OfalUDzKaCd0Ax30MgX3KvQy7EDWTuUlz9qXLbg5vKwBCbwjxcAR9+7sYr7EzrVPhFyaj9Wnco4k6pWr1Z7O6Xl2+J44EzfVj+pNliv8rFbfqzTpslq92XgZl9Wn2wz9dpchx5vO4V/k+4Tjuu/uZZ6q5ftqj3ySVEsq3OSdJCHS5snHI7lxuUc+qfSZmypNWPjFsj6Sv+otOVLksj3vSI5kWlIqvd7Tktxz87Q/Rf4eyLUz1TCehSUrHZKl51TPkgmfceUkSW7Iv1siSXNynJH+v9v4jt/bH5vlA+fcnMuyfE+UJE0+ccqEniuS8ijr9qdzT3LlW/dC1SznSWAfoZmahEmh7NznyTtFsz8sHFmPkPFU0SVa3tyEEH0vHhB1Mj50w4l2N7cLl2izGkr03mHLiN76iFafHlyihTCiC0r03SF6d0y02gwheu99q972yrQp3XK9bpDo4001Qzlmlze9p4OMtGS7mV554R5/vOnRwu7ecVQtfUTvMj1bdR/ee2UqyuOm271v0jbTYyrM3fZ65SKj9b6wbzC5ZUl9RDO9mx5T3acco7PI9ShyLtHJZjPJhfDk9gd7zSJrzqRs77MBoj2O1Q4RWC/qBxlp8rs73/F7nV2GENUfgzXa1X+yFJNNkSWnRGnScpY1AKo1LCulRYizGl2wpP4a1RdOxRer9OKbB3K/ou7V6KTb9co8gOWeaMGTnPvvUHUfqWQB1XUyLsixu67vuKu6RKWamQDRDZdlPwOqS4kWuS5L4aouuR8pJH8b7e2Jkvx3Tsbse3gbDcWTLXw2SLSQ8zpN+zaF+0OirB0RvsXTREmaTICofbcQorbK+IhyzdvcnijrVO2+lx7NueZreQZRrnxTJC37wVZdYoN0RnFjG5mlS5SVebNHUnid122V9Pv3tB8KEF0STSo4RLnyAVHuvWmrLjV2LtFbeknaSDMFltU2JndUAqq6NOmGdH2L/3S3dP4jR4hFnEw8okuH6OQ0UW7Ry9zQgtvc3NyQWtVtxbDzs1orMh6TIlekKe72GcuZKqO9aPouN6GJCLnm/jKFzT7VhNpnkqLpXJxSogQKJOUdPVF07seyMpmocNleZrKXqElSZMsZmqK58A7Tn4vsaaIpUqRIkSLF74ae/ZY4Jtrs3XxD9JL4ftQUKVKkSJFYFJr/xBTxJP+xzH/sIwd517HI5il+2WP4OxZieWKHnODxPfthxyJ09j3/6F7kw41p6Hm3EN+asVGIAv2Nxqa55tIJTXHdfM/5Vvig/xd5FjW7Y+HzJ3pOv3VCZ5RM9oNFPpyZDBeLvHs53SP/2IxD/sh4dGcqms7nz2be8aAKtvBVO/68J0oqnaVgRLkNi1cdEM1tPpx5AT3fc4Lq1yXa/XlwoPmLu3HCmg7RyedEFyxUGCRazHMPjl6QEz/tMNh1iWa6Bwfeu1RMBlt1C/km/XCIsph51Q7TM6L6kjXSINEbQs05QE7oeVYi1yWaP5h5YiRzNvtC/u3t56+8/cMhSg695W3BuczH29tH3iaksxNuBJPWeO/BOUG0hHVIVyZ6YFiqVG0Xdv9RyBeL77fOCYfoLZ2J7eZZpDbXJT+yVabHpHPdzxIzBS+4JUAzU924LtFfB3fPN8m41dFWJmremRMMtNEmI2G3Ua5KrU1Add829CKPLKd9ope5NtHeJPDz7m1J8cgsJSOadeo8QJT7oLbWIZqlyfxEF/l7dhXWkp0Ty8mViWbzgdG4a9TZUVv57uyZ9iBR1rQdos03Lkg05ygB8zx0V4Oby+Zvl/4cTN6c5Q5UXReucWdN1Wll7/akj59ol5F3iLJZER/Rgvv1ya/T2fxhK/nTeMhXF9nFTZ76gDnX2BRtrbV/sH7XIZrrdrt3Zbuvvq+SH09Lx452KehFym7fy/TCK4Gu3fCviOIk95ibsK7z1jtIJzGLjp5m6eEFE39TJug92EowYT/sstHLDDTlrdcY6Ezh/pqTdKYrRYoUKVKkSCaaucw3RO6fCGqmSJEiRYoUceN/02atpCbxyocAAAAASUVORK5CYII=" alt="" title="branson image.png" width="213" height="200"><br></div><div><a href="http://bransoncentre.co/caribbean/">http://bransoncentre.co/caribbean/</a></div><div></div>', 'uploads/2015-09-08/4c1b711ca687310b9bf713dfefaf972a.jpg', '', 2, 'page', 'our-partners', '', 0),
(39, '2015-09-03 23:18:15', '2015-09-03 23:18:15', 'How It Works', '', '', '', '', 2, 'link', 'http://isupport.spidercentro.com/learn', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('hallstar_01@yahoo.com', '1b9ad79fea2c819c4cc3cb8fcad8ad1517adb458f96be17d88ec91ca55de2959', '2015-03-12 04:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `pledges`
--

CREATE TABLE IF NOT EXISTS `pledges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `project_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pledges_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_picks`
--

CREATE TABLE IF NOT EXISTS `project_picks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `project_picks_user_id_foreign` (`user_id`),
  KEY `project_picks_project_id_foreign` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_uploads`
--

CREATE TABLE IF NOT EXISTS `project_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `project_uploads`
--

INSERT INTO `project_uploads` (`id`, `project_id`, `upload_id`, `created_at`, `updated_at`) VALUES
(3, 4, 33, '2015-08-28 10:14:16', '0000-00-00 00:00:00'),
(4, 4, 34, '2015-08-28 10:17:07', '0000-00-00 00:00:00'),
(7, 1, 44, '2015-08-29 02:20:53', '0000-00-00 00:00:00'),
(8, 1, 91, '2015-09-10 01:16:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `donated_amount` decimal(15,2) NOT NULL,
  `need_amount` decimal(15,2) NOT NULL,
  `end_date` date NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_short` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `staff_picked` tinyint(1) NOT NULL,
  `creator_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator_bio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL,
  `views` int(11) NOT NULL,
  `about_project` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `location`, `donated_amount`, `need_amount`, `end_date`, `user_id`, `description`, `description_short`, `video_url`, `image_url`, `category_id`, `staff_picked`, `creator_name`, `creator_bio`, `creator_location`, `published`, `amount`, `created_at`, `updated_at`, `start_date`, `views`, `about_project`) VALUES
(1, 'X-RAY MACHINE ', 'St Ann Parish, Jamaica', 0.00, 0.00, '2016-02-24', 1, '<p class="MsoNormal">TO PURCHASE AN X-RAY MACHINE FOR THE ST. ANNS BAY HOSPITAL.<o:p></o:p></p>', '<p class="MsoNormal">Busy (2020) Helping Hands Foundation has acquired the\r\nundertaking of upgrading the St. Ann''s Bay Hospital''s Imagining Department. Our\r\nfirst project is to purchase an X-Ray Machine. The current X-ray machine has\r\nbeen in the department for over 30 years and is out of service at regular\r\nintervals, which leaves patients to receive this service at private facilities\r\nwhich is often very expensive.  Estimated\r\ncost of the X-Ray Machine $105,000.00 usd.<o:p></o:p></p>', 'https://www.youtube.com/watch?v=LbzG6OR3rjw', 'uploads/2015-10-01/8274cae50cd2427049eeff105f23fccf.jpg', 2, 0, 'Busy 2020 Helping Hands Foundation Ltd', '<p class="MsoNormal"><span style="font-size: 10.5pt; line-height: 115%; font-family: Roboto, serif; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-posit', 'St Ann Parish, Jamaica', 1, 5000.00, '2015-03-11 05:57:31', '2015-10-02 01:48:54', '2015-08-24', 157, ''),
(2, 'test', 'test', 0.00, 0.00, '2015-03-03', 1, 'test', 'test', '', '', 10, 0, 'John Brown', 'Mobile app developer specializes in security', 'Mandeville, Manchester', 0, 800.00, '2015-03-11 08:11:12', '2015-09-06 21:46:37', '2015-02-24', 70, ''),
(3, 'another one hit the dust', 'test', 0.00, 0.00, '2015-02-27', 1, 'test', 'test', '', 'uploads/2015-03-10/c37a0bc342b032d67a7cdea1efce642e.jpg', 2, 0, 'John Brown', 'Mobile app developer specializes in securitytest test', 'Mandeville, Manchester', 1, 100.00, '2015-03-11 09:18:14', '2015-10-01 22:48:11', '2015-02-23', 60, ''),
(4, 'Another one hit the dust Tv shows2', 'Kingston', 0.00, 0.00, '2015-05-29', 2, 'Dance competition TV show where dance group compete for prizes and showcase there talents', 'Dance tv show competition where groups of 2 compete for prizes', 'https://www.youtube.com/watch?v=aPxVSCfoYnU', 'uploads/2015-03-10/c37a0bc342b032d67a7cdea1efce642e.jpg', 12, 0, 'James Issac', 'Recording artist with a degree in arts. Founded the Moves School of modern Arts', 'Kingston', 1, 10000.00, '2015-03-12 01:45:55', '2015-09-10 01:00:26', '2015-03-15', 84, ''),
(7, 'Build the HIVE', 'Manchester ', 0.00, 0.00, '2015-03-28', 1, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent </span>', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent </span>', 'https://www.youtube.com/watch?v=WbMV9qYIXqM', '', 2, 0, 'John Brown', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orc', 'Mandeville, Manchester', 1, 999.99, '2015-03-19 03:34:34', '2015-08-26 10:12:52', '2015-03-18', 54, ''),
(8, 'kool no', 'Manchester Rd, Jamaica', 0.00, 0.00, '2015-04-18', 15, 'kool no thats right kool no..... :)', 'kool no is the maddest poem...... :)', '', 'uploads/2015-04-19/100x100_5c8cefb8630da604c3a64d6cace436d7.jpg', 6, 0, 'George Henry', 'kool no...just kool... :)', 'Manchester Parish, Jamaica', 1, 20000.00, '2015-04-20 05:33:56', '2015-08-31 00:21:04', '2015-04-01', 58, ''),
(9, 'My Art Project ', 'Mandeville, Jamaica', 0.00, 0.00, '2015-05-31', 18, 'L<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span>', '<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span>', 'https://www.youtube.com/watch?v=3qKmDnJVDjs', 'uploads/2015-04-29/100x100_4afa055317adf575d2fa4bffe9743ab8.jpg', 4, 0, 'Ricardo Smith', 'L<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14p', 'Mandeville, Jamaica', 0, 15000.00, '2015-04-30 06:18:53', '2015-09-13 18:30:45', '2015-05-01', 78, ''),
(10, 'TEst', 'Jamaica', 0.00, 0.00, '2015-09-30', 30, 'bbjhbhj', 'testing descr', 'https://www.youtube.com/watch?v=odQQ4jgGpFU', 'uploads/2015-09-24/100x100_9f8ef30a4ce581027224460bdf7b5d21.jpg', 11, 0, 'nameeee', 'fvdvdvd', 'Jamaica', 0, 900000.00, '2015-09-24 23:25:54', '2015-09-24 23:28:31', '2015-09-16', 1, 'vfdvdvd'),
(11, 'get girls', 'Jamaica', 0.00, 0.00, '2015-09-18', 30, 'adafvddfgfdgdfgdfgdfg', 'adassdasdadasdsadasd', 'https://www.youtube.com/watch?v=XV4dIGR_P_U', 'uploads/2015-09-30/9fde644530626f1dc0b90cd28762e866.png', 12, 0, 'nameeee', 'fvdvdvd', 'Jamaica', 0, 9999999999999.99, '2015-10-01 00:10:33', '2015-10-01 01:01:50', '2015-09-25', 3, 'vfdvdvd'),
(12, 'TTTTTTTT', 'Japan', 0.00, 0.00, '2015-10-21', 30, 'mkl', 'mklmkl', 'https://www.youtube.com/watch?v=A0owfH3UERI', 'uploads/2015-09-25/b748147dd81df985272cd6a5d9acbe01.jpg', 4, 0, 'nameeee', 'fvdvdvd', 'Jamaica', 0, 9000000.00, '2015-10-01 12:18:15', '2015-10-01 22:50:27', '2015-10-16', 2, 'vfdvdvd'),
(13, 'Testing image size', 'Jamaica', 0.00, 0.00, '2015-10-30', 30, 'Describing in full what the project is about...', 'Here is a description about of the project', 'https://www.youtube.com/watch?v=2-MBfn8XjIU', 'uploads/2015-10-01/80128e535b29c0c085425551dafea0b4.png', 2, 0, 'Phillip Lindsay', 'This is my biography', 'Jamaica', 0, 60000.00, '2015-10-01 22:55:00', '2015-10-01 23:53:28', '2015-10-30', 12, 'Jamaica');

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE IF NOT EXISTS `rewards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `estimated_delivery_time` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rewards_project_id_foreign` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rewards`
--

INSERT INTO `rewards` (`id`, `project_id`, `amount`, `description`, `created_at`, `updated_at`, `estimated_delivery_time`) VALUES
(1, 1, 50.00, '5% t the first investor who invest 50 or more', '2015-03-11 05:59:28', '2015-03-11 05:59:28', '2015-04-30'),
(2, 4, 100.00, 'Become one of the judes', '2015-03-12 01:46:35', '2015-03-12 01:46:35', '2015-03-08'),
(4, 7, 50.00, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ligula elit, tristique vel aliquet nec, vehicula sit amet orci. Integer est sem, semper eu eros vitae, laoreet egestas risus. Sed mattis leo neque, ut ornare orci iaculis eget. Nunc nec viverra mi, sed dapibus neque. Donec non lacus ultrices, interdum mi vitae, dignissim mauris. Sed lobortis fringilla diam id iaculis. Mauris semper ligula sit amet porta pulvinar. Suspendisse potenti. Cras elit nisl, pharetra ac congue vel, pulvinar eget magna. Phasellus porta mi in leo malesuada, eget congue lectus condimentum. Cras vehicula sed sem in imperdiet. Praesent&nbsp;</span>', '2015-03-19 03:34:55', '2015-03-19 03:34:55', '2015-04-01'),
(5, 9, 0.00, '<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">orem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer to</span>', '2015-04-30 06:21:48', '2015-04-30 06:21:48', '2015-05-15');

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`) VALUES
(1, 'What was the colour of your first car'),
(2, 'Where was your wedding reception held'),
(3, 'What is the name of your favourite Football team'),
(4, 'Where were you born'),
(5, 'What is the first name of your best friend'),
(6, 'Where is your mother''s birthplace'),
(7, 'Which high school did you attend'),
(8, 'What is the name of your favourite place to visit'),
(9, 'What is the name of your favourite pet'),
(10, 'Where did you go on your honeymoon'),
(11, 'Which parish were you born'),
(12, 'Where is your father''s birthplace');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `checked` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `image_path`, `caption`, `url`, `checked`, `created_at`, `updated_at`) VALUES
(9, 'fifty-ways-of-grace-1140x510.jpg', '', '', 1, '2015-09-11 18:43:34', '2015-09-11 18:43:34');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `call_to_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=117 ;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `call_to_action`, `created_at`, `updated_at`, `dir`, `project_id`) VALUES
(7, 'ccfe16ab7230a4fbb2134f3f4ebeb1d8.png', '2015-03-08 06:29:26', '2015-03-08 06:29:26', 'uploads/2015-03-07/', 0),
(8, 'c37a0bc342b032d67a7cdea1efce642e.jpg', '2015-03-11 05:12:03', '2015-03-11 05:12:03', 'uploads/2015-03-10/', 0),
(9, 'c37a0bc342b032d67a7cdea1efce642e.jpg', '2015-03-11 05:15:15', '2015-03-11 05:15:15', 'uploads/2015-03-10/', 0),
(10, 'fdd15cce365d2fe832fc9963969497bd.jpg', '2015-03-16 06:06:23', '2015-03-16 06:06:23', 'uploads/2015-03-15/', 0),
(11, '755b662e71cb719c0d2afda72b7df4c7.png', '2015-03-16 14:31:27', '2015-03-16 14:31:27', 'uploads/2015-03-16/', 0),
(12, '08dfd5cb049d158f1ec04132702a97a3.jpg', '2015-03-20 05:33:02', '2015-03-20 05:33:02', 'uploads/2015-03-19/', 0),
(13, '3ba62f092399f7de4b853de20337ca83.jpg', '2015-03-20 09:00:46', '2015-03-20 09:00:46', 'uploads/2015-03-19/', 0),
(14, 'ab1afe0e2111f348028812ec2c50b48a.png', '2015-04-13 02:26:10', '2015-04-13 02:26:10', 'uploads/2015-04-12/', 0),
(15, '5c8cefb8630da604c3a64d6cace436d7.jpg', '2015-04-20 05:33:52', '2015-04-20 05:33:52', 'uploads/2015-04-19/', 0),
(16, '4afa055317adf575d2fa4bffe9743ab8.jpg', '2015-04-30 06:18:46', '2015-04-30 06:18:46', 'uploads/2015-04-29/', 0),
(17, '73f945b302c4c8685678f964e2d2033b.png', '2015-06-23 03:54:52', '2015-06-23 03:54:52', 'uploads/2015-06-22/', 0),
(18, 'de1c9fc806869c70f767145674eb406e.jpg', '2015-07-14 07:53:16', '2015-07-14 07:53:16', 'uploads/2015-07-13/', 0),
(19, '3688e7557bd7f438809f3f4ff9b4b526.jpg', '2015-07-14 07:56:42', '2015-07-14 07:56:42', 'uploads/2015-07-13/', 0),
(20, 'c365181892bb53d1f8c23f762d5c071b.png', '2015-07-14 08:04:28', '2015-07-14 08:04:28', 'uploads/2015-07-13/', 0),
(21, 'e92eb2f9bbe5336c13818c3eabceb898.png', '2015-07-23 08:41:55', '2015-07-23 08:41:55', 'uploads/2015-07-22/', 0),
(22, 'e3939d2f3f2c758442937b5da5027674.png', '2015-07-23 08:45:48', '2015-07-23 08:45:48', 'uploads/2015-07-22/', 0),
(23, '948af6489a8de6e066e1ffb9ef9bec0e.jpg', '2015-07-23 14:44:39', '2015-07-23 14:44:39', 'uploads/2015-07-23/', 0),
(24, '9fa49f67f34296a8854c68e9e7e61e0e.png', '2015-08-04 06:40:06', '2015-08-04 06:40:06', 'uploads/2015-08-03/', 0),
(25, '96dd20bf7b8596ba5badd364f235e457.png', '2015-08-04 07:10:23', '2015-08-04 07:10:23', 'uploads/2015-08-03/', 0),
(26, '7e6d76b22f12eafb7ffb66114009e1aa.jpg', '2015-08-04 07:11:04', '2015-08-04 07:11:04', 'uploads/2015-08-03/', 0),
(27, 'a289f1841a47f25ace34d825c7a3ba4c.png', '2015-08-19 08:40:37', '2015-08-19 08:40:37', 'uploads/2015-08-18/', 0),
(28, '8e7886c2d44de9794b9b02b7742231a0.png', '2015-08-25 15:53:43', '2015-08-25 15:53:43', 'uploads/2015-08-25/', 0),
(29, 'a41fa8e12b69782b75f8b177b3413a09.jpg', '2015-08-25 21:06:00', '2015-08-25 21:06:00', 'uploads/2015-08-25/', 0),
(30, '7a38d1e94d5c71349130b4276d10d227.jpg', '2015-08-25 23:19:20', '2015-08-25 23:19:20', 'uploads/2015-08-25/', 0),
(31, '313ba58e516609b8d2f634b67c9b6c23.png', '2015-08-25 23:36:06', '2015-08-25 23:36:06', 'uploads/2015-08-25/', 0),
(32, 'c565b780b6d03d9d63c73ef7586a509d.jpg', '2015-08-25 23:40:34', '2015-08-25 23:40:34', 'uploads/2015-08-25/', 0),
(33, '77b15d87c53fc0fd6e7d6b630622528d.png', '2015-08-28 09:14:16', '2015-08-28 09:14:16', 'uploads/2015-08-27/', 0),
(34, '17fb80ba1cc641708e651fdc2ce021ec.png', '2015-08-28 09:17:07', '2015-08-28 09:17:07', 'uploads/2015-08-27/', 0),
(35, '89fe46a43292f232fba80e4f533d388d.jpg', '2015-08-28 09:18:22', '2015-08-28 09:18:22', 'uploads/2015-08-27/', 0),
(36, '5c353078e43450caf4f0c6c73a6f2568.jpg', '2015-08-28 20:00:43', '2015-08-28 20:00:43', 'uploads/2015-08-28/', 0),
(37, '7225a5de57dea5b18f849ea4a10ee141.png', '2015-08-28 20:42:29', '2015-08-28 20:42:29', 'uploads/2015-08-28/', 0),
(38, '76110d1f5f6ba7add9cc60e354990ce3.jpg', '2015-08-28 21:43:21', '2015-08-28 21:43:21', 'uploads/2015-08-28/', 0),
(39, 'a14910533fd418b7f1b4901c88187ced.jpg', '2015-08-29 00:41:33', '2015-08-29 00:41:33', 'uploads/2015-08-28/', 0),
(40, 'b0ceedf752f70069691416be8a310365.jpg', '2015-08-29 00:52:14', '2015-08-29 00:52:14', 'uploads/2015-08-28/', 0),
(41, '1350d0fcbaaa2d7b9cbc0af6f5d3ded8.png', '2015-08-29 01:02:40', '2015-08-29 01:02:40', 'uploads/2015-08-28/', 0),
(42, 'b7495f44019c135b2806fba1fd631664.jpg', '2015-08-29 01:08:00', '2015-08-29 01:08:00', 'uploads/2015-08-28/', 0),
(43, 'cbaab00d74767a2f031626472ac51825.jpg', '2015-08-29 01:11:49', '2015-08-29 01:11:49', 'uploads/2015-08-28/', 0),
(44, 'c172ba026a8ea2c8337b64cb64c20f52.jpg', '2015-08-29 01:20:53', '2015-08-29 01:20:53', 'uploads/2015-08-28/', 0),
(45, '20d42881ee4d99d14c9dad32bd4243a7.png', '2015-08-29 02:01:47', '2015-08-29 02:01:47', 'uploads/2015-08-28/', 0),
(46, '56f277f5e32c2ecfb16c9c105ed5b8b8.png', '2015-08-29 02:08:11', '2015-08-29 02:08:11', 'uploads/2015-08-28/', 0),
(47, 'd4a22bd073ff9cba2cfb90f9f44b3371.jpg', '2015-08-29 02:12:12', '2015-08-29 02:12:12', 'uploads/2015-08-28/', 0),
(48, 'f33b677941e65249a818a20bceaac25b.png', '2015-08-29 02:19:58', '2015-08-29 02:19:58', 'uploads/2015-08-28/', 0),
(49, 'ca72977d3d16f8fa5acca37931c3d58b.jpg', '2015-08-29 02:52:49', '2015-08-29 02:52:49', 'uploads/2015-08-28/', 0),
(50, '7a608754c04bd8c4b3eba4ac89c09442.jpg', '2015-08-29 04:15:36', '2015-08-29 04:15:36', 'uploads/2015-08-28/', 0),
(51, '7c1dfa3fcc34e8867012ea4a3e06a17f.jpg', '2015-08-29 04:23:38', '2015-08-29 04:23:38', 'uploads/2015-08-28/', 0),
(52, '7f0216b02a53436c7ada71d4aa89cfa7.jpg', '2015-08-29 11:01:51', '2015-08-29 11:01:51', 'uploads/2015-08-29/', 0),
(53, '984e5d5dcd8f1b66c775ccfc8786b103.jpg', '2015-08-29 15:54:57', '2015-08-29 15:54:57', 'uploads/2015-08-29/', 0),
(54, '23930903398a520da680e762d4eca8b8.png', '2015-08-29 15:57:19', '2015-08-29 15:57:19', 'uploads/2015-08-29/', 0),
(55, '731d08c906a1d5b10a712f84e616658e.jpg', '2015-08-31 00:20:21', '2015-08-31 00:20:21', 'uploads/2015-08-30/', 0),
(56, 'a7ce23bb376e7f9e20a036f609056a2f.jpg', '2015-08-31 00:25:21', '2015-08-31 00:25:21', 'uploads/2015-08-30/', 0),
(57, '58b33a9971566795a8bf571c9e5625d5.jpg', '2015-08-31 00:34:24', '2015-08-31 00:34:24', 'uploads/2015-08-30/', 0),
(58, 'be2937d2c2958f178fab0ca46544f202.jpg', '2015-08-31 00:40:10', '2015-08-31 00:40:10', 'uploads/2015-08-30/', 0),
(59, '44dbe666e6a1fee635bfee593129175b.jpg', '2015-08-31 00:43:11', '2015-08-31 00:43:11', 'uploads/2015-08-30/', 0),
(60, '7389563d085ba17fd78079ecd12dd557.jpg', '2015-08-31 00:48:03', '2015-08-31 00:48:03', 'uploads/2015-08-30/', 0),
(61, 'df28e2242e206d869571acd43e2d4f6e.jpg', '2015-08-31 00:49:28', '2015-08-31 00:49:28', 'uploads/2015-08-30/', 0),
(62, '85bf11ccff5dd5d090d33b158d0c8057.jpg', '2015-08-31 00:57:37', '2015-08-31 00:57:37', 'uploads/2015-08-30/', 0),
(63, 'b15d8e28f1e067ce58eadec89300f130.jpg', '2015-08-31 00:59:22', '2015-08-31 00:59:22', 'uploads/2015-08-30/', 0),
(64, 'd63f5e6f5d23d272c158a67d3161877d.jpg', '2015-08-31 07:50:13', '2015-08-31 07:50:13', 'uploads/2015-08-30/', 0),
(65, '89fc0dca27527e75b532348553b186d1.png', '2015-08-31 07:55:02', '2015-08-31 07:55:02', 'uploads/2015-08-30/', 0),
(66, 'eec198b6d189f023ad17c54e0e939452.jpg', '2015-08-31 08:10:39', '2015-08-31 08:10:39', 'uploads/2015-08-30/', 0),
(67, 'a335ffd9dbbcfa3cdce6babbb4d02c54.jpg', '2015-08-31 08:12:42', '2015-08-31 08:12:42', 'uploads/2015-08-30/', 0),
(68, 'd8c17f80d49f1ed5e90d581fb5b967fb.jpg', '2015-08-31 08:33:15', '2015-08-31 08:33:15', 'uploads/2015-08-30/', 0),
(69, '8d9850dc79b522edc55400a5c88608f9.jpg', '2015-09-02 02:46:13', '2015-09-02 02:46:13', 'uploads/2015-09-01/', 0),
(70, '41fc594114ddfa5ace22e02eeb1f8ca0.jpg', '2015-09-02 02:48:42', '2015-09-02 02:48:42', 'uploads/2015-09-01/', 0),
(71, 'e1b0e1ece31c392acacd288e48c2ffbf.jpg', '2015-09-02 21:44:06', '2015-09-02 21:44:06', 'uploads/2015-09-02/', 0),
(72, '4d22d8573fddcef84cf8a283489df231.jpg', '2015-09-02 21:47:15', '2015-09-02 21:47:15', 'uploads/2015-09-02/', 0),
(73, '352d3dd39b00197fda3440e27cefb0da.jpg', '2015-09-02 21:54:41', '2015-09-02 21:54:41', 'uploads/2015-09-02/', 0),
(74, 'd16b3e963e6c518069d0c32314e5fb97.jpg', '2015-09-02 21:57:39', '2015-09-02 21:57:39', 'uploads/2015-09-02/', 0),
(75, 'b7199e3f39846d2bc1189415353e5863.jpg', '2015-09-02 21:59:02', '2015-09-02 21:59:02', 'uploads/2015-09-02/', 0),
(76, 'b1e81df5c588a60a14e9a325db96a4eb.jpg', '2015-09-02 22:01:29', '2015-09-02 22:01:29', 'uploads/2015-09-02/', 0),
(77, '03588e3c30c8772f97c83463ef95a455.jpg', '2015-09-02 22:01:55', '2015-09-02 22:01:55', 'uploads/2015-09-02/', 0),
(78, '4e4d8b3ed25dd8461a02a836f9db5f10.jpg', '2015-09-02 22:18:04', '2015-09-02 22:18:04', 'uploads/2015-09-02/', 0),
(79, 'd7996463db3fa5d777347cf3a50d288b.jpg', '2015-09-03 07:24:17', '2015-09-03 07:24:17', 'uploads/2015-09-02/', 0),
(80, '629ff9ed959dd39d2b0d7ec9d111453a.jpg', '2015-09-08 11:03:35', '2015-09-08 11:03:35', 'uploads/2015-09-08/', 0),
(81, 'c47d91a339ea8745dcc032f8b7eaf22e.jpg', '2015-09-08 11:08:02', '2015-09-08 11:08:02', 'uploads/2015-09-08/', 0),
(82, '9002b3537c016da4c52cf9ee4bf54e3f.jpg', '2015-09-08 11:13:49', '2015-09-08 11:13:49', 'uploads/2015-09-08/', 0),
(83, '1e30b04e73a73d09159a581f1a98d132.jpg', '2015-09-08 23:23:30', '2015-09-08 23:23:30', 'uploads/2015-09-08/', 0),
(84, '4c1b711ca687310b9bf713dfefaf972a.jpg', '2015-09-08 23:29:04', '2015-09-08 23:29:04', 'uploads/2015-09-08/', 0),
(85, '1d95aae8a4b038959ccf949adf2c444a.jpg', '2015-09-08 23:38:02', '2015-09-08 23:38:02', 'uploads/2015-09-08/', 0),
(86, 'c859883b7f1c9a06d22e3faf18e79755.jpg', '2015-09-09 01:31:04', '2015-09-09 01:31:04', 'uploads/2015-09-08/', 0),
(87, 'a487582bf205103bafa64ce72d94b570.jpg', '2015-09-09 01:34:46', '2015-09-09 01:34:46', 'uploads/2015-09-08/', 0),
(88, 'e72f0910b02728149346be868d1ec587.png', '2015-09-09 01:50:25', '2015-09-09 01:50:25', 'uploads/2015-09-08/', 0),
(89, '6cb129543dc6397802c49c758942d004.jpg', '2015-09-09 01:55:23', '2015-09-09 01:55:23', 'uploads/2015-09-08/', 0),
(90, 'dae20beca9f82e9e064e287ebe24bb5b.png', '2015-09-09 01:58:04', '2015-09-09 01:58:04', 'uploads/2015-09-08/', 0),
(91, '9f8ef30a4ce581027224460bdf7b5d21.jpg', '2015-09-24 23:25:52', '2015-09-24 23:25:52', 'uploads/2015-09-24/', 0),
(92, 'da155d24eea5a211be1146b1da078a0e.jpg', '2015-09-24 23:27:43', '2015-09-24 23:27:43', 'uploads/2015-09-24/', 0),
(93, 'b0a76de56eb6f83ff63dde88b8c25d50.png', '2015-09-24 23:55:09', '2015-09-24 23:55:09', 'uploads/2015-09-24/', 0),
(94, '9c06d04d5246a22d8458747d257dd770.jpg', '2015-09-26 00:27:54', '2015-09-26 00:27:54', 'uploads/2015-09-25/', 0),
(95, 'b092c43b2a99344181c9cfe0c6a88cad.png', '2015-09-26 00:28:43', '2015-09-26 00:28:43', 'uploads/2015-09-25/', 0),
(96, '0eca4da316b15c2b920b298f1efb3616.png', '2015-09-26 00:30:37', '2015-09-26 00:30:37', 'uploads/2015-09-25/', 0),
(97, 'b748147dd81df985272cd6a5d9acbe01.jpg', '2015-09-26 00:46:13', '2015-09-26 00:46:13', 'uploads/2015-09-25/', 0),
(98, '7dc76d7245c7c764429f8b24bc631796.jpg', '2015-09-30 03:04:58', '2015-09-30 03:04:58', 'uploads/2015-09-29/', 0),
(99, '688265dff606513442492710bb601c65.jpg', '2015-09-30 04:55:04', '2015-09-30 04:55:04', 'uploads/2015-09-29/', 0),
(100, '1920x1200-Dizorb-On-Time-HD-Wallpaper.jpg', '2015-09-30 08:33:34', '2015-09-30 08:33:34', 'uploads/2015-09-29/', 0),
(101, '11535-freedom-quote-1920x1200-digital-art-wallpaper-1366x768.jpg', '2015-09-30 08:46:27', '2015-09-30 08:46:27', 'uploads/2015-09-29/', 0),
(102, 'gitl-with-camera.jpg', '2015-09-30 09:05:28', '2015-09-30 09:05:28', 'uploads/2015-09-29/', 0),
(103, 'owner.png', '2015-09-30 09:06:36', '2015-09-30 09:06:36', 'uploads/2015-09-29/', 0),
(104, 'isj_hand.png', '2015-09-30 18:01:50', '2015-09-30 18:01:50', 'uploads/2015-09-30/', 0),
(105, 'project_lg_img.png', '2015-09-30 18:05:35', '2015-09-30 18:05:35', 'uploads/2015-09-30/', 0),
(106, 'project_sm_image.png', '2015-09-30 22:12:00', '2015-09-30 22:12:00', 'uploads/2015-09-30/', 0),
(107, 'background-image.png', '2015-09-30 22:14:18', '2015-09-30 22:14:18', 'uploads/2015-09-30/', 0),
(108, '0b65a095bbe1fc6d43cbc9d5c0af1b24.png', '2015-09-30 23:44:15', '2015-09-30 23:44:15', 'uploads/2015-09-30/', 0),
(109, '9fde644530626f1dc0b90cd28762e866.png', '2015-10-01 00:08:22', '2015-10-01 00:08:22', 'uploads/2015-09-30/', 0),
(110, 'dc5fc85c24743b3efd5b13afc2722841.png', '2015-10-01 22:58:53', '2015-10-01 22:58:53', 'uploads/2015-10-01/', 0),
(111, '80128e535b29c0c085425551dafea0b4.png', '2015-10-01 23:48:32', '2015-10-01 23:48:32', 'uploads/2015-10-01/', 0),
(116, 'aec9f60ff71e479c6ba8d64f990b5eca.jpg', '2015-10-02 05:26:25', '2015-10-02 05:26:25', 'uploads/2015-10-01/', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mr',
  `email_validate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_email` tinyint(1) NOT NULL DEFAULT '0',
  `security_question1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_question2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_question3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_answer3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('regular','admin','supervisor') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'regular',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `avatar`, `facebook_id`, `remember_token`, `created_at`, `updated_at`, `title`, `email_validate`, `confirmed_email`, `security_question1`, `security_question2`, `security_question3`, `security_answer1`, `security_answer2`, `security_answer3`, `level`) VALUES
(1, 'Romarioh', 'Hall', 'hallstar01@yahoo.com', '$2y$10$HNoHHs9Z17KeeQC0Te7E9.PIJoWEDVmbh5/E22vJP3DR1jhohb1lW', 'uploads/2015-03-19/08dfd5cb049d158f1ec04132702a97a3.jpg', '', 'FAfezgSfZ0mvCFRWMBJGhgIoGmvkINswdBr5bMF2MXt00FS0aEIJE0YrElGs', '2015-03-03 13:15:13', '2015-05-07 01:54:38', 'mr', 'f551cc6cfe57cdee0d9c690bfb99b8c8', 0, 'What was the colour of your first car', 'Where were you born', 'What is the first name of your best friend', 'red', 'hospital', 'rushio', 'supervisor'),
(2, 'Phillip', 'Lindsay', 'hallstar01@gmail.com', '$2y$10$qCsIzMp9.AxMYJUJH7Ft0OUADmPnL9KujdnaiQSOP5HeIjaKA0ZBS', 'uploads/2015-08-25/313ba58e516609b8d2f634b67c9b6c23.png', '', 'Ls51NIqmYj7vUuRgayHuvHV9XUQtjM0rFW70F7G4chS9V6bdiixBnFdG4DrC', '2015-03-11 11:04:17', '2015-09-03 09:52:25', 'mr', 'c5a538befc1ec7d635b186944b91b6f0', 0, 'What was the colour of your first car', 'What is the name of your favourite Football team', 'Where were you born', 'white', 'arsenal', 'kingston', 'supervisor'),
(9, 'Rushio', 'Billings', 'rushiobillings@gmail.com', '$2y$10$N.5cpkedLai1siYXzRCVqehHxOkNsIyPRnHpvaPIEU6X8TkOX1WES', '', '', 'dZgyd4Yr3aPEo7jUQJgOC5CMfPgQ0giNiLoqmIfzSNYZLCWFuUhUg1ywRsWv', '2015-03-12 04:25:34', '2015-03-12 09:27:26', 'mr', '311aec1396b094572b35c49a74d3b692', 0, '', '', '', '', '', '', 'admin'),
(10, 'Rushio', 'Billings', 'rushiobillings@yahoo.com', '$2y$10$bmod/nLnYSlMrttLz9KTjODaH487SqVl3IghtCFkLKUITLQwvj0iC', 'uploads/2015-08-18/a289f1841a47f25ace34d825c7a3ba4c.png', '10204798496177033', 'Nb9Gc04nZAvn7Bc6v5mg8Nc2Oej10ULf5VyFitSiv4rfsTWop84C7mdbcvtA', '2015-03-20 05:50:57', '2015-08-19 08:40:53', '', '', 0, 'What was the colour of your first car', 'Where were you born', 'What is the name of your favourite place to visit', 'hello', 'hi', '3434', 'admin'),
(11, 'Kornel', 'Lewis', 'cornel_lewis@yahoo.com', '', 'uploads/2015-03-19/3ba62f092399f7de4b853de20337ca83.jpg', '1067615696585862', NULL, '2015-03-20 08:59:50', '2015-08-19 11:36:56', '', '', 0, 'Where were you born', 'What is the first name of your best friend', 'Which parish were you born', '123456', '123535', '2222', 'regular'),
(12, 'Anquane', 'Campbell', 'tajwane@gmail.com', '', 'https://graph.facebook.com/v2.2/715813185206475/picture?type=normal', '715813185206475', NULL, '2015-03-25 09:17:31', '2015-08-19 11:36:41', '', '', 0, 'What was the colour of your first car', 'Where was your wedding reception held', 'What is the name of your favourite Football team', 'blue', 'mandeville', 'barcelona', 'regular'),
(13, 'Romario', 'Hall', 'hallstar_01@yahoo.com', '', 'https://graph.facebook.com/v2.2/10206202194436344/picture?type=normal', '10206202194436344', 'QCpnTvIxzmnunm3QJUZetyNcwjXyK0jTXAHVi3y6GhMrG3YuvE0A9xujN8kY', '2015-03-26 01:23:54', '2015-03-26 03:47:42', 'mr', '', 0, 'What was the colour of your first car', 'Where were you born', 'Which parish were you born', 'Blue', 'university hospital', 'kingston', 'regular'),
(14, 'Michael', 'Claire', 'mwclaire@msn.com', '', 'https://graph.facebook.com/v2.2/10153134065208818/picture?type=normal', '10153134065208818', NULL, '2015-03-27 09:22:05', '2015-03-27 09:22:28', 'mr', '', 0, 'Where was your wedding reception held', 'What is the first name of your best friend', 'Where were you born', 'xcds', 'fhhhd', 'skj;sj;j;a', 'regular'),
(15, 'George', 'Henry', 'kgkinggeorge@yahoo.com', '$2y$10$WIPXPvOMtKct77JBqFB4GOfhyTUgdtqVBcoU3BWtyFNclgq4dUWPC', '', '', 'Ch9fAxYb7MvCSa3oNM3JF2hlldYefMLA9JjbAwQ9IG45YJYuLpcEv1TMesJC', '2015-04-18 00:17:14', '2015-04-20 05:36:33', 'mr', '9bab9a4ab781ab5dd7492cad6d8f6376', 0, 'Where were you born', 'What is the name of your favourite pet', 'Which parish were you born', 'trelawny', 'dingo', 'trelawny', 'regular'),
(17, 'dwayne', 'brown', 'dwayneb@jnbs.com', '$2y$10$gVDwyffA2GZtQo0yl5/JdOr17mO3gdZQqRwgLeNPju2EU3ommkAOS', '', '', 'pK1x0FELgB6d0YYzU0XFoGGiwO5TOTgDVvTDDvxbreQw6pAcPZxeL6mqUZjP', '2015-04-30 05:27:26', '2015-07-02 04:36:04', 'mr', '9d55db06cdf99a5b7582ef52423f6118', 0, 'What was the colour of your first car', 'Where were you born', 'What is the name of your favourite pet', 'white', 'kingston', 'dog', 'regular'),
(18, 'Ricardo', 'Smith', 'jodarina1@hotmail.com', '$2y$10$F0pfMIZTqKT4zw6F2d17K.gq9E.l0kZVBPtNM9fDoA4aCkYlahk6O', '', '', NULL, '2015-04-30 06:14:34', '2015-04-30 06:22:59', 'mr', 'aa5a82db3f9ee1122a37707bf2e87b73', 0, 'What was the colour of your first car', 'Where was your wedding reception held', 'What is the name of your favourite Football team', 'Red', 'yard', 'beatles', 'regular'),
(19, 'George', 'Henry', 'kgkinggeorge@gmail.com', '$2y$10$T2XuHf/Gb2..7TuwjIqIq.fK417r3cV663bMKi/vDDAvd4D1bm37G', '', '', 'VJqlfT0tWeqPaygOpLb1lQt31KnWnVt38N3gBi5UeHGlCdK3s0tU93LWhafd', '2015-05-08 02:33:30', '2015-05-08 02:33:43', 'mr', '5c0a67b0290684390707505e9690e615', 0, '', '', '', '', '', '', 'regular'),
(20, 'George', 'Baker', 'georgeh@spidercentro.com', '$2y$10$HNoHHs9Z17KeeQC0Te7E9.PIJoWEDVmbh5/E22vJP3DR1jhohb1lW', '', '', 'wFB630Vj84Ne5d4N1LnoROONAV0Lc7lhO9LFAxFyZePc92CWEJbBp8E2uGi8', '2015-05-08 02:34:26', '2015-07-02 06:12:18', 'mr', '772d45303b21d9c1c7d07f3aa6df0e19', 1, 'What was the colour of your first car', 'Where were you born', 'Where was your wedding reception held', 'swift', 'Trelawny', 'mandeville', 'admin'),
(23, 'dwayne', 'brown', 'crazydwayne@gmail.com', '$2y$10$jma.7fNmo01jYKzqN1FCr.SnVSu9nWZxEBDLj9KJ0U5ES.pd883M.', '', '', 'Qt2w4MKed6NMlVQ97l2guqbDlvI4fg2t6gV5SDaFkJ5LVd9t3gyoQmT9tdp3', '2015-07-02 04:38:36', '2015-09-03 09:51:00', '', 'db5e2f4c673532d97baeb4e53e134763', 1, 'Where were you born', 'Which parish were you born', 'What is the first name of your best friend', 'kingston', 'kingston', 'charlie', 'supervisor'),
(28, 'Kimberly', 'Manradge', 'kimberlym@spidercentro.com', '$2y$10$L8uLq/l95k6.VncPu6c04.QCHfikm8VrEP7ToffESHdjIt0gSrAKu', '', '', NULL, '2015-08-19 11:39:51', '2015-09-03 09:50:37', '', '6d4b2df3c0c4d2eef00c5fae91c309ad', 1, '', '', '', '', '', '', 'supervisor'),
(30, 'Phillip', 'Lindsay', 'PLindsay@jnbs.com', '$2y$10$RsoQtaii/oRopsjy2FandO3dcmz1go2Y2OGO/KIg5MUEY48AV4gWO', 'uploads/2015-09-25/9c06d04d5246a22d8458747d257dd770.jpg', '', 'RCdyucpJJNR6H1eVp6K69GRWCGIFV41jIDf3Nv0IL2FJNaMS6lGWgZ8zE0nj', '2015-09-03 09:52:14', '2015-10-01 12:15:09', 'mr', '195d1245bed337838e75b238617707e9', 1, 'What was the colour of your first car', 'Where was your wedding reception held', 'Which high school did you attend', 'white', 'altimont court hotel', 'meadowbrook', 'supervisor');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_follow`
--
ALTER TABLE `category_follow`
  ADD CONSTRAINT `category_follow_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_follow_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_page_section_id_foreign` FOREIGN KEY (`page_section_id`) REFERENCES `page_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pledges`
--
ALTER TABLE `pledges`
  ADD CONSTRAINT `pledges_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_picks`
--
ALTER TABLE `project_picks`
  ADD CONSTRAINT `project_picks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_picks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rewards`
--
ALTER TABLE `rewards`
  ADD CONSTRAINT `rewards_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;
--
-- Database: `jmmb_los`
--
--
-- Database: `loancirrus`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_07_05_190151_create_organizations_table', 1),
('2016_07_18_175958_add_timezone_to_organizations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'UTC',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizations_slug_unique` (`slug`),
  KEY `organizations_name_index` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `slug`, `timezone`, `created_at`, `updated_at`) VALUES
(1, 'ShaniceAgency', 'shanloans', 'America/Los_Angeles', '2016-07-20 08:56:19', '2016-07-20 08:56:19');
