var app = angular.module('blackJack', ['ngRoute', 'ui.router']);

    app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        // the known route
        //$urlRouterProvider.when('', '/');

        // For any unmatched url, send to 404
        $urlRouterProvider.otherwise('/404');
        
        $stateProvider

           
            .state('/', {
                url: '/',
                templateUrl: 'views/home.html'
            })
     
            .state('black-jack', {
                url: '/black-jack',
                templateUrl: 'views/black-jack.html',
                controller: 'BlackJackController'      
            })

            .state('/404', { 
                url : '/404',
                templateUrl: 'views/404.html',
            });
    });

    