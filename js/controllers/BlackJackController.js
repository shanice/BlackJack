app.controller('BlackJackController', ['$scope','$interval','$q', function($scope, $interval, $q){

	$scope.title 		= "Let the Games Begin!";
	$scope.my_score 	= 0;
	$scope.dealer_score = 0;
	$scope.myDeal 		= false;
	$scope.dealerDeal 	= false;
	$scope.dealShow 	= true;
	$scope.hitShow 		= false;
	$scope.standShow 	= false;
	$scope.message 		= false;

	$scope.showDeal = function()
	{
		$scope.dealShow 	= true;
		$scope.hitShow 		= false;
		$scope.standShow 	= false;
	}

	$scope.showOthers = function()
	{
		$scope.dealShow 	= false;
		$scope.hitShow 		= true;
		$scope.standShow 	= true;;
	}

	$scope.deal = function() 
	{
		if($scope.dealShow)
		{
			//Reset Scores
			$scope.my_score 	= 0;
			$scope.dealer_score = 0; 
			$scope.message 		= false; 

			//Get a random Card and calculate my score
	     	$scope.myCard 	= $scope.getRandomCard();
	     	$scope.my_score = $scope.my_score + $scope.myCard;
	     	$scope.myDeal 	= true;
	   
	   		//Gets the dealers card and score
	     	$scope.dealer($scope.dealer_score);
	     	$scope.showOthers();
	     	$scope.title = "";
	     	
		}
	}

	$scope.hit = function(my_score, dealer_score) 
	{
		if($scope.hitShow)
		{
			//Get another card and recalculate score
	     	$scope.myCard 	= $scope.getRandomCard();
	     	$scope.my_score = my_score + $scope.myCard;
	     	$scope.myDeal 	= true;

	     	//Get another dealer card and recalcute dealer's score
	     	$scope.dealer(dealer_score).then(function(new_dealer_score)
	     	{
	     		//Check to see if there is a winner
	     		$scope.chooseWinner($scope.my_score, new_dealer_score);
	     	});
		}
     	
	}

	$scope.dealer = function(dealer_score) 
	{
		var defered = $q.defer();

		//gets a dealer card and calculate the dealer score, and show in one second 
		$interval(function(){

	     	$scope.dealerCard 	= $scope.getRandomCard();
	     	$scope.dealer_score = dealer_score + $scope.dealerCard;
	     	$scope.dealerDeal 	= true;

	     	defered.resolve($scope.dealer_score);

	    }, 1000, 1, true, dealer_score);

	  	//returns apromise with the calculated dealer score 
	    return defered.promise;
	}

	$scope.stand = function(my_score, dealer_score)
	{
		console.log('stand');
		$scope.hitShow 		= false;
		$scope.standShow 	= false;

		//Get dealer card
		$scope.dealer(dealer_score).then(function(new_score)
 		{
 			dealer_score = new_score;

 			//Recursion baby!! continue playing
 			if(new_score < 21 && new_score < my_score)
			{
				$scope.stand(my_score, dealer_score);
			}
			else if(new_score < 21 && new_score == my_score)
			{
				$scope.stand(my_score, dealer_score);
			}
			else //otherwise
			{
				//Check to see if there is a winner
				if($scope.my_score < 21 && dealer_score < 21)
				{
					if(dealer_score > $scope.my_score)
					{
						$scope.message = 'Dealer wins!!';
			     		$scope.showDeal();
					}
					else
					{
						$scope.message = 'You win!!';
			     		$scope.showDeal();
					}
				}
				else
				{
     				$scope.chooseWinner($scope.my_score, dealer_score);
				}
			}

		});
	}

	$scope.getRandomCard = function()
	{
		var min = 1;
		var max = 11;

	    return randomCard = Math.floor(Math.random() * (max - min + 1)) + min;
	}

	$scope.chooseWinner = function(my_score, dealer_score)
	{
		console.log('dealer actual score: '+dealer_score);

 		if(my_score > 21 && dealer_score <= 21)
 		{
 			$scope.message = 'You Bust! Dealer Wins!!!!';
 			$scope.showDeal();
 		}

 		if(my_score <= 21 && dealer_score > 21)
 		{
 			$scope.message = 'Dealer Bust!!! You Win!!!!!';
 			$scope.showDeal();
 		}

 		if(my_score > 21  && dealer_score > 21)
 		{
 			$scope.message = 'You Loose!!';
     		$scope.showDeal();
 		}

 		if(my_score == 21)
     	{
     		$scope.message = 'You win!!';
     		$scope.showDeal();
     	}

     	if(dealer_score == 21)
     	{
     		$scope.message = 'Dealer wins!!';
     		$scope.showDeal();
     	}
	}

}]);